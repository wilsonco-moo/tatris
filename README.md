
# Tatris

Tatris is a tile-matching puzzle game, similar to the popular game [Tetris](www.tetris.com) ®.

## Tatris 1.0: Java version

I designed Tatris version 1 in April 2016 as an introductory project, when learning to use the Java graphics
library [LibGDX](https://libgdx.badlogicgames.com/). It is complete and playable, and should be able to
run on any operating system supporting Java.

Note that the options menu, and the stats menu were never actually implemented, so they do nothing in the main menu.

The source code is included as a Gradle project, and can be compiled using `./gradlew desktop:dist`.
A pre-compiled version of the game is also available as an executable jar file: tatris1.0.jar.

**Acknowledgements:**
 * Andrew Ackerley, for graphics design and game concepts.

## Tatris 2.1: C version

I designed Tatris version 2 in April 2018, as an introductory project to help me learn C. This is also my
first project using OpenGL and FreeGLUT. Like the Java version, Tatris 2.1 is also complete and playable. The only
difference (gameplay-wise), is that in Tatris 2.1, the speed at which the tiles fall does not increase over time.
This makes Tatris 2.1 easier and more relaxed to play.

Textures are not used in any part of Tatris 2.1. Instead, all UI and game elements are drawn using rectangles and gradients.
This means that Tatris 2.1 can flawlessly scale to any resolution, while still maintaining exactly the same look.
This also means that no texture files are needed, so Tatris 2.1 can run as a standalone application.

Note that much like Tatris 1.0, the options menu was never actually implemented. As such, options in the main menu
does nothing.

Pre-compiled versions for linux (i386 and amd64), are included in the bin directory. Also included is a pre-compiled
version for Windows, which I compiled using Microsoft Visual Studio.

To run Tatris 2.1 in linux, the following libraries are needed:
 * FreeGLUT, (the package `freeglut3` in Debian/Ubuntu).
 * The OpenGL Utility Toolkit, (the package `libglu1-mesa` in Debian/Ubuntu).

To compile Tatris 2.1 in linux, the following additional libraries are needed:
 * FreeGLUT development files, (the package `freeglut3-dev` in Debian/Ubuntu).
 * OpenGL Utility Toolkit development files, (the package `libglu1-mesa-dev` in Debian/Ubuntu).
 * The GNU C compiler, (the package `gcc` in Debian/Ubuntu).
 * GNU make, (the package `make` in Debian/Ubuntu).

To compile, simply run `make`, while within the tatris2.1 directory.

**Acknowledgements:**
 * Andrew Ackerley, for graphics design and game concepts.

---

_**Tetris** is a registered trademark of The Tetris Company, LLC, [www.tetris.com](www.tetris.com)._

---

## Screenshots

The main menu of Tatris 1.0:
![Image](screenshots/tatris1.0-menu.png)

Tatris 1.0, in game:
![Image](screenshots/tatris1.0-game.png)

The main menu of Tatris 2.1:
![Image](screenshots/tatris2.1-menu.png)

Tatris 2.1, in game:
![Image](screenshots/tatris2.1-game.png)
