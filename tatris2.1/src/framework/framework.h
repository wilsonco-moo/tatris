/*
 *  Tatris
 *  Copyright (C) April 2018, Daniel Wilson
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


#ifndef GL_HEADER_INCLUDED
    #define  GL_HEADER_INCLUDED
    #include "GL/freeglut.h"
    #include <stdio.h>
    #include <math.h>
    #include <time.h>
    #include <stdint.h>
    
    // If we are using windoze, include the extra stuff required for vsync.
    #if defined (__WIN32__)
        //#include <windows.h> // Is this necessary?
        #include "GL/wglext.h"
    #endif
    
#endif


/*
 * This is a macro to give a convenient way to carry out the proper mod operation.
 */
#define BETTER_MOD(a,b) (((a)%(b)) + ((a) < 0 ? (b) : 0))



// ------------------------------------------------------- UTIL FUNCTIONS -------------------------------------------------------


/*
 * This is used as a resizing callback. This allows us to maintain an aspect ratio
 * when resizing.
 * The sizes for this are defined below, and are assigned in util_initConfig().
 */
void util_reshape(GLsizei width, GLsizei height);

/*
 * Updates the shape of the window, without requiring the user to actually resize the window.
 * This should be called after we change, or initially set any of:
 * MAIN_WIDTH, MAIN_HEIGHT, MAIN_BORDER_LEFT, MAIN_BORDER_RIGHT, MAIN_BORDER_TOP, MAIN_BORDER_BOTTOM.
 * This function also calculates:
 * MAIN_AREA, MAIN_ASPECT, MAIN_ASPECT_WITH_BORDERS, MAIN_WIDTH_WITH_BORDERS, MAIN_HEIGHT_WITH_BORDERS.
 */
void util_updateShape(void);

/*
 * Here, we can optionally set a custom update function that will run each time util_updateShape is run.
 * This update function will run:
 *  > Once, after full reshaping, when we call util_updateShape outside of a step.
 *  > Twice, once after partial reshaping, again after full reshaping, when we call util_updateShape inside a step.
 *  > Once, each time the window is resized.
 */
void util_setUpdateFunction(void ( * newUpdateFunction ) ( void ) );

/*
 * This is a utility function to process a pointer given by malloc or calloc.
 * If the pointer is null, this outputs an apporpriate message, the exits.
 */
void * util_check(void * pointer);

/*
 * This enables vsync in a cross-platform way.
 */
void util_vsync(void);


// ------------------------------------------------------- VIEW CONFIGURATION ------------------------------------------------------

/*
 * This is the main width and height of the view area, and is assigned in util_initConfig().
 */
int MAIN_WIDTH, MAIN_HEIGHT;

/*
 * This is the area of the view area, assigned to MAIN_WIDTH * MAIN_HEIGHT in util_initConfig().
 */
int MAIN_AREA;


/*
 * These are the borders that are added onto MAIN_WIDTH and MAIN_HEIGHT. These are assigned in
 * util_initConfig().
 */
GLfloat MAIN_BORDER_LEFT, MAIN_BORDER_RIGHT, MAIN_BORDER_TOP, MAIN_BORDER_BOTTOM;

/*
 * This is the aspect of the view area, calculated in util_initConfig().
 * This is used for convenience in util_reshape().
 */
GLfloat MAIN_ASPECT;

/*
 * This is the aspect of the view area (including the borders), calculated in util_initConfig().
 * This is used for convenience in util_reshape().
 */
GLfloat MAIN_ASPECT_WITH_BORDERS;

/*
 * This is the width and height, calculated in util_initConfig(), used for convenience in util_reshape().
 */
GLfloat MAIN_WIDTH_WITH_BORDERS, MAIN_HEIGHT_WITH_BORDERS;

/*
 * These store the values for what is actually displayed on the screen, this is calculated by util_reshape().
 */
GLfloat MAIN_currentMinX, MAIN_currentMaxX, MAIN_currentMinY, MAIN_currentMaxY;



// ------------------------------------- GAME STEP STUFF --------------------------------------------------------


struct gameRoom; // Declare the game room here, so it can be used in the following method.
                 // This is properly defined below.

/*
 * This stores the current elapsed time (in seconds) since the last game step, this should be used for
 * speed scaling.
 * This is calculated by the step system.
 */
GLfloat step_elapsed;

/*
 * This stores the current game frame rate, and is automatically calculated by the step system.
 */
GLfloat step_fps;

/*
 * This stores the number of frames that have happened since the game was launched. This is automatically
 * updated by the game step system.
 */
int step_frameCount;

/*
 * This function changes the room from the current room to the new room.
 * If there is already a room set, this will run the old room's free() method.
 * This will also run the new room's init() method.
 * THIS MUST BE CALLED AT SOME POINT IN util_initConfig() OR THERE WILL BE NOTHING TO RUN EACH STEP,
 * AND WE WILL GET A SEGMENTATION FAULT.
 */
void step_setCurrentRoom(struct gameRoom newRoom);

/*
 * This gets the current room, set by step_setRoom(). If one has not been defined yet, this
 * function will return an undefined value.
 */
struct gameRoom step_getCurrentRoom(void);

/*
 * This can be used to set whether frame rate logging is enabled.
 * Frame rate logging is disabled by default.
 */
void step_setFrameRateLoggingEnabled(int enabled);

/*
 * This function is assigned by main.c to run every frame. This should not be modified by the user.
 */
void step_runMain(void);

/*
 * This function is run at the start by main.c. This calls the init() method in the current game room.
 */
void step_init(void);

/*
 * This function is set to be run when a key is pressed, by main.c.
 * This calls the keyNormal() method in the current game room.
 */
void step_keyNormal(unsigned char key, int x, int y);

/*
 * This function is set to be run when a key is released, by main.c.
 * This calls the keyNormalRelease() method in the current game room.
 */
void step_keyNormalRelease(unsigned char key, int x, int y);

/*
 * This function is set to be run when a special key is pressed, by main.c.
 * This calls the keySpecial() method in the current game room.
 */
void step_keySpecial(int key, int x, int y);

/*
 * This function is set to be run when a special key is released, by main.c.
 * This calls the keySpecialRelease() method in the current game room.
 */
void step_keySpecialRelease(int key, int x, int y);

/*
 * This stores if we should run util_updateShape() at the end of the frame.
 * This is so users can call util_updateShape() at any point, even during drawing.
 */
int step_updateShapeAtEndOfFrame;
/*
 * This stores whether we are currently drawing.
 */
int step_currentlyDrawing;


// ----------------------------------------------------------- GAME ROOM --------------------------------------------------------------------

/*
 * This struct is how user code should be added. It provides a portable way to manage game rooms.
 * The current room should be set by the function step_setCurrentRoom().
 * The current room is stored internally in step.c, and can be accessed by step_getCurrentRoom().
 */
struct gameRoom {
    void ( * init ) (void);  // This is run once initially at the start, when this room is set through step_setCurrentRoom().
    void ( * step ) (void);  // This is run each frame.
    void ( * free ) (void);  // This is run when we switch to another room, (in step_setCurrentRoom()),
                             // this should contain any code to free any resources allocated by this room.
    
    void ( * keyNormal        )  (unsigned char key, int x, int y); // This is run by step.c each time a normal key is pressed.   This is a: glutKeyboardFunc
    void ( * keyNormalRelease )  (unsigned char key, int x, int y); // This is run by step.c each time a normal key is released.  This is a: glutKeyboardUpFunc
    
    void ( * keySpecial )        (int key, int x, int y);           // This is run by step.c each time a special key is pressed.  This is a: glutSpecialFunc
    void ( * keySpecialRelease ) (int key, int x, int y);           // This is run by step.c each time a special key is released. This is a: glutSpecialUpFunc
};


/*
 * This is defined in rooms.c.
 * This is called by main.c at the very start of program execution.
 * Here, stuff to initialise general configuration should be placed.
 */
void room_initConfig(void);



// -------------------------------------------------------- STUFF FOR DRAWING -------------------------------------------------------------

/*
 * This is a simple struct defining an RGB colour.
 */
struct colour {
    GLfloat red;
    GLfloat green;
    GLfloat blue;
};
