/*
 *  Tatris
 *  Copyright (C) April 2018, Daniel Wilson
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */




#ifndef DONT_USE_MAIN_HEADER
    #include "framework.h"
#endif


/*
 * This contains various utility functions.
 */


int notReshapedYet = 1;
GLsizei lastWidth, lastHeight;

void ( * customUpdateFunction ) ( void ) = NULL;


void util_reshape(GLsizei width, GLsizei height) {
    lastWidth = width;
    lastHeight = height;
    notReshapedYet = 0;
    
    glViewport(0,0,width,height);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    
    GLfloat aspect = ((GLfloat)width)/((GLfloat)height);
    
    if (aspect > MAIN_ASPECT_WITH_BORDERS) {
        GLfloat halfW = (aspect/MAIN_ASPECT_WITH_BORDERS)*0.5f;
        MAIN_currentMinX = (0.5f - halfW)*MAIN_WIDTH_WITH_BORDERS - MAIN_BORDER_LEFT;
        MAIN_currentMaxX = (halfW + 0.5f)*MAIN_WIDTH_WITH_BORDERS - MAIN_BORDER_LEFT;
        MAIN_currentMinY = -MAIN_BORDER_TOP;
        MAIN_currentMaxY = MAIN_HEIGHT + MAIN_BORDER_BOTTOM;
    } else {
        GLfloat halfH = (MAIN_ASPECT_WITH_BORDERS/aspect)*0.5f;
        MAIN_currentMinY = (0.5f - halfH)*MAIN_HEIGHT_WITH_BORDERS - MAIN_BORDER_TOP;
        MAIN_currentMaxY = (halfH + 0.5f)*MAIN_HEIGHT_WITH_BORDERS - MAIN_BORDER_TOP;
        MAIN_currentMinX = -MAIN_BORDER_LEFT;
        MAIN_currentMaxX = MAIN_WIDTH + MAIN_BORDER_RIGHT;
    }
    gluOrtho2D(MAIN_currentMinX,MAIN_currentMaxX,MAIN_currentMaxY,MAIN_currentMinY);
    
    if (customUpdateFunction != NULL) {  // Then run the custom update function.
        customUpdateFunction();
    }
}


void util_updateShape(void) {
    
    MAIN_AREA = MAIN_WIDTH * MAIN_HEIGHT;
    
    MAIN_ASPECT = ((GLfloat)MAIN_WIDTH) / ((GLfloat)MAIN_HEIGHT);
    
    MAIN_ASPECT_WITH_BORDERS = (((GLfloat)MAIN_WIDTH) + MAIN_BORDER_LEFT + MAIN_BORDER_RIGHT) / (((GLfloat)MAIN_HEIGHT + MAIN_BORDER_TOP + MAIN_BORDER_BOTTOM));
    
    MAIN_WIDTH_WITH_BORDERS = MAIN_WIDTH + MAIN_BORDER_LEFT + MAIN_BORDER_RIGHT;
    MAIN_HEIGHT_WITH_BORDERS = MAIN_HEIGHT + MAIN_BORDER_TOP + MAIN_BORDER_BOTTOM;
    
    // This will only continue running here if we are resizing mid-game, i.e: glut has started.
    if (notReshapedYet) return;
    
    // If we are currently drawing, notify the step system to re-call util_updateShape() at the end of the frame.
    if (step_currentlyDrawing) {
        step_updateShapeAtEndOfFrame = 1;
    } else {
        // If we are not currently drawing, e.g: a key event, then run util_reshape immediately.
        util_reshape(lastWidth, lastHeight);
    }
    
    if (customUpdateFunction != NULL) {  // Then run the custom update function.
        customUpdateFunction();
    }
}


void util_setUpdateFunction(void ( * newUpdateFunction ) ( void ) ) {
    customUpdateFunction = newUpdateFunction;
}


/*
 * Allocated memory should be passed through this function, before use.
 * This checks if the memory allocation is NULL. If it is, an appropriate
 * message is shown, and the program exits.
 */
void * util_check(void * pointer) {
    if (pointer == NULL) {
        printf("\nMEMORY ALLOCATION FAILED, i.e: malloc() or calloc() returned NULL.\n  It seems that we have run out of memory.\n  More RAM memory anyone?\n\n");
        exit(0);
    }
    return pointer;
}













#if defined (__WIN32__)
    int WGLExtensionSupported(const char *extension_name);
#endif

/*
 * This function enables vsync.
 * In windoze, this uses the nasty windoze-specific stuff to enable vsync. (https://stackoverflow.com/a/589232)
 * In linux, double buffering seems to sync to the screens refresh rate anyway, so this does nothing.
 * MacOS ??
 */
void util_vsync(void) {
    
    #if defined (__WIN32__) // Only do anything if we are in windoze
        
        PFNWGLSWAPINTERVALEXTPROC       wglSwapIntervalEXT    = NULL;
        PFNWGLGETSWAPINTERVALEXTPROC    wglGetSwapIntervalEXT = NULL;

        if (WGLExtensionSupported("WGL_EXT_swap_control")) {
            // Extension is supported, init pointers.
            wglSwapIntervalEXT = (PFNWGLSWAPINTERVALEXTPROC) wglGetProcAddress("wglSwapIntervalEXT");
            
            // this is another function from WGL_EXT_swap_control extension
            wglGetSwapIntervalEXT = (PFNWGLGETSWAPINTERVALEXTPROC) wglGetProcAddress("wglGetSwapIntervalEXT");
            
            wglSwapIntervalEXT(1);
        }
        
    #endif
    
}


#if defined (__WIN32__)
    /*
     * This is a helper function for the windoze vsync function.
     */
    int WGLExtensionSupported(const char *extension_name) {
        // this is pointer to function which returns pointer to string with list of all wgl extensions
        PFNWGLGETEXTENSIONSSTRINGEXTPROC _wglGetExtensionsStringEXT = NULL;

        // determine pointer to wglGetExtensionsStringEXT function
        _wglGetExtensionsStringEXT = (PFNWGLGETEXTENSIONSSTRINGEXTPROC) wglGetProcAddress("wglGetExtensionsStringEXT");

        if (strstr(_wglGetExtensionsStringEXT(), extension_name) == NULL) {
            // string was not found
            return 0;
        }

        // extension is supported
        return 1;
    }
#endif







