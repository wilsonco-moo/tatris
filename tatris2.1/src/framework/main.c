/*
 *  Tatris
 *  Copyright (C) April 2018, Daniel Wilson
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


#ifndef DONT_USE_MAIN_HEADER
    #include "framework.h"
#endif


/*
 * This is the main .c file, the entry point of this program.
 */



int main(int argc, char **argv) {
    
    // First we should initialise the configuration.
    room_initConfig();
    
    // Next we should set up glut.
    
    glutInit(&argc, argv);            // Run glutInit
    glutInitDisplayMode(GLUT_DOUBLE); // Set the display mode to double buffering.
    glutInitWindowSize(800, 600);     // Set the window size.
    glutInitWindowPosition(100, 100); // Set the window position.
    glutCreateWindow("Program");      // Set the window title.
    
    // Now we should set up function pointers for the window:
    
    glutDisplayFunc(&step_runMain); // Set the display function: The function to redraw the window.
    glutIdleFunc(&step_runMain);    // Set the idle function: This runs each frame.
    glutReshapeFunc(&util_reshape); // Set the reshape function: This runs each time the window is resized.
    
    // After that we should set more function pointers:
    
    glutKeyboardFunc(&step_keyNormal);          // This runs each time a key is pressed.
    glutKeyboardUpFunc(&step_keyNormalRelease); // This runs each time a key is released.
    glutSpecialFunc(&step_keySpecial);          // This runs each time a special key is pressed.
    glutSpecialUpFunc(&step_keySpecialRelease); // This runs each time a special key is released.
    
    // Finally we should start the game.
    
    step_init();    // Run step.c's init function.
    glutMainLoop(); // Finally start the glut main program loop.
    
    return 0;
}
