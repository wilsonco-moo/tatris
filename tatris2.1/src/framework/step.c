/*
 *  Tatris
 *  Copyright (C) April 2018, Daniel Wilson
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */


#ifndef DONT_USE_MAIN_HEADER
    #include "framework.h"
#endif


/*
 * This manages the program's step, and does stuff like calculating the frame
 * elapsed time.
 * This file should not be modified by user implementation, as code should be added using
 * the game room system.
 */



/*
 * This stores if we have added a game room yet.
 */
int roomHasBeenSet = 0;
/*
 * This stores the current game room.
 */
struct gameRoom currentRoom;

/*
 * This stores whether frame rate logging is enabled.
 * This is disabled by default.
 */
int enableFrameRateLogging = 0;


/*
 * This can be used to set whether frame rate logging is enabled.
 */
void step_setFrameRateLoggingEnabled(int enabled) {
    enableFrameRateLogging = enabled;
}

/*
 * This stores if we should run util_updateShape() at the end of the frame.
 * This is so users can call util_updateShape() at any point, even during drawing.
 */
int step_updateShapeAtEndOfFrame = 0;
/*
 * This stores whether we are currently drawing.
 */
int step_currentlyDrawing = 0;


/*
 * This function changes the current game room.
 */
void step_setCurrentRoom(struct gameRoom newRoom) {
    if (roomHasBeenSet) {
        currentRoom.free(); // Run the old game room's free() method, if there is one.
    }
    currentRoom = newRoom; // Store the new room.
    currentRoom.init();    // Run the new room's init() method.
    roomHasBeenSet = 1;    // Ensure we know that we now have a game room.
}


/*
 * This gets our current room.
 */
struct gameRoom step_getCurrentRoom(void) {
    return currentRoom;
}



void step_manageFrameRate(void);


void step_init(void) {
    // Try to enable vsync when the game starts.
    util_vsync();
}

/*
 * This is run each frame.
 */
void step_runMain(void) {
    // Show that we are currently drawing.
    step_currentlyDrawing = 1;
    
    // Do the frame rate calculation stuff.
    step_manageFrameRate();
    
    // Fill with the default background colour.
    glClearColor(0.4, 0.4, 0.4, 0.4);
    // Clear the drawing area.
    glClear(GL_COLOR_BUFFER_BIT);
    // Begin drawing rectangles.
    glBegin(GL_QUADS);
    // Run the room's step method.
    currentRoom.step();
    // End the drawing.
    glEnd();
    
    // Swap the buffers for double buffering.
    glutSwapBuffers();
    
    // Show that we are no longer currently drawing.
    step_currentlyDrawing = 0;
    
    // Do an appropriate action if we are supposed to update the shape at the end of the frame.
    if (step_updateShapeAtEndOfFrame) {
        step_updateShapeAtEndOfFrame = 0;
        util_updateShape();
    }
}


// ------------------- THE FUNCTION POINTERS FOR KEY PRESSES -------------


void step_keyNormal(unsigned char key, int x, int y) {
    currentRoom.keyNormal(key,x,y);
}

void step_keyNormalRelease(unsigned char key, int x, int y) {
    currentRoom.keyNormalRelease(key,x,y);
}
    
void step_keySpecial(int key, int x, int y) {
    currentRoom.keySpecial(key,x,y);
}

void step_keySpecialRelease(int key, int x, int y) {
    currentRoom.keySpecialRelease(key,x,y);
}








// ------------------- TIME CALCULATION --------------------------------------


int lastFpsCalculation = 0;
int lastStepTime = 0;
GLfloat fpsCalcCounter = 0.0f;
GLfloat fpsCalcElapsed = 0.0f;
int fpsFrameCount = 0;

// This does all of the time maths.
void step_manageFrameRate(void) {
    int thisStepTime = glutGet(GLUT_ELAPSED_TIME);
    
    step_elapsed = ((GLfloat)(thisStepTime - lastStepTime)) / 1000.0f;
    
    fpsCalcCounter += step_elapsed;
    fpsCalcElapsed += step_elapsed;
    
    if (fpsCalcCounter >= 1.0f) {
        step_fps = ((GLfloat)fpsFrameCount)/fpsCalcElapsed;
        fpsFrameCount = 0;
        fpsCalcCounter = fmod(fpsCalcCounter, 1.0f);
        fpsCalcElapsed = 0.0f;
        if (enableFrameRateLogging) {
            printf("%f fps\n", step_fps);
        }
    }
    
    lastStepTime = thisStepTime;
    step_frameCount++;
    fpsFrameCount++;
}
