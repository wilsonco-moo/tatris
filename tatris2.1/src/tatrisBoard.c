/*
 *  Tatris
 *  Copyright (C) April 2018, Daniel Wilson
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */




#ifndef DONT_USE_MAIN_HEADER
    #include "tatris.h"
#endif




/*
 * Adds the block to this board, using the position defined within the block.
 * If the block's internal position is outside the board's area, nothing is added, and the block is freed.
 * If there is already a block at that position, the old block is freed, and the new block is added here instead.
 * If the block is NULL, the position is set to NULL, effectively removing/freeing any blocks already at that location.
 */
void tatrisBoard_addBlock(TatrisBoard board, struct tatrisBlock * block) {
    
    // Get the position of the block, and the width/height of our board.
    int x = block->x,
        y = block->y,
        width = board->width,
        height = board->height;
    
    // If the block's position is within the board's area...
    if (x >= 0 && x < width && y >= 0 && y < height) { 
        
        // Get the location within the board's block array that we will be adding our block to.
        struct tatrisBlock ** location = board->blocks + x + y*width;
        
        // Get a pointer to the block that is currently at this location.
        struct tatrisBlock * oldBlock = *location;
        
        // If this is not NULL, i.e: There is already a block here...
        if (oldBlock != NULL) {
            oldBlock->free(oldBlock); // ... then we must free this block.
        }
        
        *location = block; // Now, set that location to point at our block.
        
        block->xOffset = &(board->xOffset); // And set our block's offsets to the board's offsets, as it is now a member of the board.
        block->yOffset = &(board->yOffset);
        
    } else {
        // If the block's position is outside the board's area, free the block.
        block->free(block);
    }
}



/*
 * Adds the other board onto this board, such that:
 *  > This method ensures that the other board is freed from memory completely.
 *  > Any blocks in the other board, that are inside our board's area, are put into our board.
 *  > Any blocks in our board that are replaced, are also freed.
 *  > Any blocks in the other board oudside the area, are freed from memory.
 */
void tatrisBoard_addBoard(TatrisBoard board, TatrisBoard other) {
    
    // Get the position within our board that we will be adding the other board into.
    int xOrigin = other->x - board->x,
        yOrigin = other->y - board->y;
    
    // Get the other board's blocks array, as well as a pointer to the end of it.
    struct tatrisBlock ** otherArr = other->blocks;
    struct tatrisBlock ** otherEnd = otherArr + other->area;
    
    // Now loop through every element of the other board's blocks array.
    while(otherArr != otherEnd) {
        
        // Get a pointer to a block from this element of the other board's array.
        struct tatrisBlock * block = *(otherArr);
        
        // If there actually is a block here....
        if (block != NULL) {
            
            // Set it to null in the other board's array, so this block is not freed at the end of this method.
            *(otherArr++) = NULL;
            
            // Set the block's x and y position to something relative to the origin we worked out before.
            block->x = block->x + xOrigin;
            block->y = block->y + yOrigin;
            
            // And add it to the board using that board's addBlock method.
            board->addBlock(board, block);
            
        } else otherArr++;
    }
    // Finally, free the other board at the end.
    other->free(other);
}



/*
 * A tatris collision check.
 * Returns TRUE (1) IF:
 *      Any of the blocks in board intersect with any of the blocks of other.
 *   OR Any of the blocks in board lie outside other's area, to the left, right or bottom.
 *          **NOTE: Blocks are allowed to lie outside of the area to the top.
 */
int tatrisBoard_checkCollision(TatrisBoard board, TatrisBoard other) {
    //printf("\nChecking collision...\n");
    
    int xOrigin = board->x - other->x,
        yOrigin = board->y - other->y,
        
        otherWidth  = other->width,
        otherHeight = other->height;
        
    
    
    //printf("Adding to origin: %d,%d\n",xOrigin,yOrigin);
    
    struct tatrisBlock ** arr = board->blocks;
    struct tatrisBlock ** end = arr + board->area;
    struct tatrisBlock ** otherBlocks = other->blocks;

    
    while(arr != end) {
        
        //printf("Processing block %d....\n",TEMP_DEBUG_COUNT++);
        struct tatrisBlock * block = *(arr++);
        
        
        if (block != NULL) {
            
            int blockX = block->x + xOrigin,
                blockY = block->y + yOrigin;
                
            //printf("There is a block here: %d,%d\n",blockX,blockY);
            
            if (blockX < 0 || blockX >= otherWidth || blockY >= otherHeight) {
                //printf("This is an illegal location, returning a collision.\n");
                return 1;
            } else if (blockY >= 0) {
                //printf("This is a valid location.\n");
                
                struct tatrisBlock * blockFromOther = *(otherBlocks + blockX + blockY * otherWidth);
            
                if (blockFromOther != NULL) {
                    //printf("There is an intersecting block at %d,%d - returning a collision.\n",blockX,blockY);
                    return 1;
                } //else {
                //    printf("There is no intersecting collision here.\n");
                //}
                
            } else {
                //printf("Above grid, this is ignored.\n");
            }
            
        }
    }
    return 0;
}



/*
 * Returns 1 (true) if the specified line is filled.
 */
int tatrisBoard_checkLine(TatrisBoard board, int line) {
    int width = board->width;
    struct tatrisBlock ** arr = board->blocks + line * width;
    struct tatrisBlock ** end = arr + width;
    
    while(arr != end) {
        if (*(arr++) == NULL) return 0;
    }
    return 1;
}

/*
 * Removes all blocks from the specified line.
 */
void tatrisBoard_removeLine(TatrisBoard board, int line) {
    int width = board->width;
    struct tatrisBlock ** arr = board->blocks + line * width;
    struct tatrisBlock ** end = arr + width;
    
    while(arr != end) {
        TatrisBlock block = *arr;
        if (block != NULL) {
            block->free(block);
            *arr = NULL;
        }
        arr++;
    }
}


/*
 * Splits the board at the specified start line and end line, moving all the blocks into
 * the new board.
 */
TatrisBoard tatrisBoard_split(TatrisBoard board, int startLine, int endLine) {
    int width = board->width;
    int newHeight = endLine - startLine;
    
    TatrisBoard newBoard = tatrisBoard_new(width, newHeight);
    
    struct tatrisBlock ** arr = board->blocks + startLine * width;
    
    int ix = 0, iy = 0;
    while(iy < newHeight) {
        while(ix < width) {
            
            struct tatrisBlock * block = *arr;
            
            if (block != NULL) {
                block->x = ix;
                block->y = iy;
                newBoard->addBlock(newBoard, block);
                *arr = NULL;
            }
            arr++; ix++;
        }
        iy++; ix = 0;
    }

    newBoard->xOffset = board->xOffset;
    newBoard->yOffset = board->yOffset + startLine;
    
    newBoard->x = board->x;
    newBoard->y = board->y + startLine;
    
    return newBoard;
}



//void tatrisBoard_addBlock(TatrisBoard board, struct tatrisBlock * block) {
//    struct tatrisBlock ** arr = board->blocks;
//    *(arr + (int)(block->x) + (int)(block->y)*board->width) = block;
//    block->xOffset = &(board->xOffset);
//    block->yOffset = &(board->yOffset);
//}

//void tatrisBoard_addBlockPos(TatrisBoard board, struct tatrisBlock * block, int x, int y) {
//    struct tatrisBlock ** arr = board->blocks;
//    *(arr + x + y*board->width) = block;
//    block->xOffset = &(board->xOffset);
//    block->yOffset = &(board->yOffset);
//}

void tatrisBoard_removeBlock(TatrisBoard board, int x, int y) {
    struct tatrisBlock ** arr = board->blocks;
    struct tatrisBlock * b = *(arr + x + y*board->width);
    if (b != NULL) {
        b->free(b);
        *(arr + x + y*board->width) = NULL;
    }
}

struct tatrisBlock * tatrisBoard_removeBlockNoFree(TatrisBoard board, int x, int y) {
    struct tatrisBlock ** arr = board->blocks;
    struct tatrisBlock * b = *(arr + x + y*board->width);
    if (b != NULL) {
        b->setOffsets(b,NULL,NULL);
        *(arr + x + y*board->width) = NULL;
    }
    return b;
}

struct tatrisBlock * tatrisBoard_getBlock(TatrisBoard board, int x, int y) {
    struct tatrisBlock ** arr = board->blocks;
    return *(arr + x + y*board->width);
}

void tatrisBoard_draw(TatrisBoard board) {
    struct tatrisBlock ** arr = board->blocks;
    int i = 0, len = board->area; while(i < len) {
        TatrisBlock b = *(arr++);
        if (b != NULL) {
            b->draw(b);
        }
        i++;
    }
}

void tatrisBoard_free(TatrisBoard board) {
    int ix = 0, iy = 0, w = board->width, h = board->height;
    while(iy < h) {
        while(ix < w) {
            tatrisBoard_removeBlock(board,ix,iy);
            ix++;
        }
        ix = 0; iy++;
    }
    free(board->blocks);
    free(board);
}


TatrisBoard tatrisBoard_new(int width, int height) {
    TatrisBoard board = util_check(malloc(sizeof(struct tatrisBoard)));
    board->xOffset = 0.0f;
    board->yOffset = 0.0f;
    board->x = 0;
    board->y = 0;
    board->width = width;
    board->height = height;
    board->area = width*height;
    board->blocks = util_check(calloc(width * height, sizeof(TatrisBlock)));
    board->addBlock = &tatrisBoard_addBlock;
    board->addBoard = &tatrisBoard_addBoard;
    board->checkCollision = &tatrisBoard_checkCollision;
    board->checkLine = &tatrisBoard_checkLine;
    board->removeLine = &tatrisBoard_removeLine;
    board->split = &tatrisBoard_split;
    board->removeBlock = &tatrisBoard_removeBlock;
    board->getBlock = &tatrisBoard_getBlock;
    board->draw = &tatrisBoard_draw;
    board->free = &tatrisBoard_free;
    return board;
}
