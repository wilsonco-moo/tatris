/*
 *  Tatris
 *  Copyright (C) April 2018, Daniel Wilson
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */



#ifndef DONT_USE_MAIN_HEADER
    #include "tatris.h"
#endif

/*
 * This file contains the stuff for defining and drawing rectangle fonts.
 */




/*
 * This macro reads all the configuration of a font, assuming we have a font in the variable "font".
 * After this has finished, font now points at the start of the actual font data.
 */
#define READ_FONT_CONFIG                                 \
                                                         \
    /* Read the min char, max char and char count */     \
    unsigned char minChar = (unsigned char) *(font++),   \
                  maxChar = (unsigned char) *(font++),   \
                charCount = (unsigned char) *(font++);   \
                                                         \
    /* Read the shapes and floats per char */            \
    int shapesPerChar = (int) *(font++),                 \
        floatsPerChar = (int) *(font++);                 \
                                                         \
    /* Read the char width and height */                 \
    GLfloat charWidth = *(font++),                       \
           charHeight = *(font++);                       \



/*
 * This macro allows a character to be drawn.
 * An x and y offset to draw the character can be optionally given.
 * The x and y offset is added onto the end of the x and y values, so:
 * 
 *  > To draw without x and y offsets, leave XOFF and YOFF blank, such as: DRAW_CHAR(,,,).
 *  > To draw with x and y offsets, add a + character, such as:  DRAW_CHAR(+1,+2,,),  OR  DRAW_CHAR(+xOffset,+yOffset,,)
 * 
 * An x and y scale is also included, these can be uses similarly by specifying  DRAW_CHAR(+xOffset,+yOffset,*xScale,*yScale)
 * 
 * The offsets being done like this mean that if no offsets are specified, we do not have
 * a dummy "add zero" instruction. If offsets are supplied, they are added in.
 * Negative offsets can also be supplied, such as:  DRAW_CHAR(-1,-2,,),  OR   DRAW_CHAR(-xOffset,-yOffset,,)
 * Here, the add instruction is swapped for a subtrace intstruction.
 */
#define DRAW_CHAR(XOFF,YOFF,XSCALE,YSCALE)                                            \
    /* Get a pointer to the start of this character. This calculated from           */\
    /* the font configuration.                                                      */\
    GLfloat * charPointer = font + (floatsPerChar * (character - minChar));           \
    /* Get the number of shapes stored in this character. This is the first         */\
    /* value in each character. Shift left by three (multiply by 8) to get the      */\
    /* number of float values in this character.                                    */\
    int coords = ((int) *(charPointer++)) << 3;                                       \
    /* Get an end pointer, as where we should stop drawing this character's         */\
    /* coordinates.                                                                 */\
    GLfloat * charEndPointer = charPointer + coords;                                  \
    /* This is used to temporarily store x positions.                               */\
    GLfloat tempX;                                                                    \
    /* Loop from the start to the end of the character's stuff.                     */\
    while(charPointer != charEndPointer) {                                            \
        /* Get the x position, to ensure we get the x position first.               */\
        tempX = *(charPointer++) XSCALE XOFF;                                         \
        /* Then draw the vertex, (the y value is the next one along).               */\
        glVertex2f(tempX, *(charPointer++) YSCALE YOFF);                              \
    }                                                                                 \


    




void simpleRectangleFont_drawChar(SimpleRectangleFont font, unsigned char character) {
    READ_FONT_CONFIG
    DRAW_CHAR(,,,)
}
void simpleRectangleFont_drawCharPos(SimpleRectangleFont font, unsigned char character, GLfloat x, GLfloat y) {
    READ_FONT_CONFIG
    DRAW_CHAR(+x,+y,,)
}


void simpleRectangleFont_drawString(SimpleRectangleFont font, unsigned char * string, GLfloat x, GLfloat y) {
    READ_FONT_CONFIG
    unsigned char character;
    while((character = *(string++)) != '\0') {
        DRAW_CHAR(+x,+y,,)
        x += charWidth;
    }
}

void simpleRectangleFont_drawStringScale(SimpleRectangleFont font, unsigned char * string, GLfloat x, GLfloat y, GLfloat xScale, GLfloat yScale) {
    READ_FONT_CONFIG
    charWidth *= xScale;
    unsigned char character;
    while((character = *(string++)) != '\0') {
        DRAW_CHAR(+x,+y,*xScale,*yScale)
        x += charWidth;
    }
}




// This prints the configuration of the specified font.
void simpleRectangleFont_printConfig(SimpleRectangleFont font) {
    READ_FONT_CONFIG
    printf("Min char: (inclusive) %d\n", minChar);
    printf("Max char: (exclusive) %d\n", maxChar);
    printf("Char count: %d\n", charCount);
    printf("Shapes per char: %d\n", shapesPerChar);
    printf("Floats per char: %d\n", floatsPerChar);
    printf("Char width:  %f\n", charWidth);
    printf("Char height: %f\n", charHeight);
}
