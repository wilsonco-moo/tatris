/*
 *  Tatris
 *  Copyright (C) April 2018, Daniel Wilson
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */




#ifndef DONT_USE_MAIN_HEADER
    #include "tatris.h"
#endif



struct colour tatrisPiece_colours[] = {
    { 0.0f,  1.0f,  1.0f },
    { 0.0f,  0.0f,  1.0f },
    { 1.0f,  0.5f,  0.0f },
    { 1.0f,  1.0f,  0.0f },
    { 0.0f,  1.0f,  0.0f },
    { 0.75f, 0.0f,  1.0f },
    { 1.0f,  0.0f,  0.0f }
};




// TATRIS_PIECE_DEF_BITWISE(a0,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13,a14,a15) // ANGLE=0
// TATRIS_PIECE_DEF_BITWISE(a3,a7,a11,a15,a2,a6,a10,a14,a1,a5,a9,a13,a0,a4,a8,a12) // ANGLE=1
// TATRIS_PIECE_DEF_BITWISE(a15,a14,a13,a12,a11,a10,a9,a8,a7,a6,a5,a4,a3,a2,a1,a0) // ANGLE=2
// TATRIS_PIECE_DEF_BITWISE(a12,a8,a4,a0,a13,a9,a5,a1,a14,a10,a6,a2,a15,a11,a7,a3) // ANGLE=3

#define TATRIS_PIECE_DEF_BITWISE(a0,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13,a14,a15) a0<<15 | a1<<14 | a2<<13 | a3<<12 | a4<<11 | a5<<10 | a6<<9 | a7<<8 | a8<<7 | a9<<6 | a10<<5 | a11<<4 | a12<<3 | a13<<2 | a14<<1 | a15<<0, 
#define TATRIS_PIECE_DEF(a0,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13,a14,a15) TATRIS_PIECE_DEF_BITWISE(a0,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13,a14,a15) TATRIS_PIECE_DEF_BITWISE(a3,a7,a11,a15,a2,a6,a10,a14,a1,a5,a9,a13,a0,a4,a8,a12) TATRIS_PIECE_DEF_BITWISE(a15,a14,a13,a12,a11,a10,a9,a8,a7,a6,a5,a4,a3,a2,a1,a0) TATRIS_PIECE_DEF_BITWISE(a12,a8,a4,a0,a13,a9,a5,a1,a14,a10,a6,a2,a15,a11,a7,a3)

short tatrisPiece_contentsArray[] = {
    
    TATRIS_PIECE_DEF(
        0,0,0,0,
        1,1,1,1,
        0,0,0,0,
        0,0,0,0
    )
    
    TATRIS_PIECE_DEF(
        0,0,0,0,
        1,1,1,0,
        0,0,1,0,
        0,0,0,0
    )
    
    TATRIS_PIECE_DEF(
        0,0,0,0,
        0,1,1,1,
        0,1,0,0,
        0,0,0,0
    )
    
    TATRIS_PIECE_DEF(
        0,0,0,0,
        0,1,1,0,
        0,1,1,0,
        0,0,0,0
    )
    
    TATRIS_PIECE_DEF(
        0,0,0,0,
        0,1,1,0,
        1,1,0,0,
        0,0,0,0
    )
    
    TATRIS_PIECE_DEF(
        0,0,0,0,
        0,1,1,1,
        0,0,1,0,
        0,0,0,0
    )
    
    TATRIS_PIECE_DEF(
        0,0,0,0,
        0,1,1,0,
        0,0,1,1,
        0,0,0,0
    )
};



TatrisBoard tatrisPiece_new(int type) {
    
    TatrisBoard b = tatrisBoard_new(TATRIS_PIECE_WIDTH, TATRIS_PIECE_HEIGHT);
    
    int contents = tatrisPiece_contentsArray[type],
        mask     = 32768,
        ix       = 0,
        iy       = 0;
    
    struct colour col = tatrisPiece_colours[type/TATRIS_PIECE_ROTATIONS];
    
    while(iy < TATRIS_PIECE_HEIGHT) {
        while(ix < TATRIS_PIECE_WIDTH) {
            
            if ((contents & mask) != 0) {
                b->addBlock(b, tatrisBlock_newPosCol(ix, iy, col));
            }
            
            mask >>= 1;
            ix++;
        }
        ix = 0; iy++;
    }
    
    return b;
}



void tatrisPiece_initRandom(void) {
    srand(time(NULL));
}

int tatrisPiece_newRandomId(void) {
    //return 0; // TESTING
    return rand() % TATRIS_PIECE_COUNT;
}

int tatrisPiece_rotate(int type, int rotation) {
    int angle = type % TATRIS_PIECE_ROTATIONS;
    int basePiece = type - angle;
    int newAngle = angle + rotation;
    
    return basePiece + BETTER_MOD(newAngle,TATRIS_PIECE_ROTATIONS);
}






