/*
 *  Tatris
 *  Copyright (C) April 2018, Daniel Wilson
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */



#ifndef DONT_USE_MAIN_HEADER
    #include "tatris.h"
#endif

/*
 * This is the menu.
 */


int menu_startGame = 0;
int menu_selectedOption = 0;


void menu_changeSelectedOption(int by);
void menu_drawSelectBox(void);
void menu_processNextRoom(void);


void menu_keyNormal(unsigned char key, int x, int y) {
    
    switch(key) {
    
    case 13: case ' ':
        menu_startGame = 1;
        break;
    
    case 9:
        menu_changeSelectedOption(1);
        break;
            
    }
}
void menu_keyNormalRelease(unsigned char key, int x, int y) {
}

void menu_keySpecial(int key, int x, int y) {
    
    switch(key) {
    
    case GLUT_KEY_DOWN: case GLUT_KEY_RIGHT:
        menu_changeSelectedOption(1);
        break;
        
    case GLUT_KEY_UP: case GLUT_KEY_LEFT:
        menu_changeSelectedOption(-1);
        break;
    }
}

void menu_keySpecialRelease(int key, int x, int y) {
}
void menu_init(void) {
    room_setUpAreaConstantsForMenu();
}


GLfloat menu_time = 0.0f;
GLfloat menu_timeNum = 0.0f;
GLfloat menu_maxTime = 10.0f;

#define MENU_END_TIME 0.3f
#define MENU_SWITCH_ROOM_TIME 0.45f

#define MENU_OPTION_Y 62.0f
#define MENU_SELECT_BOX_Y 60.5f
#define MENU_OPTION_SPACING 10.0f

#define MENU_SELECT_BOX_WIDTH 50.0f


struct colour menu_bgTop    = {0.0f, 0.0f, 1.0f};
struct colour menu_bgBottom = {0.0f, 1.0f, 1.0f};



struct colour menu_fgTop    = {0.2f, 0.2f, 0.8f};
struct colour menu_fgBottom = {0.1f, 0.8f, 0.8f};

struct animation menu_fgAnimation = {
    0.10f,             // Min time
    MENU_END_TIME,     // Max time
    100.0f,            // Smoothness
    {0.0f, 0.0f},      // Start
    {0.0f, 0.0f},      // End
    {0.0f, 0.0f},      // Current
    &MAIN_currentMaxX, // xOrigin
    &MAIN_currentMaxY  // yOrigin
};



struct colour menu_title = {0.05f, 1.0f, 0.05f};
struct animation menu_superTatrisAnimation = {
    0.175f,            // Min time
    MENU_END_TIME,     // Max time
    120.0f,            // Smoothness
    {6.0f, -14.0f},    // Start
    {6.0f, 2.0f},      // End
    {0.0f, 0.0f},      // Current
    NULL,              // xOrigin
    &MAIN_currentMinY  // yOrigin
};
struct animation menu_1995Animation = {
    0.15f,             // Min time
    MENU_END_TIME,     // Max time
    120.0f,            // Smoothness
    {21.6f, -7.0f},    // Start
    {21.6f, 9.0f},     // End
    {0.0f, 0.0f},      // Current
    NULL,              // xOrigin
    &MAIN_currentMinY  // yOrigin
};


struct colour menu_mainMenu = { 1.0f, 1.0f, 1.0f};
struct animation menu_mainMenuAnimation = {
    0.20f,                   // Min time
    MENU_END_TIME,           // Max time
    80.0f,                   // Smoothness
    {/*-72.0f*/0.0f, 20.0f}, // Start
    {12.0f, 20.0f},          // End
    {0.0f, 0.0f},            // Current
    &MAIN_currentMaxX,       // xOrigin
    NULL                     // yOrigin
};



struct colour menu_selectBox = {0.0f, 0.0f, 1.0f};
struct animation menu_selectBoxAnimation = {
    0.23f,                                                   // Min time
    MENU_END_TIME,                                           // Max time
    80.0f,                                                   // Smoothness
    {-MENU_SELECT_BOX_WIDTH, MENU_SELECT_BOX_Y},             // Start
    {(96.0f-MENU_SELECT_BOX_WIDTH)*0.5f, MENU_SELECT_BOX_Y}, // End
    {0.0f, 0.0f},                                            // Current
    &MAIN_currentMinX,                                       // xOrigin
    NULL                                                     // yOrigin
};


struct colour menu_selectedColour   = {1.0f, 1.0f, 0.0f};
struct colour menu_unSelectedColour = {0.9f, 0.9f, 0.9f};
struct animation menu_startGameAnimation = {
    0.23f,                   // Min time
    MENU_END_TIME,           // Max time
    80.0f,                   // Smoothness
    {-40.0f, MENU_OPTION_Y}, // Start
    {28.0f, MENU_OPTION_Y},  // End
    {0.0f, 0.0f},            // Current
    &MAIN_currentMinX,       // xOrigin
    NULL                     // yOrigin
};
struct animation menu_optionsAnimation = {
    0.26f,                                        // Min time
    MENU_END_TIME,                                // Max time
    80.0f,                                        // Smoothness
    {0.0f, MENU_OPTION_Y + MENU_OPTION_SPACING},  // Start
    {34.0f, MENU_OPTION_Y + MENU_OPTION_SPACING}, // End
    {0.0f, 0.0f},                                 // Current
    &MAIN_currentMaxX,                            // xOrigin
    NULL                                          // yOrigin
};
struct animation menu_quitAnimation = {
    0.29f,                                                // Min time
    MENU_END_TIME,                                        // Max time
    80.0f,                                                // Smoothness
    {-16.0f, MENU_OPTION_Y + MENU_OPTION_SPACING * 2.0f}, // Start
    {40.0f, MENU_OPTION_Y + MENU_OPTION_SPACING * 2.0f},  // End
    {0.0f, 0.0f},                                         // Current
    &MAIN_currentMinX,                                    // xOrigin
    NULL                                                  // yOrigin
};



void menu_bg(void);


void menu_step(void) {
    animation_step(&menu_fgAnimation, menu_timeNum);
    animation_step(&menu_superTatrisAnimation, menu_timeNum);
    animation_step(&menu_1995Animation, menu_timeNum);
    animation_step(&menu_mainMenuAnimation, menu_timeNum);
    animation_step(&menu_selectBoxAnimation, menu_timeNum);
    animation_step(&menu_startGameAnimation, menu_timeNum);
    animation_step(&menu_optionsAnimation, menu_timeNum);
    animation_step(&menu_quitAnimation, menu_timeNum);
    
    menu_bg();
    
    SET_COLOUR(menu_title)
        simpleRectangleFont_drawStringScale(fonts_mainFont, "Wilsonco Super-Tatris", ANIM(menu_superTatrisAnimation), 0.5f, 0.5f);
        simpleRectangleFont_drawStringScale(fonts_mainFont, "1995 release, ver: 2.1", ANIM(menu_1995Animation), 0.3f, 0.3f);
    
    SET_COLOUR(menu_mainMenu)
        simpleRectangleFont_drawString(fonts_mainFont, "Main menu", ANIM(menu_mainMenuAnimation));
    
    
    struct colour c;
    
    menu_drawSelectBox();
    
    c = menu_selectedOption == 0 ? menu_selectedColour : menu_unSelectedColour;
        SET_COLOUR(c)
        simpleRectangleFont_drawStringScale(fonts_mainFont, "Start game", ANIM(menu_startGameAnimation), 0.5f, 0.5f);
    
    c = menu_selectedOption == 1 ? menu_selectedColour : menu_unSelectedColour;
        SET_COLOUR(c)
        simpleRectangleFont_drawStringScale(fonts_mainFont, "Options", ANIM(menu_optionsAnimation), 0.5f, 0.5f);
        
    c = menu_selectedOption == 2 ? menu_selectedColour : menu_unSelectedColour;
        SET_COLOUR(c)
        simpleRectangleFont_drawStringScale(fonts_mainFont, "Quit", ANIM(menu_quitAnimation), 0.5f, 0.5f);
    
    menu_time += step_elapsed;
    menu_timeNum = menu_time / menu_maxTime;
    if (menu_startGame) {
        if (menu_timeNum > MENU_SWITCH_ROOM_TIME) menu_processNextRoom();
    } else {
        if (menu_timeNum > MENU_END_TIME) {
            menu_timeNum = MENU_END_TIME;
            menu_time = MENU_END_TIME * menu_maxTime;
        }
    }
}

void menu_free(void) {
    menu_time = 0.0f;
    menu_timeNum = 0.0f;
    menu_startGame = 0;
    menu_selectedOption = 0;
}



void menu_bg(void) {
    SET_COLOUR(menu_bgTop);
        glVertex2f(MAIN_currentMinX,MAIN_currentMinY);
    SET_COLOUR(menu_bgBottom);
        glVertex2f(MAIN_currentMaxX,MAIN_currentMinY);
    SET_COLOUR(menu_bgTop);
        glVertex2f(MAIN_currentMaxX,MAIN_currentMaxY);
    SET_COLOUR(menu_bgBottom);
        glVertex2f(MAIN_currentMinX,MAIN_currentMaxY);
    
    SET_COLOUR(menu_fgTop);
        glVertex2f(menu_fgAnimation.current.x, menu_fgAnimation.current.y);
        glVertex2f(menu_fgAnimation.current.x + MAIN_WIDTH, menu_fgAnimation.current.y);
    SET_COLOUR(menu_fgBottom);
        glVertex2f(menu_fgAnimation.current.x + MAIN_WIDTH, menu_fgAnimation.current.y + MAIN_HEIGHT);
        glVertex2f(menu_fgAnimation.current.x, menu_fgAnimation.current.y + MAIN_HEIGHT);
}


void menu_changeSelectedOption(int by) {
    menu_selectedOption += by;
    menu_selectedOption = BETTER_MOD(menu_selectedOption, 3);
}

void menu_drawSelectBox(void) {
    SET_COLOUR(menu_selectBox);
        glVertex2f(menu_selectBoxAnimation.current.x, menu_selectBoxAnimation.current.y + MENU_OPTION_SPACING * menu_selectedOption);
        glVertex2f(menu_selectBoxAnimation.current.x + MENU_SELECT_BOX_WIDTH, menu_selectBoxAnimation.current.y + MENU_OPTION_SPACING * menu_selectedOption);
        glVertex2f(menu_selectBoxAnimation.current.x + MENU_SELECT_BOX_WIDTH, menu_selectBoxAnimation.current.y + MENU_OPTION_SPACING * (menu_selectedOption + 1) );
        glVertex2f(menu_selectBoxAnimation.current.x, menu_selectBoxAnimation.current.y + MENU_OPTION_SPACING * (menu_selectedOption + 1));
}




void menu_processNextRoom(void) {
    switch(menu_selectedOption) {

    case 0:
        step_setCurrentRoom(room_gameLogicRoom);
        break;
    
    case 1:
        step_setCurrentRoom(room_menuRoom);
        break;
        
    case 2:
        exit(0);
        break;
    
    }
}
