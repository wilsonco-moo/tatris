/*
 *  Tatris
 *  Copyright (C) April 2018, Daniel Wilson
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */




#ifndef DONT_USE_MAIN_HEADER
    #include "framework/framework.h"
#endif



// --------------------------------- CONSTANTS -----------------------------


// The values for the tatris piece preview box.
GLfloat TATRIS_PREVIEW_PIECE_X, TATRIS_PREVIEW_PIECE_Y;
GLfloat TATRIS_PREVIEW_BOX_X, TATRIS_PREVIEW_BOX_Y, TATRIS_PREVIEW_BOX_X2, TATRIS_PREVIEW_BOX_Y2;


// ------------------------------- ROOMS --------------------



struct gameRoom room_gameLogicRoom;
struct gameRoom room_menuRoom;
void room_setUpAreaConstantsForTitle(void);
void room_setUpAreaConstantsForGame(void);
void room_setUpAreaConstantsForMenu(void);

// ------------------------------------------------------------ TITLE SCREEN ---------------------------------------------------------------

void title_init(void);
void title_step(void);
void title_free(void);

void title_keyNormal(unsigned char key, int x, int y);
void title_keyNormalRelease(unsigned char key, int x, int y);
void title_keySpecial(int key, int x, int y);
void title_keySpecialRelease(int key, int x, int y);


// ------------------------------------------------------------ MENU SCREEN ---------------------------------------------------------------

void menu_init(void);
void menu_step(void);
void menu_free(void);

void menu_keyNormal(unsigned char key, int x, int y);
void menu_keyNormalRelease(unsigned char key, int x, int y);
void menu_keySpecial(int key, int x, int y);
void menu_keySpecialRelease(int key, int x, int y);


// ----------------------------------------------------------- GAME LOGIC -----------------------------------------------------------------

/*
 * This is the example implementation of a game room. These are simply the functions for the various parts of the game room.
 */

void gameLogic_init(void);
void gameLogic_step(void);
void gameLogic_free(void);

void gameLogic_keyNormal(unsigned char key, int x, int y);
void gameLogic_keyNormalRelease(unsigned char key, int x, int y);
void gameLogic_keySpecial(int key, int x, int y);
void gameLogic_keySpecialRelease(int key, int x, int y);

void gameLogic_allocationTest(void);




// ------------------ TATRIS BLOCK --------------------------------

struct tatrisBlock;
typedef struct tatrisBlock * TatrisBlock;
struct tatrisBlock {
    int x;
    int y;
    GLfloat * xOffset;
    GLfloat * yOffset;
    struct colour col;
    void ( * draw ) ( TatrisBlock block );
    void ( * setOffsets ) ( TatrisBlock block, GLfloat * xOffset, GLfloat * yOffset );
    void ( * resetOffsets ) ( TatrisBlock block );
    void ( * free ) ( TatrisBlock block );
};


TatrisBlock tatrisBlock_newDefault     (void);
TatrisBlock tatrisBlock_newPos         (int x, int y);
TatrisBlock tatrisBlock_newCol         (struct colour col);
TatrisBlock tatrisBlock_newPosCol      (int x, int y, struct colour col);
TatrisBlock tatrisBlock_newPosOffset   (int x, int y, GLfloat * xOffset, GLfloat * yOffset);
TatrisBlock tatrisBlock_newPosOffsetCol(int x, int y, GLfloat * xOffset, GLfloat * yOffset, struct colour col);




// ------------------- TATRIS BOARD -------------------------------


struct tatrisBoard;
typedef struct tatrisBoard * TatrisBoard;

struct tatrisBoard {
    TatrisBlock * blocks;
    GLfloat xOffset; // This is the draw offset, for drawing the blocks.
    GLfloat yOffset;
    int x; // This is the actual x and y position, for collision checking.
    int y;
    int width;
    int height;
    int area;
    void ( * addBlock ) (TatrisBoard board, TatrisBlock block);
    void ( * addBoard ) (TatrisBoard board, TatrisBoard other);
    void ( * removeBlock ) (TatrisBoard board, int x, int y);
    TatrisBlock ( * removeBlockNoFree ) (TatrisBoard board, int x, int y);
    TatrisBlock ( * getBlock ) (TatrisBoard board, int x, int y);
    void ( * draw ) (TatrisBoard board);
    void ( * free ) (TatrisBoard board);
    int ( * checkCollision ) (TatrisBoard board, TatrisBoard other);
    int ( * checkLine ) (TatrisBoard board, int line);
    void ( * removeLine ) (TatrisBoard board, int line);
    TatrisBoard ( * split ) (TatrisBoard board, int startLine, int endLine);
};

TatrisBoard tatrisBoard_new(int width, int height);







// ------------------ TATRIS PIECE GENERATION ---------------------------

#define TATRIS_PIECE_WIDTH 4
#define TATRIS_PIECE_HEIGHT 4
#define TATRIS_PIECE_ROTATIONS 4
#define TATRIS_PIECE_COUNT 28

TatrisBoard tatrisPiece_new(int type);

void tatrisPiece_initRandom(void);
int tatrisPiece_newRandomId(void);
int tatrisPiece_rotate(int type, int rotation);




// ------------------ DRAWING METHODS -----------------------------


#define DRAW_BG_TOP_RED   0.1f
#define DRAW_BG_TOP_GREEN 0.1f
#define DRAW_BG_TOP_BLUE  0.1f

#define DRAW_BG_BOTTOM_RED   0.2f
#define DRAW_BG_BOTTOM_GREEN 0.1f
#define DRAW_BG_BOTTOM_BLUE  0.5f


#define DRAW_FG_TOP_RED   0.25f
#define DRAW_FG_TOP_GREEN 0.1f
#define DRAW_FG_TOP_BLUE  0.5f

#define DRAW_FG_BOTTOM_RED   0.4f
#define DRAW_FG_BOTTOM_GREEN 0.4f
#define DRAW_FG_BOTTOM_BLUE  0.4f


struct colour bgTop;
struct colour bgBottom;
struct colour fgTop;
struct colour fgBottom;




void draw_init(void);
void draw_background(void);
void draw_tatrisBlock(GLfloat x, GLfloat y, struct colour col);




// ------------------------------------------------------- FONT STUFF -------------------------------------------------------------------------------

/*
 * This is the typedef for simple rectangle fonts.
 */
typedef GLfloat * SimpleRectangleFont;

/*
 * This draws a single character from the specified font, to the origin poing.
 */
void simpleRectangleFont_drawChar       (SimpleRectangleFont font, unsigned char character);
/*
 * This draws a single character from the specified font, at the specified position.
 */
void simpleRectangleFont_drawCharPos    (SimpleRectangleFont font, unsigned char character, GLfloat x, GLfloat y);
/*
 * This draws the specified null terminated string of characters, at the specified position.
 */
void simpleRectangleFont_drawString     (SimpleRectangleFont font, unsigned char * string,  GLfloat x, GLfloat y);
/*
 * This draws the specified null terminated string of characters, at the specified position.
 * Each character is scaled relatively by the specified amount.
 */
void simpleRectangleFont_drawStringScale(SimpleRectangleFont font, unsigned char * string,  GLfloat x, GLfloat y, GLfloat xScale, GLfloat yScale);
/*
 * This prints information about the given font. This information includes:
 *  > The min/max characters, and the number of characters covered by this font.
 *  > The number of rectangle shapes, per character, and the number of float values per character.
 *  > The width and height of each character, when drawn without changing the x and y scale.
 */
void simpleRectangleFont_printConfig(SimpleRectangleFont font);



// -------------------------------------------------- FONTS ------------------------------------------------

SimpleRectangleFont fonts_mainFont;




// ------------------------------------------- UTILITY MACROS -------------------------

#define SET_COLOUR(colour)                                   \
    glColor3f((colour).red, (colour).green, (colour).blue);




// ------------------------------------------- ANIMATION -------------------------------------

struct animationCoordinate {
    GLfloat x;
    GLfloat y;
};

struct animation {
    GLfloat min;
    GLfloat max;
    GLfloat smoothness;
    struct animationCoordinate start;
    struct animationCoordinate end;
    struct animationCoordinate current;
    GLfloat * xOrigin;
    GLfloat * yOrigin;
};

void animation_step(struct animation * anim, float time);

#define ANIM(anAnimation) anAnimation.current.x, anAnimation.current.y
