/*
 *  Tatris
 *  Copyright (C) April 2018, Daniel Wilson
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */



#ifndef DONT_USE_MAIN_HEADER
    #include "tatris.h"
#endif



struct gameRoom room_gameLogicRoom = {
    &gameLogic_init, &gameLogic_step, &gameLogic_free,
    
    &gameLogic_keyNormal,
    &gameLogic_keyNormalRelease,
    &gameLogic_keySpecial,
    &gameLogic_keySpecialRelease
};

struct gameRoom room_menuRoom = {
    &menu_init, &menu_step, &menu_free,
    
    &menu_keyNormal,
    &menu_keyNormalRelease,
    &menu_keySpecial,
    &menu_keySpecialRelease
};

struct gameRoom room_titleRoom = {
    &title_init, &title_step, &title_free,
    
    &title_keyNormal,
    &title_keyNormalRelease,
    &title_keySpecial,
    &title_keySpecialRelease
};



void room_recalculateAreaConstants(void);

/*
 * This initialises the configuration for the game.
 * This should be modified by the user to set the appropriate configuration and default game room.
 * This is run at the start of the game.
 */
void room_initConfig(void) {
    
    util_setUpdateFunction(&room_recalculateAreaConstants);
    
    
    room_setUpAreaConstantsForMenu();
    
    
    tatrisPiece_initRandom();
    step_setCurrentRoom(room_titleRoom);
}




void room_setUpAreaConstantsForMenu(void) {
    
    MAIN_WIDTH = 96;
    MAIN_HEIGHT = 128;
    
    MAIN_BORDER_LEFT   = 2.0f;
    MAIN_BORDER_RIGHT  = 2.0f;
    MAIN_BORDER_TOP    = 2.0f;
    MAIN_BORDER_BOTTOM = 2.0f;
    
    util_updateShape();
}

void room_setUpAreaConstantsForGame(void) {
    
    MAIN_WIDTH = 10;
    MAIN_HEIGHT = 20;
    
    MAIN_BORDER_LEFT   = 1.0f;
    MAIN_BORDER_RIGHT  = 7.0f;
    MAIN_BORDER_TOP    = 1.0f;
    MAIN_BORDER_BOTTOM = 1.0f;
    
    util_updateShape();
}

void room_setUpAreaConstantsForTitle(void) {
    MAIN_WIDTH = 80;
    MAIN_HEIGHT = 60;
    
    MAIN_BORDER_LEFT   = 4.0f;
    MAIN_BORDER_RIGHT  = 4.0f;
    MAIN_BORDER_TOP    = 4.0f;
    MAIN_BORDER_BOTTOM = 4.0f;
    
    util_updateShape();
}




void room_recalculateAreaConstants(void) {
    TATRIS_PREVIEW_BOX_X = MAIN_WIDTH  + 1.0f;
    TATRIS_PREVIEW_BOX_Y = 0.0f;
    TATRIS_PREVIEW_BOX_X2 = TATRIS_PREVIEW_BOX_X + 5.0f;
    TATRIS_PREVIEW_BOX_Y2 = TATRIS_PREVIEW_BOX_Y + 5.0f;
    
    
    TATRIS_PREVIEW_PIECE_X = TATRIS_PREVIEW_BOX_X + 0.5f;
    TATRIS_PREVIEW_PIECE_Y = TATRIS_PREVIEW_BOX_Y + 0.5f;
    
    draw_init();
}
