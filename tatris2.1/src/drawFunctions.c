/*
 *  Tatris
 *  Copyright (C) April 2018, Daniel Wilson
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */




#ifndef DONT_USE_MAIN_HEADER
    #include "tatris.h"
#endif








struct colour bgTop    = { DRAW_BG_TOP_RED,    DRAW_BG_TOP_GREEN,    DRAW_BG_TOP_BLUE    };
struct colour bgBottom = { DRAW_BG_BOTTOM_RED, DRAW_BG_BOTTOM_GREEN, DRAW_BG_BOTTOM_BLUE };
struct colour fgTop    = { DRAW_FG_TOP_RED,    DRAW_FG_TOP_GREEN,    DRAW_FG_TOP_BLUE    };
struct colour fgBottom = { DRAW_FG_BOTTOM_RED, DRAW_FG_BOTTOM_GREEN, DRAW_FG_BOTTOM_BLUE };




struct colour blockBorderColour = { 0.0f, 0.0f, 0.0f };

struct colour yellow = {1.0f, 1.0f, 0.0f};
struct colour green =  {0.0f, 1.0f, 0.0f};
struct colour cyan =   {0.0f, 1.0f, 1.0f};

struct colour previewBoxBottomColour;



void draw_init(void);
void draw_tatrisBlock(GLfloat x, GLfloat y, struct colour col);
void draw_tatrisBoardBackground(void);
void draw_mainBackground(void);
void draw_nextBlockBackground(void);




void draw_init(void) {
    GLfloat a = (TATRIS_PREVIEW_BOX_Y2 - TATRIS_PREVIEW_BOX_Y)/((GLfloat)MAIN_HEIGHT);
    GLfloat b = 1.0f - a;
    
    previewBoxBottomColour.red   =   fgTop.red * b +   fgBottom.red * a;
    previewBoxBottomColour.green = fgTop.green * b + fgBottom.green * a;
    previewBoxBottomColour.blue  =  fgTop.blue * b +  fgBottom.blue * a;
}


void draw_background(void) {
    draw_mainBackground();
    draw_tatrisBoardBackground();
    draw_nextBlockBackground();
}


void draw_mainBackground(void) {
    glColor3f(bgTop.red,bgTop.blue,bgTop.green);
        glVertex2f(MAIN_currentMinX,MAIN_currentMinY);
        glVertex2f(MAIN_currentMaxX,MAIN_currentMinY);
    glColor3f(bgBottom.red,bgBottom.green,bgBottom.blue);
        glVertex2f(MAIN_currentMaxX,MAIN_currentMaxY);
        glVertex2f(MAIN_currentMinX,MAIN_currentMaxY);
}


void draw_tatrisBoardBackground(void) {
    glColor3f(fgTop.red,fgTop.green,fgTop.blue);
        glVertex2f(0,0);
        glVertex2f(MAIN_WIDTH,0);
    glColor3f(fgBottom.red,fgBottom.green,fgBottom.blue);
        glVertex2f(MAIN_WIDTH, MAIN_HEIGHT);
        glVertex2f(0, MAIN_HEIGHT);
}


void draw_nextBlockBackground(void) {
    glColor3f(fgTop.red,fgTop.green,fgTop.blue);
        glVertex2f(TATRIS_PREVIEW_BOX_X,  TATRIS_PREVIEW_BOX_Y );
        glVertex2f(TATRIS_PREVIEW_BOX_X2, TATRIS_PREVIEW_BOX_Y );
    glColor3f(previewBoxBottomColour.red,previewBoxBottomColour.green,previewBoxBottomColour.blue);
        glVertex2f(TATRIS_PREVIEW_BOX_X2, TATRIS_PREVIEW_BOX_Y2);
        glVertex2f(TATRIS_PREVIEW_BOX_X , TATRIS_PREVIEW_BOX_Y2);
}



#define draw_gradientHalf 0.8f // Gradient multiplier (half)
#define draw_gradientFull 0.6f // Gradient multiplier (full)

#define draw_blockOuterBorder1 0.05f // Block border x1,y1
#define draw_blockOuterBorder2 0.95f // Block border x2,y2

#define draw_blockInnerBorder1 0.2f // Block border x1,y1
#define draw_blockInnerBorder2 0.8f // Block border x2,y2


void draw_tatrisBlock(GLfloat x, GLfloat y, struct colour col) {
    glColor3f(blockBorderColour.red, blockBorderColour.green, blockBorderColour.blue);
    glVertex2f(x,y);
    glVertex2f(x+1.0f,y);
    glVertex2f(x+1.0f,y+1.0f);
    glVertex2f(x,y+1.0f);
    
    glColor3f(col.red, col.green, col.blue);
    glVertex2f(x + draw_blockOuterBorder1, y + draw_blockOuterBorder1);
    glColor3f(col.red*draw_gradientHalf, col.green*draw_gradientHalf, col.blue*draw_gradientHalf);
    glVertex2f(x + draw_blockOuterBorder2,y + draw_blockOuterBorder1);
    glColor3f(col.red*draw_gradientFull, col.green*draw_gradientFull, col.blue*draw_gradientFull);
    glVertex2f(x + draw_blockOuterBorder2,y + draw_blockOuterBorder2);
    glColor3f(col.red*draw_gradientHalf, col.green*draw_gradientHalf, col.blue*draw_gradientHalf);
    glVertex2f(x + draw_blockOuterBorder1,y + draw_blockOuterBorder2);
    
    glColor3f(col.red*draw_gradientFull, col.green*draw_gradientFull, col.blue*draw_gradientFull);
    glVertex2f(x+draw_blockInnerBorder1,y+draw_blockInnerBorder1);
    glColor3f(col.red*draw_gradientHalf, col.green*draw_gradientHalf, col.blue*draw_gradientHalf);
    glVertex2f(x+draw_blockInnerBorder2,y+draw_blockInnerBorder1);
    glColor3f(col.red, col.green, col.blue);
    glVertex2f(x+draw_blockInnerBorder2,y+draw_blockInnerBorder2);
    glColor3f(col.red*draw_gradientHalf, col.green*draw_gradientHalf, col.blue*draw_gradientHalf);
    glVertex2f(x+draw_blockInnerBorder1,y+draw_blockInnerBorder2);
}



