/*
 *  Tatris
 *  Copyright (C) April 2018, Daniel Wilson
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */



#ifndef DONT_USE_MAIN_HEADER
    #include "tatris.h"
#endif


/*
 * This stores the main board in the background.
 */
TatrisBoard mainBoard;


// Some colour structs to represent RGB green and blue.
struct colour gr = { 0.0f, 1.0f, 0.0f };
struct colour bl  = { 0.0f, 0.0f, 1.0f };


/*
 * This stores the tatris board representing the piece that is currently falling.
 */
TatrisBoard b;

/*
 * This stores the tatris board representing the next piece, this is displayed on the right.
 */
TatrisBoard nextPiece;

/*
 * This stores the type of the next piece (shown on the right).
 */
int nextPieceType;
/*
 * This stores the type of the current falling piece.
 */
int currentPieceType;


/*
 * This is the default fall speed.
 */
GLfloat slowSpeed = 2.0f;
/*
 * This is the fall speed when the user presses the down key.
 */
GLfloat fastSpeed = 15.0f;

/*
 * This points to the current speed.
 */
GLfloat * currentSpeed = &slowSpeed;

/*
 * This is the drop speed that the main board does when we get a line.
 */
GLfloat boardDropSpeed = 2.5f;


/*
 * This stores the secondary boards: I.e: Those created when the main board gets
 * a line, and drops other boards.
 */
TatrisBoard * secondaryBoards;
/*
 * This stores the number of secondary boards currently shown.
 */
int secondaryBoardCount = 0;

/*
 * This stores our current score.
 */
int gameLogic_score = 0;
/*
 * This stores the number of lines we have done so far.
 */
int gameLogic_linesSoFar = 0;
/*
 * This stores the number of tatris pieces we have landed so far.
 */
int gameLogic_piecesSoFar = 0;
/*
 * This stores 1 if the game is running (we have not lost yet),
 * and 0 if the game is not running (we have lost).
 */
int gameLogic_gameRunning = 1;
/*
 * This allows us to set the score display only on the first frame.
 */
int notSetScoreYet = 1;


// ---------------- STUFF FOR SCORE -------------

char gameLogic_scoreStr[7];
char gameLogic_linesStr[7];
char gameLogic_piecesStr[7];



// --------------- FUNCTION DECLARATIONS ----------------------

int gameLogic_setRandomTatrisPiece(void);
void gameLogic_generateNewNextPiece(void);
void gameLogic_setTatrisPieceFromType(int type);
int gameLogic_setTatrisPiece(TatrisBoard new);
int gameLogic_checkCollisions(TatrisBoard * boardPointer, GLfloat speed);
void gameLogic_addNewPieceIfPossible(void);
void gameLogic_smoothHorizontalMovement(void);
void gameLogic_moveBlock(int xDiff);
void gameLogic_rotateBlock(int rotation);
int gameLogic_checkForLines(void);
void gameLogic_drawAndCollideSecondaryBoards(void);
void gameLogic_addSecondaryBoard(TatrisBoard board);
void gameLogic_removeSecondaryBoard(void);
void gameLogic_freeAllSecondaryBoards(void);
void gameLogic_updateScoreDisplay(void);
void gameLogic_recalculateScore(int lines);
void gameLogic_drawScoreAndText(void);
void gameLogic_drawYouLose(void);

// ---------------------------------------------------------




/*
 * Our normal key press event: Move/rotate appropriately when we press keys.
 */
void gameLogic_keyNormal(unsigned char key, int x, int y) {
    switch(key) {
    
    case 'a': case 'A':
        gameLogic_moveBlock(-1);
        break;
    
    case 'd': case 'D':
        gameLogic_moveBlock(1);
        break;
    
    case 'w': case 'W': case 'e': case 'E':
        gameLogic_rotateBlock(-1);
        break;
    
    case 'q': case 'Q':
        gameLogic_rotateBlock(1);
        break;
    
    case 's': case 'S':
        currentSpeed = &fastSpeed;
        break;
    
    case '\e':
        // Go back to the menu room when the user presses escape
        step_setCurrentRoom(room_menuRoom);
        break;
    }
}

/*
 * Our normal key release event: If we release the down (S) key, change to slow speed.
 */
void gameLogic_keyNormalRelease(unsigned char key, int x, int y) {
    switch(key) {
        
    case 's': case 'S':
        currentSpeed = &slowSpeed;
        break;
    
    }
}

/*
 * Our special key event: Rotate/move appropriately.
 */
void gameLogic_keySpecial(int key, int x, int y) {
    switch(key) {
    
    case GLUT_KEY_LEFT:
        gameLogic_moveBlock(-1);
        break;
    
    case GLUT_KEY_RIGHT:
        gameLogic_moveBlock(1);
        break;
    
    case GLUT_KEY_UP:
        gameLogic_rotateBlock(-1);
        break;
    
    case GLUT_KEY_DOWN:
        currentSpeed = &fastSpeed;
        break;
    }
}

/*
 * Our special key release event: Stop moving down when we release the down key.
 */
void gameLogic_keySpecialRelease(int key, int x, int y) {
    
    switch(key) {
        
    case GLUT_KEY_DOWN:
        currentSpeed = &slowSpeed;
        break;
    
        
    }
    
}

/*
 * Our room initiatialisation function.
 */
void gameLogic_init(void) {
    // Set up the area constants to show the game.
    room_setUpAreaConstantsForGame();
    // Allocate some memory for secondary boards, with enough elements to store the worst
    // case scenario that all lines become secondary boards. Use calloc so it is initialised
    // to NULL. Check the pointer.
    secondaryBoards = util_check(calloc(MAIN_HEIGHT, sizeof(TatrisBoard)));
    
    // Create a new tatris board, of correct width and height.
    mainBoard = tatrisBoard_new(MAIN_WIDTH, MAIN_HEIGHT);
    
    // Next we must create the falling tatris piece:
    // So we must manually create the next piece, as this is used for the current piece,
    // otherwise we would get a NULL current piece.
    gameLogic_generateNewNextPiece();
    
    // Now run the function to set the current piece to the next piece, and create a new next piece.
    gameLogic_setRandomTatrisPiece();
}


/*
 * Our room step function: Run each frame.
 */
void gameLogic_step(void) {
    // On the first frame, set the score display on the window title. This cannot be done in gameLogic_init(),
    // as glut has not been initialised yet there.
    if (notSetScoreYet) {
        notSetScoreYet = 0; gameLogic_updateScoreDisplay();
    }
    
    // Draw the background.
    draw_background();
    
    // Check if the currently falling board will collide with the main
    // board this frame, (if there is a currently falling tatris piece).
    if (gameLogic_checkCollisions(&b, *currentSpeed)) {
        // If there has been a collision, (and the currently falling boad has been added
        // to the main board), then check for lines. This function appropriately creates
        // secondary boards, and returns the number of lines that have been filled.
        // Use this number to recalculate the score.
        gameLogic_recalculateScore(gameLogic_checkForLines());
    }
    
    // Now draw the secondary boards, (if there are any), and add them to the main
    // board if they will collide this frame, (this moves them down if they won't collide).
    // Also draw the secondary boards.
    gameLogic_drawAndCollideSecondaryBoards();
    
    // If there is no currently falling tatris piece, and there are no secondary boards
    // active, then create a new tatris piece.
    gameLogic_addNewPieceIfPossible();
    
    // If there actually is a currently falling tatris piece:
    if (b != NULL) {
        // Run it's horizontal movement smoothing method.
        gameLogic_smoothHorizontalMovement();
        // Draw it.
        b->draw(b);
    }
    
    // Draw the main board.
    mainBoard->draw(mainBoard);
    // Draw the next piece.
    nextPiece->draw(nextPiece);
    
    
    gameLogic_drawScoreAndText();
    
    if (!gameLogic_gameRunning) {
        gameLogic_drawYouLose();
    }
}


/*
 * This is our memory de-allocation method.
 */
void gameLogic_free(void) {
    // Free all secondary boards.
    gameLogic_freeAllSecondaryBoards();
    
    // Free the currently falling board, if there is one.
    if (b != NULL) { b->free(b); b = NULL; }
    
    // Free the next piece
    nextPiece->free(nextPiece); nextPiece = NULL;
    
    // Free the main board.
    mainBoard->free(mainBoard);
    
    gameLogic_score = 0; // Reset the score
    gameLogic_linesSoFar = 0;
    gameLogic_gameRunning = 1; // Reset that the game is running.
    notSetScoreYet = 1; // Reset this so we set the window title next time.
    gameLogic_piecesSoFar = 0;
}





/*
 * The initial x position for falling tatris pieces.
 */
#define GAMELOGIC_TATRISPIECE_INITIAL_X 3
/*
 * The initial y position for falling tatris pieces.
 */
#define GAMELOGIC_TATRISPIECE_INITIAL_Y -2

/*
 * The horizontal movement smoothness, in proportion moved towards the new position per second.
 */
#define GAMELOGIC_TATRISPIECE_SMOOTHNESS 0.00001f


// ---------------------------------- METHODS TO CHANGE TATRIS PIECE ---------------------------------



/*
 * This function is for when we rotate a currently falling tatris piece, and need it to
 * switch to another mid-fall.
 * Here, we use tatrisPiece_new to generate a tatris piece of correct type.
 * Then we use gameLogic_setTatrisPiece to try to set the piece to the new type. If successful,
 * we set currentPieceType to the new type. This will not be successful if we rotate a tatris piece
 * to one that does not fit.
 */
void gameLogic_setTatrisPieceFromType(int type) {
    if (gameLogic_setTatrisPiece(tatrisPiece_new(type))) {
        currentPieceType = type;
    }
}




/*
 * This function is for when new falling tatris pieces are generated,
 * such as at the start, or when we drop a piece and need a new one.
 * 
 * We first use gameLogic_setTatrisPiece with the next tatris piece.
 * If this is successful, the current piece type is set.
 * We then generate a new next tatris piece, as if gameLogic_setTatrisPiece fails,
 * the existing next piece is freed anyway, (in gameLogic_setRandomTatrisPiece).
 * 
 * This returns 1 (true)  if setting the random tatris piece was successful,
 *      returns 0 (false) if setting the random tatris piece was unsuccessful: (i.e: there was a collision).
 */
int gameLogic_setRandomTatrisPiece(void) {
    int ret;
    if (ret = gameLogic_setTatrisPiece(nextPiece)) {
        currentPieceType = nextPieceType;
    } else {
        printf("YOU LOSE.\n");
    }
    
    gameLogic_generateNewNextPiece();
    
    return ret;
}

/*
 * This function allows us to generate a next piece.
 * All this function does is creates a new next piece, assigning it to nextPiece.
 * This must only be used if the nextPiece has already been re-assigned/used,
 * as this function ignores what is currently assigned to nextPiece.
 */
void gameLogic_generateNewNextPiece(void) {
    // Get the random next piece type, with tatrisPiece_newRandomId().
    nextPieceType = tatrisPiece_newRandomId();
    // Create a new next piece, using our random next piece type. Assign this to nextPiece.
    nextPiece = tatrisPiece_new(nextPieceType);
    // Now set the next piece's position to the correct position.
    nextPiece->x       = TATRIS_PREVIEW_PIECE_X;
    nextPiece->y       = TATRIS_PREVIEW_PIECE_Y;
    nextPiece->xOffset = TATRIS_PREVIEW_PIECE_X;
    nextPiece->yOffset = TATRIS_PREVIEW_PIECE_Y;
}


/*
 * This function changes the currently falling tatris piece.
 * This is done in a generic way, that works for both switching tatris piece mid-fall, and generating a new one.
 *   > First it calculates the position the new piece should go. This is:
 *      - The position of the currently falling tatris piece, if there is one
 *      - Otherwise we use GAMELOGIC_TATRISPIECE_INITIAL_X and GAMELOGIC_TATRISPIECE_INITIAL_Y.
 *   > Next we check if the new piece collides with anything.
 *      - If it is colliding with the main board, the new piece is freed. This is for the circumstance where
 *        we switch to a new piece mid-fall, and the new piece does not fit.
 *      - If it is not colliding, the old piece is freed (if there is one), the currently falling piece is
 *        set to the new one, and it's offsets are set.
 * Returns: 1 (true) if successful, 0 (false) if we failed to set the tatris piece.
 */
int gameLogic_setTatrisPiece(TatrisBoard new) {
    int initialX, initialY;
    GLfloat initialXOffset, initialYOffset;
    
    if (b == NULL) {
        initialX = GAMELOGIC_TATRISPIECE_INITIAL_X; initialY = GAMELOGIC_TATRISPIECE_INITIAL_Y;
        initialXOffset = GAMELOGIC_TATRISPIECE_INITIAL_X; initialYOffset = GAMELOGIC_TATRISPIECE_INITIAL_Y;
    } else {
        initialX = b->x; initialY = b->y;
        initialXOffset = b->xOffset; initialYOffset = b->yOffset;
    }
    
    new->x = initialX;  new->y = initialY;
    
    if (new->checkCollision(new, mainBoard)) {
        new->free(new);
        return 0;
    } else {
        if (b != NULL) b->free(b);
        b = new;
        b->xOffset = initialXOffset; b->yOffset = initialYOffset;
        return 1;
    }
}




// ---------------------------------- COLLISION CHECKING AND LINE CALCULATIONS -------------------------





/*
 * This function runs a movement step on the specified pointer to a tatris board,
 * using the specified speed.
 * If this board is now colliding with the main board, then it is added to the
 * main board, and the pointer is nullified.
 * Returns: 1 if there was a collision, 0 if there was no collision.
 */
int gameLogic_checkCollisions(TatrisBoard * boardPointer, GLfloat speed) {
    
    TatrisBoard board = *boardPointer;
    
    if (board != NULL) {
        
        GLfloat oldY = board->yOffset;
        board->yOffset += step_elapsed * speed;
        
        board->y = ceil(board->yOffset);
        
        if (board->checkCollision(board, mainBoard)) {
            board->yOffset = oldY;
            board->y = ceil(board->yOffset);
            mainBoard->addBoard(mainBoard, board);
            *boardPointer = NULL;
            return 1;
        }
        
        return 0;
    }
    
    return 0; // Return zero if board is NULL.
}



/*
 * Does all necessary operations to remove lines that have been filled.
 * Returns the number of lines that have been filled/removed.
 */
int gameLogic_checkForLines(void) {
    uint8_t * linesFilled = util_check(malloc(MAIN_HEIGHT));
    
    int i = 0, linesFilledCount = 0; while(i < MAIN_HEIGHT) {
        int f = mainBoard->checkLine(mainBoard,i);
        if (f) {
            mainBoard->removeLine(mainBoard,i);
            linesFilledCount++;
        }
        linesFilled[i] = f;
        i++;
    }
    
    // FILLED  -1
    // #        1
    // #        2
    // FILLED   3
    // #        4
    // #        5
    // FILLED   6
    
    int lastFilledLine = -1;
    i = 0; while(i < MAIN_HEIGHT) {
        
        if (linesFilled[i]) {
            if (i - lastFilledLine > 1) {
                gameLogic_addSecondaryBoard(mainBoard->split(mainBoard, lastFilledLine + 1, i));
            }
            lastFilledLine = i;
        }
        
        i++;
    }
    
    free(linesFilled);
    
    return linesFilledCount;
}




// ----------------------------------- SIMPLE UTILITY METHODS -----------------------------------------------




/*
 * This runs gameLogic_setRandomTatrisPiece() if there is no currently falling
 * tatris piece, and there are no secondary boards.
 */
void gameLogic_addNewPieceIfPossible(void) {
    if (b == NULL && secondaryBoardCount == 0 && gameLogic_gameRunning) {
        gameLogic_gameRunning = gameLogic_setRandomTatrisPiece();
    }
}

/*
 * This manages the x offset of the currently falling tatris piece, to smooth out
 * it's horizontal movement.
 */
void gameLogic_smoothHorizontalMovement(void) {
    int x = b->x;
    b->xOffset = (b->xOffset-x)*pow(GAMELOGIC_TATRISPIECE_SMOOTHNESS,step_elapsed) + x;
}

/*
 * This moves the currently falling block horizontally by the specified amount,
 * if there is one, if possible.
 */
void gameLogic_moveBlock(int xDiff) {
    if (b == NULL) return; // Do nothing if there is no currently falling piece.
    
    b->x += xDiff; // Add the xDiff.
    if (b->checkCollision(b, mainBoard)) { // If there is a collision:
        b->x -= xDiff; // Then undo the adding of the offset.
    }
}

/*
 * This rotates the currently falling tatris piece, if possible, if there is one.
 */
void gameLogic_rotateBlock(int rotation) {
    if (b == NULL) return;
    
    gameLogic_setTatrisPieceFromType(tatrisPiece_rotate(currentPieceType, rotation));
}







// ------------------------------- SECONDARY BOARD MANAGEMENT -----------------------------------


/*
 * This adds a secondary board to the stack of secondary boards.
 * Here, we can conveniently use a stack, because secondary boards are created starting from the top,
 * and should always be removed starting from the bottom.
 */
void gameLogic_addSecondaryBoard(TatrisBoard board) {
    
    secondaryBoards[secondaryBoardCount] = board;
    secondaryBoardCount++;
}

/*
 * To remove a secondary board, we do not need to know which board to remove, as it will always be
 * the furthest-down secondary board. We also do not need to free it, as that is done in
 * gameLogic_drawAndCollideSecondaryBoards().
 */
void gameLogic_removeSecondaryBoard(void) {
    secondaryBoardCount--;
}


/*
 * This method, (for each of the secondary boards, iterated in reverse order, so the stack works correctly), does:
 *  > Moves and checks collisions with the main board, adding to the mainboard and freeing if we are colliding.
 *  > Removes from the secondary board stack if we are colliding.
 */
void gameLogic_drawAndCollideSecondaryBoards(void) {
    
    TatrisBoard * t = secondaryBoards + secondaryBoardCount - 1;
    TatrisBoard * start = t;
    TatrisBoard * end = secondaryBoards - 1;
    
    while(t != end) {
        if (gameLogic_checkCollisions(t, boardDropSpeed)) {
            gameLogic_removeSecondaryBoard();
            if (t != start) {
                printf("!! WARNING: BOARD COMPONENT REMOVED WHEN IT IS NOT THE MOST RECENT ONE CREATED. The game will now probably have a segmentation fault.\n");
            }
            start--;
        } else {
            (*t)->draw(*t);
        }
        t--;
    }
}

/*
 * This function frees all of the secondary boards (if there are any),
 * and frees the array that stores the secondary boards.
 */
void gameLogic_freeAllSecondaryBoards(void) {
    TatrisBoard * s = secondaryBoards;
    TatrisBoard * end = s + secondaryBoardCount;
    while(s != end) {
        TatrisBoard o = *(s++);
        o->free(o);
    }
    free(secondaryBoards);
    secondaryBoardCount = 0;
}




// ------------------------------------------------- SCORE CALCULATION --------------------------------------------



/*
 * This calculates the new score, based on the number of lines we have just removed.
 * This automatically runs gameLogic_updateScoreDisplay().
 */
void gameLogic_recalculateScore(int lines) {
    
    // This runs each time a piece lands, so increment the piece counter.
    gameLogic_piecesSoFar++;
    
    switch(lines) {
        case 4: gameLogic_score += 100;
        case 3: gameLogic_score += 50;
        case 2: gameLogic_score += 30;
        case 1: gameLogic_score += 20;
    }
    
    gameLogic_linesSoFar += lines;
    gameLogic_updateScoreDisplay();
}


#define TITLE_MAX_LENGTH 100

/*
 * This updates the score shown on the window's title bar, based on the current
 * gameLogic_score.
 */
void gameLogic_updateScoreDisplay(void) {
    char * newTitle = util_check(malloc(TITLE_MAX_LENGTH*sizeof(char)));
    snprintf(newTitle,TITLE_MAX_LENGTH, "Tatris, Current score: %d, lines so far: %d", gameLogic_score, gameLogic_linesSoFar);
    glutSetWindowTitle(newTitle);
    free(newTitle);
    
    snprintf(gameLogic_scoreStr, 7, "%6d", gameLogic_score);
    snprintf(gameLogic_linesStr, 7, "%6d", gameLogic_linesSoFar);
    snprintf(gameLogic_piecesStr, 7, "%6d", gameLogic_piecesSoFar);
}







    

/* SOME TEST CODE
    int ix = 0, iy = 4; while(iy < TATRIS_HEIGHT) {
        while(ix < TATRIS_WIDTH) {
            if (ix != 4 && (ix != 3 || iy%4 > 1)) {
                mainBoard->addBlock(mainBoard, tatrisBlock_newPos(ix,iy));  TESTING
            }
            
            ix++;
        }
        ix = 0; iy++;
    }
*/

//gameLogic_updateScoreDisplay();



void gameLogic_allocationTest(void) {
    while(1) {
        gameLogic_init();
        int i = 0; while(i < 20) {
            gameLogic_addSecondaryBoard(tatrisBoard_new(10,10));
            i++;
        }
        i = 0; while(i < 1000000) {
            i++;
        }
        gameLogic_free();
    }
}





// ------------------------------------------------ TEXT, SCORE AND YOU LOSE MESSAGES ---------------------------------------------------


#define GAMELOGIC_YOU_LOSE_SCALE 0.4f
#define GAMELOGIC_YOU_LOSE_SPACING 5.6f
#define GAMELOGIC_YOU_LOSE_SHADOW_SPACING 0.2f


struct colour gameLogic_youLoseBg = {0.3f, 0.0f, 0.0f};
struct colour gameLogic_youLoseFg = {1.0f, 0.0f, 0.0f};
struct colour gameLogic_pressEscapeToReturnToMainMenu = {1.0f, 1.0f, 1.0f};

void gameLogic_drawYouLose(void) {
    
    GLfloat y = MAIN_HEIGHT * 0.5f - GAMELOGIC_YOU_LOSE_SPACING,
           hw = MAIN_WIDTH * 0.5f,
         xYou = hw - GAMELOGIC_YOU_LOSE_SCALE * 12.0f,
        xLose = hw - GAMELOGIC_YOU_LOSE_SCALE * 16.0f;
    
    SET_COLOUR(gameLogic_youLoseBg)
        simpleRectangleFont_drawStringScale(fonts_mainFont, "YOU", xYou + GAMELOGIC_YOU_LOSE_SHADOW_SPACING, y + GAMELOGIC_YOU_LOSE_SHADOW_SPACING, GAMELOGIC_YOU_LOSE_SCALE, GAMELOGIC_YOU_LOSE_SCALE);
        simpleRectangleFont_drawStringScale(fonts_mainFont, "LOSE", xLose + GAMELOGIC_YOU_LOSE_SHADOW_SPACING, y + GAMELOGIC_YOU_LOSE_SHADOW_SPACING + GAMELOGIC_YOU_LOSE_SPACING, GAMELOGIC_YOU_LOSE_SCALE, GAMELOGIC_YOU_LOSE_SCALE);
    
    SET_COLOUR(gameLogic_youLoseFg)
        simpleRectangleFont_drawStringScale(fonts_mainFont, "YOU", xYou, y, GAMELOGIC_YOU_LOSE_SCALE, GAMELOGIC_YOU_LOSE_SCALE);
        simpleRectangleFont_drawStringScale(fonts_mainFont, "LOSE", xLose, y + GAMELOGIC_YOU_LOSE_SPACING, GAMELOGIC_YOU_LOSE_SCALE, GAMELOGIC_YOU_LOSE_SCALE);
    
    SET_COLOUR(gameLogic_pressEscapeToReturnToMainMenu)
        simpleRectangleFont_drawStringScale(fonts_mainFont, "Press [ESC] to return to the main menu.", 0.0f, MAIN_HEIGHT - 0.05f * 14.0f, 0.05f, 0.05f);
        
}


struct colour gameLogic_scoreTitleColour   = {0.5f, 1.0f, 0.5f};
struct colour gameLogic_scoreNumbersColour = {0.0f, 1.0f, 0.0f};

#define GAMELOGIC_SCORESCALE 0.1f
#define GAMELOGIC_SCORESPACING 1.4f
#define GAMELOGIC_SCORE_LINE_SPACING 2.0f
#define GAMELOGIC_SCOREVOFFSET 0.5f

void gameLogic_drawScoreAndText(void) {
    GLfloat y = TATRIS_PREVIEW_BOX_Y2 + GAMELOGIC_SCOREVOFFSET;
    
    SET_COLOUR(gameLogic_scoreTitleColour)
        simpleRectangleFont_drawStringScale(fonts_mainFont, " Score", TATRIS_PREVIEW_BOX_X, y, GAMELOGIC_SCORESCALE, GAMELOGIC_SCORESCALE);
        y += GAMELOGIC_SCORESPACING;
    
    SET_COLOUR(gameLogic_scoreNumbersColour)
        simpleRectangleFont_drawStringScale(fonts_mainFont, gameLogic_scoreStr, TATRIS_PREVIEW_BOX_X, y, GAMELOGIC_SCORESCALE, GAMELOGIC_SCORESCALE);
        y += GAMELOGIC_SCORE_LINE_SPACING;
    
    
    
    SET_COLOUR(gameLogic_scoreTitleColour)
        simpleRectangleFont_drawStringScale(fonts_mainFont, " Lines", TATRIS_PREVIEW_BOX_X, y, GAMELOGIC_SCORESCALE, GAMELOGIC_SCORESCALE);
        y += GAMELOGIC_SCORESPACING;
    
    SET_COLOUR(gameLogic_scoreNumbersColour)
        simpleRectangleFont_drawStringScale(fonts_mainFont, gameLogic_linesStr, TATRIS_PREVIEW_BOX_X, y, GAMELOGIC_SCORESCALE, GAMELOGIC_SCORESCALE);
        y += GAMELOGIC_SCORE_LINE_SPACING;
    
    
    
    SET_COLOUR(gameLogic_scoreTitleColour)
        simpleRectangleFont_drawStringScale(fonts_mainFont, "Pieces", TATRIS_PREVIEW_BOX_X, y, GAMELOGIC_SCORESCALE, GAMELOGIC_SCORESCALE);
        y += GAMELOGIC_SCORESPACING;
    
    SET_COLOUR(gameLogic_scoreNumbersColour)
        simpleRectangleFont_drawStringScale(fonts_mainFont, gameLogic_piecesStr, TATRIS_PREVIEW_BOX_X, y, GAMELOGIC_SCORESCALE, GAMELOGIC_SCORESCALE);
        y += GAMELOGIC_SCORE_LINE_SPACING;
}
