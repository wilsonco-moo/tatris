/*
 *  Tatris
 *  Copyright (C) April 2018, Daniel Wilson
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */




#ifndef DONT_USE_MAIN_HEADER
    #include "tatris.h"
#endif


#define SUB_BORD(val) (val) - 4.0f


/*
 * This is the menu.
 */

GLfloat title_time = 0.0f;
GLfloat title_timeMax = 10.0f;
GLfloat title_timeNum = 0.0f;




void title_keyNormal(unsigned char key, int x, int y) {
}

void title_keyNormalRelease(unsigned char key, int x, int y) {
}

void title_keySpecial(int key, int x, int y) {
}

void title_keySpecialRelease(int key, int x, int y) {
}


void title_init(void) {
    room_setUpAreaConstantsForTitle();
}

void title_free(void) {
    title_time = 0.0f;
    title_timeNum = 0.0f;
}



void title_bg(void);


#define TITLE_END_TIME 0.85f

// ------------------------ ANIMATION DECLARATIONS ------------------------------

struct colour title_wilsoncoFg = {1.0f, 1.0f, 1.0f};
struct animation title_wilsoncoAnimation = {
    0.15f,             // Min time
    TITLE_END_TIME,    // Max time
    50.0f,             // Smoothness
    {8.0f, -14.0f},    // Start
    {8.0f, 0.0f},      // End
    {0.0f, 0.0f},      // Current
    NULL,              // xOrigin
    &MAIN_currentMinY  // yOrigin
};

struct colour title_superTatris = {0.0f, 1.0f, 0.0f};
struct animation title_superTatrisAnimation = {
    0.2f,               // Min time
    TITLE_END_TIME,     // Max time
    150.0f,             // Smoothness
    {0.0f, 14.0f},      // Start
    {16.0f, 14.0f},     // End
    {0.0f, 0.0f},       // Current
    &MAIN_currentMaxX,  // xOrigin
    NULL                // yOrigin
};

struct colour title_1995 = {1.0f, 0.2f, 0.2f};
struct animation title_1995Animation = {
    0.3f,                // Min time
    TITLE_END_TIME,      // Max time
    80.0f,               // Smoothness
    {-62.0f, 22.0f},     // Start
    {0.0f, 22.0f},       // End
    {0.0f, 0.0f},        // Current
    &MAIN_currentMinX,   // xOrigin
    NULL                 // yOrigin
};
struct animation title_ReleaseAnimation = {
    0.3f,               // Min time
    TITLE_END_TIME,     // Max time
    80.0f,              // Smoothness
    {-28.0f, 22.0f},    // Start
    {34.0f, 22.0f},     // End
    {0.0f, 0.0f},       // Current
    &MAIN_currentMinX,  // xOrigin
    NULL                // yOrigin
};




struct colour title_version = {0.0f, 1.0f, 1.0f};
struct animation title_versionAnimation = {
    0.4f,               // Min time
    TITLE_END_TIME,     // Max time
    80.0f,              // Smoothness
    {0.0f, 36.0f},      // Start
    {26.0f, 36.0f},     // End
    {0.0f, 0.0f},       // Current
    &MAIN_currentMaxX,  // xOrigin
    NULL                // yOrigin
};
struct animation title_twoPointOneAnimation = {
    0.4f,               // Min time
    TITLE_END_TIME,     // Max time
    80.0f,              // Smoothness
    {30.0f, 31.0f},     // Start
    {56.0f, 31.0f},     // End
    {0.0f, 0.0f},       // Current
    &MAIN_currentMaxX,  // xOrigin
    NULL                // yOrigin
};


struct colour title_ultimate = {1.0f, 0.5f, 0.0f};
struct animation title_ultimateAnimation = {
    0.5f,               // Min time
    TITLE_END_TIME,     // Max time
    100.0f,             // Smoothness
    {-32.0f, 45.0f},    // Start
    {16.0f, 45.0f},     // End
    {0.0f, 0.0f},       // Current
    &MAIN_currentMinX,  // xOrigin
    NULL                // yOrigin
};


struct colour title_professional = {1.0f, 1.0f, 0.0f};
struct animation title_professionalAnimation = {
    0.55f,              // Min time
    TITLE_END_TIME,     // Max time
    140.0f,             // Smoothness
    {-48.0f, 52.0f},    // Start
    {0.0f, 52.0f},      // End
    {0.0f, 0.0f},       // Current
    &MAIN_currentMinX,  // xOrigin
    NULL                // yOrigin
};


struct colour title_edition = {1.0f, 0.7f, 0.0f};
struct animation title_editionAnimation = {
    0.6f,               // Min time
    TITLE_END_TIME,     // Max time
    80.0f,              // Smoothness
    {0.0f, 48.5f},      // Start
    {52.0f, 48.5f},     // End
    {0.0f, 0.0f},       // Current
    &MAIN_currentMaxX,  // xOrigin
    NULL                // yOrigin
};








void title_step(void) {
    
    animation_step(&title_wilsoncoAnimation, title_timeNum);
    animation_step(&title_superTatrisAnimation, title_timeNum);
    animation_step(&title_1995Animation, title_timeNum);
    animation_step(&title_ReleaseAnimation, title_timeNum);
    animation_step(&title_versionAnimation, title_timeNum);
    animation_step(&title_twoPointOneAnimation, title_timeNum);
    animation_step(&title_ultimateAnimation, title_timeNum);
    animation_step(&title_professionalAnimation, title_timeNum);
    animation_step(&title_editionAnimation, title_timeNum);
    
    title_bg();
    
    float w = MAIN_WIDTH * 0.5f,
          h = MAIN_HEIGHT * 0.5f;
    
    SET_COLOUR(title_wilsoncoFg)
        simpleRectangleFont_drawString(fonts_mainFont, "Wilsonco", ANIM(title_wilsoncoAnimation));
    
    SET_COLOUR(title_superTatris)
        simpleRectangleFont_drawStringScale(fonts_mainFont, "Super-Tatris", ANIM(title_superTatrisAnimation), 0.5f, 0.5f);    
    
    SET_COLOUR(title_1995)
        simpleRectangleFont_drawString(fonts_mainFont, "1995", ANIM(title_1995Animation));
        simpleRectangleFont_drawStringScale(fonts_mainFont, "Release", ANIM(title_ReleaseAnimation), 0.5f, 0.5f);
    
    SET_COLOUR(title_version)
        simpleRectangleFont_drawStringScale(fonts_mainFont, "Version", ANIM(title_versionAnimation), 0.5f, 0.5f);
        simpleRectangleFont_drawString(fonts_mainFont, "2.1", ANIM(title_twoPointOneAnimation));
    
    SET_COLOUR(title_ultimate)
        simpleRectangleFont_drawStringScale(fonts_mainFont, "Ultimate", ANIM(title_ultimateAnimation), 0.5f, 0.5f);
    SET_COLOUR(title_professional)
        simpleRectangleFont_drawStringScale(fonts_mainFont, "Professional", ANIM(title_professionalAnimation), 0.5f, 0.5f);
    SET_COLOUR(title_edition)
        simpleRectangleFont_drawStringScale(fonts_mainFont, "edition", ANIM(title_editionAnimation), 0.5f, 0.5f);
    
    title_time += step_elapsed;
    title_timeNum = title_time / title_timeMax;
    if (title_time > title_timeMax) step_setCurrentRoom(room_menuRoom);
}





void title_bg(void) {
    SET_COLOUR(bgTop)
        glVertex2f(MAIN_currentMinX,MAIN_currentMinY);
        glVertex2f(MAIN_currentMinX,MAIN_currentMaxY);
    SET_COLOUR(bgBottom)
        glVertex2f(MAIN_currentMaxX,MAIN_currentMaxY);
        glVertex2f(MAIN_currentMaxX,MAIN_currentMinY);
}





