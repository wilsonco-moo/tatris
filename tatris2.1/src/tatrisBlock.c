/*
 *  Tatris
 *  Copyright (C) April 2018, Daniel Wilson
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */




#ifndef DONT_USE_MAIN_HEADER
    #include "tatris.h"
#endif



void tatrisBlock_draw(TatrisBlock block) {
    draw_tatrisBlock(block->x + (block->xOffset != NULL ? *block->xOffset : 0.0f),
                     block->y + (block->yOffset != NULL ? *block->yOffset : 0.0f),
                     block->col);
}

void tatrisBlock_setOffsets(TatrisBlock block, GLfloat * xOffset, GLfloat * yOffset) {
    block->xOffset = xOffset;
    block->yOffset = yOffset;
}

void tatrisBlock_resetOffsets(TatrisBlock block) {
    block->xOffset = NULL;
    block->yOffset = NULL;
}

void tatrisBlock_free(TatrisBlock block) {
    free(block);
}


#define TATRIS_BLOCK_DEFAULT_COLOUR b->col.red = 0.6f; b->col.green = 0.8f;  b->col.blue = 0.0f;


TatrisBlock tatrisBlock_internalNew(void) {
    TatrisBlock b = util_check(malloc(sizeof(struct tatrisBlock)));
    b->draw = &tatrisBlock_draw;
    b->setOffsets = &tatrisBlock_setOffsets;
    b->resetOffsets = &tatrisBlock_resetOffsets;
    b->free = &tatrisBlock_free;
    TATRIS_BLOCK_DEFAULT_COLOUR
    return b;
}

TatrisBlock tatrisBlock_newDefault(void) {
    TatrisBlock b = tatrisBlock_internalNew();
    b->x = 0; b->y = 0;
    b->xOffset = NULL; b->yOffset = NULL;
    TATRIS_BLOCK_DEFAULT_COLOUR
    return b;
}

TatrisBlock tatrisBlock_newPos(int x, int y) {
    TatrisBlock b = tatrisBlock_internalNew();
    b->x = x; b->y = y;
    b->xOffset = NULL; b->yOffset = NULL;
    TATRIS_BLOCK_DEFAULT_COLOUR
    return b;
}

TatrisBlock tatrisBlock_newPosCol(int x, int y, struct colour col) {
    TatrisBlock b = tatrisBlock_internalNew();
    b->x = x; b->y = y;
    b->xOffset = NULL; b->yOffset = NULL;
    b->col = col;
    return b;
}

TatrisBlock tatrisBlock_newPosOffset(int x, int y, GLfloat * xOffset, GLfloat * yOffset) {
    TatrisBlock b = tatrisBlock_internalNew();
    b->x = x; b->y = y;
    b->xOffset = xOffset; b->yOffset = yOffset;
    TATRIS_BLOCK_DEFAULT_COLOUR
    return b;
}

TatrisBlock tatrisBlock_newCol(struct colour col) {
    TatrisBlock b = tatrisBlock_internalNew();
    b->x = 0.0f; b->y = 0.0f;
    b->xOffset = NULL; b->yOffset = NULL;
    b->col = col;
    return b;
}

TatrisBlock tatrisBlock_newPosOffsetCol(int x, int y, GLfloat * xOffset, GLfloat * yOffset, struct colour col) {
    TatrisBlock b = tatrisBlock_internalNew();
    b->x = x; b->y = y;
    b->xOffset = xOffset; b->yOffset = yOffset;
    b->col = col;
    return b;
}
