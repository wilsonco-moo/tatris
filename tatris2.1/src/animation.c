/*
 *  Tatris
 *  Copyright (C) April 2018, Daniel Wilson
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */



#ifndef DONT_USE_MAIN_HEADER
    #include "tatris.h"
#endif




void animation_step(struct animation * anim, GLfloat time) {
    GLfloat dist;
    if (time >= anim->min && time < anim->max) {
        dist = 0.0f;
    } else {
        GLfloat minDist = time - anim->min,
                maxDist = time - anim->max;
        minDist = minDist < 0 ? -minDist : minDist;
        maxDist = maxDist < 0 ? -maxDist : maxDist;
        dist = minDist < maxDist ? minDist : maxDist;
    }
    
    GLfloat amount = 1.0f - dist*dist*anim->smoothness;
    amount = amount < 0.0f ? 0.0f : amount;
    
    GLfloat pos; GLfloat * origin;
    
    origin = anim->xOrigin;
    pos = anim->start.x + (origin == NULL ? 0.0f : *origin);
    anim->current.x = (anim->end.x - pos) * amount + pos;
    
    origin = anim->yOrigin;
    pos = anim->start.y + (origin == NULL ? 0.0f : *origin);
    anim->current.y = (anim->end.y - pos) * amount + pos;
}
