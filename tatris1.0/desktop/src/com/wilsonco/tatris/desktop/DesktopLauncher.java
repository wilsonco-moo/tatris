package com.wilsonco.tatris.desktop;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.wilsonco.tatris.Common;
import com.wilsonco.tatris.Tatris;

public class DesktopLauncher {
    public static void main (String[] arg) {
        LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        config.title = "Tatris";
        config.width = Tatris.gameWidth;
        config.height = Tatris.gameHeight;
        config.resizable = true;
        config.vSyncEnabled = true;
        /*System.out.println("STARTED");
        String[] s = new String[]{
        "..................",
        ".###.###.###.#####",
        ".#...#.#.#.#.#.#.#",
        ".###.###.###.#.#.#",
        "...#.#...#.#.#.#.#",
        ".###.#...#.#.#.#.#",
        "..................",
        "..##############..",
        "..................",
        "..##############..",
        "..................",   ROTATION TESTER - KEPT BECAUSE SPAM.
        "..##############..",
        "..................",
        "..##############..",
        "..................",
        "..##############..",
        "..................",
        ".................."
        };
        boolean[][] b = Common.convertStringImage(s);
        Common.print2dBooleanArray(b);
        System.out.println("CONVERTED");
        b = Common.rotate2dBooleanArray(b,1);
        System.out.println("ROTATED");
        Common.print2dBooleanArray(b);
        System.out.println("PRINTED");*/

        
        new LwjglApplication(new Tatris(), config);
        
    }
}