/*
 *  Tatris
 *  Copyright (C) April 2016, Daniel Wilson
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
package com.wilsonco.tatris;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 *
 * @author WILSON
 */
public class BrickConfig {
    String[] image;
    int[] offsetX;
    int[] offsetY;
    int width;
    int height;
    Block defaultBlockType;
    String blockTypeStr;
    TatrisWorldGrid grid;
    BrickConfig (TatrisWorldGrid grid, String[] image, String blockType) {
        offsetX = new int[]{0,-1,0,1};
        offsetY = new int[]{0,0,1,0};
        this.image = image;
        blockTypeStr = blockType;
        this.grid = grid;
        defaultBlockType = grid.blockTypes.get(blockTypeStr);
    }
    
    BrickConfig (TatrisWorldGrid grid, String fname) {
        //offsetX = new int[]{0,-1,0,1};
        //offsetY = new int[]{0,0,1,0};
        this.grid = grid;
        readInConfFile(fname);
    }
    
    private void readInConfFile(String fname) {
        try {
            String[] readIn = splitStr(Gdx.files.internal("bricks/"+fname+".tat").readString());
            String[] s = readIn[0].split("x");
            width = Integer.parseInt(s[0]);
            height = Integer.parseInt(s[1]);
            int i = 0; image = new String[height];
            while (i < height) { image[i] = readIn[i+1]; i++; }
            s = readIn[1+height].split(":")[1].split(",");
            offsetX = new int[4]; i = 0; while (i < 4){offsetX[i]=Integer.parseInt(s[i]);i++;}
            s = readIn[2+height].split(":")[1].split(",");
            offsetY = new int[4]; i = 0; while (i < 4){offsetY[i]=Integer.parseInt(s[i]);i++;}
            int num = readIn.length-3-height;
            
            blockTypeStr = readIn[readIn.length-1];
            defaultBlockType = grid.blockTypes.get(blockTypeStr);
            
            System.out.println("Width: "+width);
            System.out.println("Height: "+height);
            System.out.println("OffsetsX: "); for (int e : offsetX) System.out.println(e);
            System.out.println("OffsetsY: "); for (int e : offsetY) System.out.println(e);
            System.out.println("Img: "); for (String e : image) System.out.println(e);
            System.out.println("Block type: "+blockTypeStr);
            
        } catch (com.badlogic.gdx.utils.GdxRuntimeException e ) {
            System.out.println("File no no existing "+fname);
        } catch (Exception e) {
            System.out.println("File no no written properly "+fname);
            e.printStackTrace();
        }
    }
    
    private String[] splitStr(String s) {
        return s.replace("\r","").replace("\n","").split(";");
    }
}
