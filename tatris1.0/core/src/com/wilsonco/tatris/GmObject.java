/*
 *  Tatris
 *  Copyright (C) April 2016, Daniel Wilson
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
package com.wilsonco.tatris;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author WILSON
 */
public class GmObject {
    /*static ArrayList<GmObject> gmObjectDrawList = new ArrayList<GmObject>();          OBSELETE
    static ArrayList<GmObject> gmObjectStepList = new ArrayList<GmObject>();
    
    public static void renderAllGmObjects(SpriteBatch sb) {          OBSELETE
        for (GmObject g : gmObjectStepList) {
            g.stepEvent(sb);                              OBSELETE
        }
        for (GmObject g : gmObjectDrawList) {          OBSELETE
            g.drawEvent(sb);
        }
    }*/
    
    public static boolean rectangleOverlap(double x1,double y1,double width1,double height1,double x2,double y2,double width2,double height2) {
        return !(x1 > x2+width2 || y1 > y2+height2 || x1+width1 < x2 || y1+height1 < y2);
    }
    
    public Texture sprite;
    public double x = 0;
    public double y = 0;
    public double spriteWidth = 0;
    public double spriteHeight = 0;
    public double hspeed = 0;
    public double vspeed = 0;
    public double gravity = 0;
    public double gravityDirection = 0;
    private boolean enableCollisions = false;
    public double xprevious = 0;
    public double yprevious = 0;
    
    public double originX = 0;
    public double originY = 0;
    public double transformOriginX = 0;
    public double transformOriginY = 0;
    public double collisionWidth = 0;
    public double collisionHeight = 0;
    public double collisionOriginx = 0;
    public double collisionOriginy = 0;
    
    public double scaleX = 1;
    public double scaleY = 1;
    public double angle = 0;
    
    public boolean flipX = false;
    public boolean flipY = false;
    
    public int cropLeft = 0;
    public int cropTop = 0;
    public int cropRight = 0;
    public int cropBottom;
    
    public int stepId = 0;
    public int drawId = 0;
    
    public boolean simpleDraw = false;
    
    public boolean updatePositionBeforeStep = false;
    
    public GmScaleObject scaleObject = new GmScaleObject();
    
    ArrayList<GmCollision> collisions = new ArrayList<GmCollision>();
    
    public ShapeRenderer shapeRenderer;
    
    GmObject() {
    }
    GmObject(Texture tex) {
        sprite = tex;
    }
    GmObject(String fname) {
        sprite = new Texture(Gdx.files.internal(fname));
    }
    GmObject(Texture tex,double width, double height) {
        sprite = tex;
        spriteWidth = width;
        spriteHeight = height;
        collisionWidth = width;
        collisionHeight = height;
    }
    GmObject(String fname,double width, double height) {
        sprite = new Texture(Gdx.files.internal(fname));
        spriteWidth = width;
        spriteHeight = height;
        collisionWidth = width;
        collisionHeight = height;
    }
    GmObject(double width, double height) {
        spriteWidth = width;
        spriteHeight = height;
        collisionWidth = width;
        collisionHeight = height;
    }
    
    public final void setPosition(double xx, double yy) {
        x = xx; y = yy;
    }
    
    /*public final void addDraw() {          OBSELETE
        GmObject.gmObjectDrawList.add(this);
    }
    public final void addStep() {
        GmObject.gmObjectStepList.add(this);
    }*/
    
    public final void setSprite(Texture tex) {
        sprite = tex;
    }
    public final void setSprite(Texture tex,double width,double height) {
        sprite = tex;
        spriteWidth = width; collisionWidth = width;
        spriteHeight = height; collisionHeight = height;
    }
    public final void setSprite(String fname) {
        sprite = new Texture(Gdx.files.internal(fname));
    }
    public final void setSprite(String fname,double width,double height) {
        sprite = new Texture(Gdx.files.internal(fname));
        spriteWidth = width; collisionWidth = width;
        spriteHeight = height; collisionHeight = height;
    }
    
    public final void setSpeedDirection(double speed, double direction) {
        hspeed = speed*Math.sin(Math.toRadians(direction+90));
        vspeed = speed*Math.cos(Math.toRadians(direction+90));
    }
    
    public final double getSpeed() {
        return Math.sqrt(hspeed*hspeed+vspeed*vspeed);
    }
    public final double getDirection() {
        return -Math.toDegrees(Math.atan2(vspeed,hspeed));
    }
    
    public final void setDirection(double direction) {
        setSpeedDirection(getSpeed(),direction);
    }
    
    public final void setSpeed(double speed) {
        setSpeedDirection(speed,getDirection());
    }
    public final void increaseSpeed(double amount) {
        setSpeed(getSpeed()+amount);
    }
    public final void increaseSpeedFactor(double amount) {
        setSpeed(getSpeed()*amount);
    }
    public final void increaseDirection(double amount) {
        setDirection(getDirection()+amount);
    }
    
    public final double getNextXPos() {
        double hs = hspeed + gravity*Math.sin(Math.toRadians(gravityDirection+90));
        return x + hs;
    }
    public final double getNextYPos() {
        double vs = vspeed + gravity*Math.cos(Math.toRadians(gravityDirection+90));
        return y + vs;
    }
    public final double getNextXPos(boolean updateHspeed) {
        if (updateHspeed) {
            hspeed += gravity*Math.sin(Math.toRadians(gravityDirection+90));
            return x + hspeed;
        } else {
            return getNextXPos();
        }
    }
    public final double getNextYPos(boolean updateVspeed) {
        if (updateVspeed) {
            vspeed += gravity*Math.cos(Math.toRadians(gravityDirection+90));
            return y + vspeed;
        } else {
            return getNextYPos();
        }
    }
    
    public final void setCollision(double width,double height) {
        collisionWidth = width;
        collisionHeight = height;
    }
    
    public final void setCollision(double width,double height,double originx,double originy) {
        collisionWidth = width;
        collisionHeight = height;
        collisionOriginx = originx;
        collisionOriginy = originy;
    }
    
    public final void setSpriteProperties(double width,double height) {
        spriteWidth = width;
        spriteHeight = height;
    }
    
    public final void setSpriteProperties(double width,double height,double xorigin,double yorigin) {
        spriteWidth = width;
        spriteHeight = height;
        this.originX = xorigin;
        this.originY = yorigin;
    }
    
    public final void setOrigin(double xorigin,double yorigin) {
        this.originX = xorigin;
        this.originY = yorigin;
    }
    
    public void addCollision(GmCollision g) {
        enableCollisions = true;
        collisions.add(g);
    }
    
    public void removeCollision(GmCollision g) {
        collisions.remove(g);
    }
    
    public void removeAllCollisions() {
        collisions.clear();
    }
    
    private void runAllCollisions() {
        GmCollisionTypeReturn r;
        for (GmCollision g : collisions) {
            r = g.collisionType.isColliding();
            if (r.value) {
                onCollision(r.object);
                g.collisionEvent.run(this,r.object);
            }
        }
    }
    
    public final void stepEvent(SpriteBatch sb) {
        beginStep(sb);
        if (enableCollisions) {
            runAllCollisions();
        }
        if (updatePositionBeforeStep) {
            xprevious = x; x = getNextXPos(true);
            yprevious = y; y = getNextYPos(true);
        }
        step(sb);
    }
    
    public final void drawEvent(SpriteBatch sb) {
        if (!updatePositionBeforeStep) {
            xprevious = x; x = getNextXPos(true);
            yprevious = y; y = getNextYPos(true);
        }
        beforeDraw(sb);
        draw(sb);
        endStep(sb);
    }
    
    // ======================================================================================================
    // ------------------------------ OVERRIDABLE METHODS ---------------------------------------------------
    // ======================================================================================================
    
    // beginStep - runs before everything
    // step - runs after collision but before everything else. Runs before new position is calculated
    // beforeDraw - runs after all collisions and step events of all objects but before draw of this object. Runs after new position is calculated
    // onCollision - runs when the object collides (generic)
    // draw - override to change draw event - when everything is drawn
    // endStep - runs after everything
    
    void beginStep(SpriteBatch sb) {
        
    }
    void step(SpriteBatch sb) {
        
    }
    void beforeDraw(SpriteBatch sb) {
        
    }
    void onCollision(GmObject g) {
        
    }
    void draw(SpriteBatch sb) { // By default - draws the sprite
        if (simpleDraw) { // Draws simply if simpleDraw is enabled - slightly faster
            sb.draw(sprite,(float)x-(float)originX,(float)y-(float)originY,(float)spriteWidth,(float)spriteHeight,0,0,(int)spriteWidth,(int)spriteHeight,false,true);
        } else { // Otherwise complexDraw
            //WAS
            //sb.draw(sprite,(float)x-(float)xorigin,(float)y-(float)yorigin,(float)spriteWidth,(float)spriteHeight,0,0,(int)spriteWidth,(int)spriteHeight,false,true);
            
            //      tex    x                        y                       xorigin                 yorigin                 width              height              scaleX         scaleY       rotation     srcX srcY  srcWidth        srcWidth          flipX  flipY
            //sb.draw(sprite,(float)x-(float)originX,(float)y-(float)originY,(float)transformOriginX,(float)transformOriginY,(float)spriteWidth,(float)spriteHeight,(float)scaleX,(float)scaleY,(float)angle,16,      16,(int)spriteWidth,(int)spriteHeight,flipX,!flipY);
            
            double texRatioX = sprite.getWidth()/spriteWidth;
            double texRatioY = sprite.getHeight()/spriteHeight;
                        
            sb.draw(sprite,
                    ((float)x - (float)originX) * (float)scaleObject.scaleX + (float)scaleObject.offsetX, // x
                    ((float)y - (float)originY) * (float)scaleObject.scaleY + (float)scaleObject.offsetY, // y
                    ((float)transformOriginX) * (float)scaleObject.scaleX, // originx
                    ((float)transformOriginY) * (float)scaleObject.scaleY, // originy
                    ((float)spriteWidth-cropLeft-cropRight) * (float)scaleObject.scaleX, // width
                    ((float)spriteHeight-cropTop-cropBottom) * (float)scaleObject.scaleY, // height
                    ((float)scaleX), // scalex
                    ((float)scaleY), // scaley
                    ((float)-angle), // rotation
                    (int)(cropLeft*texRatioX), // srcX
                    (int)(cropTop*texRatioY), // srcY
                    (int)((spriteWidth-cropLeft-cropRight)*texRatioX), // srcWidth
                    (int)((spriteHeight-cropTop-cropBottom)*texRatioY), // srcHeight
                    (flipX), // flipX
                    (!flipY) // flipY
                    );
        }
    }
    
    void endStep(SpriteBatch sb) {
        
    }
}


class SplitDrawObject extends GmObject {
    private class AutoTextureRegion extends TextureRegion {
        private SplitDrawObject sd;
        private int width;
        private int height;
        private float x;
        private float y;
        boolean output = false;
        
        boolean useHOffset1;
        boolean useVOffset1;
        boolean useHOffset2;
        boolean useVOffset2;
        
        AutoTextureRegion(SplitDrawObject sd,   boolean useHOffset1, boolean useVOffset1, boolean useHOffset2, boolean useVOffset2            ,Texture tex,int x,int y,int width,int height) {
            super(tex,x,y,width,height);
            this.sd = sd;
            flip(sd.flipX, !sd.flipY);
            this.width = width;
            this.height = height;
            this.x = x;
            this.y = y;
            this.useHOffset1 = useHOffset1;
            this.useVOffset1 = useVOffset1;
            this.useHOffset2 = useHOffset2;
            this.useVOffset2 = useVOffset2;
        }
        public void draw(SpriteBatch sb) {
            sb.draw(this,
                    (float)sd.x + (float)scaleObject.offsetX + (x   + ((useHOffset1)?sd.splitWidth:0)) *(float)(sd.scaleX*sd.borderScaleX*sd.scaleObject.scaleX), // x
                    (float)sd.y + (float)scaleObject.offsetY + (y   + ((useVOffset1)?sd.splitHeight:0))*(float)(sd.scaleY*sd.borderScaleY*sd.scaleObject.scaleY), // y
                    0, // originx
                    0, // originy
                    ((width  + (((useHOffset1 || useHOffset2) && !(useHOffset1 && useHOffset2))?sd.splitWidth:0)))*(float)sd.borderScaleY, // width
                    (height + (((useVOffset1 || useVOffset2) && !(useVOffset1 && useVOffset2))?sd.splitHeight:0))*(float)sd.borderScaleX, // height
                    (float)(sd.scaleX*sd.scaleObject.scaleX), // scalex
                    (float)(sd.scaleY*sd.scaleObject.scaleY), // scaley
                    0 // rotation
                    );
            /*if (output) { DEBUG STOOF
                System.out.println("==================================");
                System.out.println("Obj x: "+sd.x);
                System.out.println("Region x: "+x);
                System.out.println("Origin x: "+sd.originX);
                System.out.println("Scale x: "+sd.scaleObject.scaleX);
                System.out.println("Final x: "+((float)sd.x+x-(float)sd.originX) * (float)sd.scaleObject.scaleX);
                System.out.println("");
            
                System.out.println("Obj y: "+sd.y);
                System.out.println("Region y: "+y);
                System.out.println("Origin y: "+sd.originY);
                System.out.println("Scale y: "+sd.scaleObject.scaleY);
                System.out.println("Final y: "+((float)sd.y+y-(float)sd.originY) * (float)sd.scaleObject.scaleY);
                System.out.println("");
            
                System.out.println("Attempted width: "+width);
            }*/
        }
    }
    int marginLeft = 32;
    int marginTop = 32;
    int marginBottom = 32;
    int marginRight = 32;
    
    
    int splitWidth = 0;
    int splitHeight = 0;
    
    double borderScaleX = 1;
    double borderScaleY = 1;
    
    /*AutoTextureRegion topLeft;
    AutoTextureRegion topRight;
    AutoTextureRegion top;
    AutoTextureRegion left;
    AutoTextureRegion bottom;
    AutoTextureRegion right;
    AutoTextureRegion middle;
    AutoTextureRegion bottomLeft;
    AutoTextureRegion bottomRight;*/
    
    AutoTextureRegion[] textureRegions;
    
    SplitDrawObject() {
        super();
    }
    
    SplitDrawObject(int marginLeft, int marginTop, int marginBottom, int marginRight) {
        super();
        this.marginLeft = marginLeft; this.marginTop = marginTop; this.marginBottom = marginBottom; this.marginRight = marginRight;
    }
    
    SplitDrawObject(Texture tex) {
        super(tex);
    }
    SplitDrawObject(Texture tex, int marginLeft, int marginTop, int marginBottom, int marginRight) {
        super(tex);
        this.marginLeft = marginLeft; this.marginTop = marginTop; this.marginBottom = marginBottom; this.marginRight = marginRight;
    }
    SplitDrawObject(String fname) {
        super(fname);
    }
    SplitDrawObject(String fname, int marginLeft, int marginTop, int marginBottom, int marginRight) {
        super(fname);
        this.marginLeft = marginLeft; this.marginTop = marginTop; this.marginBottom = marginBottom; this.marginRight = marginRight;
    }
    SplitDrawObject(Texture tex,double width, double height) {
        super(tex,width,height);
    }
    SplitDrawObject(Texture tex,double width, double height, int marginLeft, int marginTop, int marginBottom, int marginRight) {
        super(tex,width,height);
        this.marginLeft = marginLeft; this.marginTop = marginTop; this.marginBottom = marginBottom; this.marginRight = marginRight;
    }
    SplitDrawObject(String fname,double width, double height) {
        super(fname,width,height);
    }
    SplitDrawObject(String fname,double width, double height, int marginLeft, int marginTop, int marginBottom, int marginRight) {
        super(fname,width,height);
        this.marginLeft = marginLeft; this.marginTop = marginTop; this.marginBottom = marginBottom; this.marginRight = marginRight;
    }
    SplitDrawObject(double width, double height) {
        super(width,height);
    }
    SplitDrawObject(double width, double height, int marginLeft, int marginTop, int marginBottom, int marginRight) {
        super(width,height);
        this.marginLeft = marginLeft; this.marginTop = marginTop; this.marginBottom = marginBottom; this.marginRight = marginRight;
    }
    
    public void updateTextureRegions() {
        int centreWidth =  (int)sprite.getWidth()  - marginLeft - marginRight;
        int centreHeight = (int)sprite.getHeight() - marginTop  - marginBottom;
        System.out.println("CENTREWIDTH: "+centreWidth);
        System.out.println("CENTREHEIGHT: "+centreHeight);
        
        splitWidth = (int)(spriteWidth*(1/borderScaleX) - sprite.getWidth());
        splitHeight = (int)(spriteHeight*(1/borderScaleY) - sprite.getHeight());
        
        textureRegions = new AutoTextureRegion[9];
        
        textureRegions[0] = new AutoTextureRegion(this,false,false,false,false,sprite,0,0,marginLeft,marginRight);
        textureRegions[1] = new AutoTextureRegion(this,false,false,true,false,sprite,marginLeft,0,centreWidth,marginTop);
        textureRegions[2] = new AutoTextureRegion(this,true,false,true,false,sprite,marginLeft+centreWidth,0,marginRight,marginTop);
        
        textureRegions[3] = new AutoTextureRegion(this,false,false,false,true,sprite,0,marginTop,marginLeft,centreHeight);
        textureRegions[4] = new AutoTextureRegion(this,false,false,true,true,sprite,marginLeft,marginTop,centreWidth,centreHeight);
        textureRegions[5] = new AutoTextureRegion(this,true,false,true,true,sprite,marginLeft+centreWidth,marginTop,marginRight,centreHeight);
        
        textureRegions[6] = new AutoTextureRegion(this,false,true,false,true,sprite,0,marginTop+centreHeight,marginLeft,marginBottom);
        textureRegions[7] = new AutoTextureRegion(this,false,true,true,true,sprite,marginLeft,marginTop+centreHeight,centreWidth,marginBottom);
        textureRegions[8] = new AutoTextureRegion(this,true,true,true,true,sprite,marginLeft+centreWidth,marginRight+centreHeight,marginRight,marginBottom);
        
        textureRegions[1].output = true;
        
    }
    
    @Override public void draw(SpriteBatch sb) {
        for (AutoTextureRegion a : textureRegions) {
            a.draw(sb);
        }
    }
}

//=================================================================================================================

class GmFontObject extends GmObject {
    BitmapFont font;
    String text;
    boolean autoSetScale = false;
    GmFontObject(String fontFName, String text) {
         this.font = new BitmapFont(Gdx.files.internal(fontFName),true);
         this.text = text;
         
    }
    GmFontObject(BitmapFont font, String text) {
         this.font = font; this.text = text;
    }
    
    @Override void draw(SpriteBatch sb) {
        if (autoSetScale) updateScale();
        font.draw(sb,text,(int)(scaleObject.scaleX*x + scaleObject.offsetX), (int)(scaleObject.scaleY*y + scaleObject.offsetY));
    }
    
    public void updateScale() {
        font.getData().setScale((float)(scaleObject.scaleX*scaleX),-(float)(scaleObject.scaleY*scaleY));
    }
    public void updateScale(float scaleX, float scaleY) {
        font.getData().setScale(scaleX*(float)this.scaleX,-scaleY*(float)this.scaleY);
    }
}

// ================================================================================================================
class GmScaleObject {
    public volatile double scaleX = 1;  // NOTE: ALL VARIABLES MADE VOLATILE
    public volatile double scaleY = 1;  // TO AVOID SYNCHRONISATION ISSUES, BECAUSE MULTIPLE OBJECTS
    public volatile double offsetX = 0; // WILL BE ACCESSING THE PROPERTIES OF THIS OBJECT, POSSIBLE ASYNCHRONOUSLY
    public volatile double offsetY = 0; // FROM MULTIPLE THREADS.
    GmScaleObject() {
    }
    GmScaleObject(double scaleX,double scaleY) {
        this.scaleX = scaleX;
        this.scaleY = scaleY;
    }
    GmScaleObject(double scaleX,double scaleY,double offsetX, double offsetY) {
        this.scaleX = scaleX;
        this.scaleY = scaleY;
        this.offsetX = offsetX;
        this.offsetY = offsetY;
    }
}
// ====================================================================================================================
class GmCollision { // Stores a pair of collision type and event
    public GmCollisionType collisionType;
    public GmCollisionEvent collisionEvent;
    GmCollision(GmCollisionType gType,GmCollisionEvent gEv) {
        collisionType = gType; collisionEvent = gEv;
    }
}

//=======================================================================================================================
class GmCollisionTypeReturn { // Stores a 
    boolean value;
    GmObject object = null;
    GmCollisionTypeReturn(boolean value,GmObject object) {
        this.value = value;
        this.object = object;
    }
}

class GmCollisionType { // COLLISION TYPE - OBJECT USED TO CHECK IF THERE IS A COLLISION ==============================================
    public GmCollisionTypeReturn isColliding() {
        return new GmCollisionTypeReturn(false,null);
    }
}

class GmObjectObjectCollisionType extends GmCollisionType { // Checks if there is a collision between 2 objects
    GmObject object1;
    GmObject object2;
    boolean useNextFrame = false;
    public GmCollisionTypeReturn isColliding() {
        if (useNextFrame) {
            double x1 = object1.getNextXPos()+object1.collisionOriginx;
            double y1 = object1.getNextYPos()+object1.collisionOriginy;
            double x2 = object2.getNextXPos()+object2.collisionOriginx;
            double y2 = object2.getNextYPos()+object2.collisionOriginy;
            boolean c = GmObject.rectangleOverlap(x1       ,y1       ,object1.collisionWidth,object1.collisionHeight,x2       ,y2       ,object2.collisionWidth,object2.collisionHeight);
            return new GmCollisionTypeReturn(c,object2);
        } else {
            //return GmObject.rectangleOverlap(object1.x,object1.y,object1.spriteWidth,object1.spriteHeight,object2.x,object2.y,object2.spriteWidth,object2.spriteHeight);
            boolean c = GmObject.rectangleOverlap(object1.x+object1.collisionOriginx,object1.y+object1.collisionOriginy,object1.collisionWidth,object1.collisionHeight,object2.x+object2.collisionOriginx,object2.y+object2.collisionOriginy,object2.collisionWidth,object2.collisionHeight);
            return new GmCollisionTypeReturn(c,object2);
        }
    }
    GmObjectObjectCollisionType(GmObject obj1, GmObject obj2)  {
        object1 = obj1; object2 = obj2;
    }
    GmObjectObjectCollisionType(GmObject obj1, GmObject obj2, boolean useNextFrame)  {
        object1 = obj1; object2 = obj2; this.useNextFrame = useNextFrame;
    }
}

//=======================================================================================================================
class GmCollisionEvent { // COLLISION EVENT - OBJECT USED TO RUN CODE WHEN THERE IS A COLLISION =======================================
    public void run(GmObject gm,GmObject other) {
        
    }
}

class GmBounceCollisionEventH extends GmCollisionEvent {
    @Override public void run(GmObject gm,GmObject other) {
        gm.hspeed  = -gm.hspeed-gm.gravity*Math.sin(Math.toRadians(gm.gravityDirection+90));;
    }
}

class GmBounceCollisionEventV extends GmCollisionEvent {
    @Override public void run(GmObject gm,GmObject other) {
        gm.vspeed = -gm.vspeed-gm.gravity*Math.cos(Math.toRadians(gm.gravityDirection+90));
    }
}

class GmStopCollisionEventH extends GmCollisionEvent {
    @Override public void run(GmObject gm,GmObject other) {
        gm.hspeed  = -gm.gravity*Math.sin(Math.toRadians(gm.gravityDirection+90));
    }
}

class GmStopCollisionEventV extends GmCollisionEvent {
    @Override public void run(GmObject gm,GmObject other) {
        gm.vspeed  = -gm.gravity*Math.cos(Math.toRadians(gm.gravityDirection+90));
    }
}


class GmOutputCollisionEvent extends GmCollisionEvent {
    @Override public void run(GmObject gm,GmObject other) {
        System.out.println("COLLISION");
    }
}

//=================================================================================================================
//=================================================================================================================
//=================================================================================================================

class GmRoom { // An empty, but useful class for rooms.
    GmObjectWorld world;
    GmListContainer worldList;
    public void create() {
        
    }
    public void updateStep(SpriteBatch sb) {
        
    }
    public void updateDraw(SpriteBatch sb) {
        
    }
    public void destroy() {
        
    }
    public void beforeEverything() {
        
    }
    public void afterEverything() {
        
    }
}



//=================================================================================================================
//=================================================================================================================
//=================================================================================================================






//                                                   0000
//    /////////////////////////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
//   |===========================================================================================================|
//   |                             THE GRAND AND MARVELOUS OBJECT LIST SYSTEM                                    |
//   |===========================================================================================================|
//    \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\//////////////////////////////////////////////////////////
//                                                   0000




final class GmObjectWorld { // A GmObject world. Contained within it is a series of object lists - such as a draw list and a step list.
    // This allows you to have sections of different types of object list systems, for use in different places
    // and circumstances at the same time, this allowing the speed of a program to be sped up greatly.
    
    //       FOR EXAMPLE:
    // In a grid-based infinite platform game, you might want a Non-dynamic object list for the world
    // grid, as only a certain amount is on screen at once, and objects will be being created and destroyed
    // continuously. You might also want a dynamic object list for mobs (at the same time), as the number of them can
    // change, will be unlimited and highly variable.
    
    // With this sytem you can have both these lists existing simultaneously.
    
    // Also, you might want to have multiple
    // object lists for multiple layers, for example the world in one object list, and GUI in another, so the
    // GUI is always in front of the world.
    
    /* A DIAGRAM OF THE SYSTEM
    
    
                                         GmObjectWorld                                             Stores a system of object lists
                                               |
               ,_______________________________|_________________________________,
               |                               |                                 |
         GmListContainer                 GmListContainer                   GmListContainer         Stores a set of 2 GmObjectLists - a draw list and a step list.
               |                               |                                 |
      ,________|________,             ,________|_______,                ,________|________,
      |                 |             |                |                |                 |
GmObjectList      GmObjectList  GmObjectList     GmObjectList     GmObjectList      GmObjectList   Stores a list of objects, stores a list of objects for step event and for draw event so some objects can have step events, but not draw events etc etc vice versa.
   (STEP)            (DRAW)        (STEP)           (DRAW)           (STEP)            (DRAW)
      |                 |             |                |                |                 |
      |                 |             |                |                |                 |
  GmObject          GmObject      GmObject         GmObject         GmObject          GmObject
   OBJECTS           OBJECTS       OBJECTS          OBJECTS          OBJECTS           OBJECTS
    
    
    */
    
    ArrayList<GmListContainer> objectList; // The list of object lists
    ArrayList<InputProcessor> inputObjects;
    private GmInputAdapter inputAdapter;
    GmObjectWorld(GmInputAdapter gi) {
        objectList = new ArrayList<GmListContainer>();
        inputObjects = new ArrayList<InputProcessor>();
        inputAdapter = gi; create();
    }
    private void create() {
        inputAdapter.world = this;
    }
    public final void updateStep(SpriteBatch sb) { // Updates all the step events of all the objects in all of the object lists
        for (GmListContainer g : objectList) {
            //System.out.println("=======================================================");
            //System.out.println("WORLD processing of STEP");
            g.updateStep(sb);
        }
    }
    public final void updateDraw(SpriteBatch sb) {
        for (GmListContainer g : objectList) { // Updates all the draw events of all the objects in all of the object lists
            //System.out.println("=======================================================");
            //System.out.println("WORLD processing of DRAW");
            g.updateDraw(sb);
        }
    }
    public final GmListContainer add(GmObjectList stepList, GmObjectList drawList) {
        GmListContainer g = new GmListContainer(stepList,drawList);
        objectList.add(g); // Adds an object list
        return g;
    }
    public final void remove(GmListContainer container) {
        objectList.remove(container); // Removes an object list
    }
    public final void destroy() { // THIS SHOULD ALWAYS BE RUN IN THE DESTROY METHOD OF THE GM-OBJECT-ROOM.
        inputAdapter.world = null; // Nullify some references to allow garbage collection to destroy both objects...
        inputAdapter = null; //       ... if nessacary.
    }
}

//============================================ GMINPUTADAPTER =====================================================================

final class GmInputAdapter implements InputProcessor { // This is a wrapper class to
    public GmObjectWorld world = null;             //     allow standard input processing. Using this with
    GmInputAdapter() {                             //     a GmObjectWorld allows you to be able to add and 
        Gdx.input.setInputProcessor(this);         //     remove inputProcessors efficiently.
    }
    @Override public boolean keyDown(int i) {
        if (world != null) for (InputProcessor obj : world.inputObjects) obj.keyDown(i); return true;
    }
    @Override public boolean keyUp(int i) {
        System.out.println("KEYUP");
        if (world != null) for (InputProcessor obj : world.inputObjects) obj.keyUp(i); return true;
    }
    @Override public boolean keyTyped(char c) {
        if (world != null) for (InputProcessor obj : world.inputObjects) obj.keyTyped(c); return true;
    }
    @Override public boolean touchDown(int i, int i1, int i2, int i3) {
        if (world != null) for (InputProcessor obj : world.inputObjects) obj.touchDown(i,i1,i2,i3); return true;
    }
    @Override public boolean touchUp(int i, int i1, int i2, int i3) {
        if (world != null) for (InputProcessor obj : world.inputObjects) obj.touchUp(i,i1,i2,i3); return true;
    }
    @Override public boolean touchDragged(int i, int i1, int i2) {
        if (world != null) for (InputProcessor obj : world.inputObjects) obj.touchDragged(i,i1,i2); return true;
    }
    @Override public boolean mouseMoved(int i, int i1) {
        if (world != null) for (InputProcessor obj : world.inputObjects) obj.mouseMoved(i,i1); return true;
    }
    @Override public boolean scrolled(int i) {
        if (world != null) for (InputProcessor obj : world.inputObjects) obj.scrolled(i); return true;
    }
}

//=================================================================================================================

final class GmListContainer { // Contains a step list and a draw list
    GmObjectList step;
    GmObjectList draw;
    GmListContainer(GmObjectList stepList, GmObjectList drawList) {
        step = stepList;
        draw = drawList;
    }
    public final void updateStep(SpriteBatch sb) {
        //System.out.println("CONTAINER processing of STEP");
        step.updateStep(sb);
    }
    public final void updateDraw(SpriteBatch sb) {
        //System.out.println("container processing of DRAW");
        draw.updateDraw(sb);
    }
}

//=================================================================================================================

interface GmObjectList { // The interface for GmObjectLists. GmObjectLists must all implement the update method.
    public void updateStep(SpriteBatch sb); // Runs the step event on all of the contained objects
    public void updateDraw(SpriteBatch sb); // Runs the draw event on all of the contained objects
    public int add(GmObject obj); // Adds a gmObject - returns an object id if needed
    public void remove(GmObject obj); // Removes a gmObject
    public void setToRemove(GmObject obj); // Removes a gmObject
    public void setToAdd(GmObject obj); // Removes a gmObject
    public void remove(int id); // Removes a gmObject by id, returns the GmObject removed
    public GmObject[] getAsArray(); // Gets the list of objects as an array
    public LinkedList<GmObject> getAsLinkedList(); // .... linked list
    public ArrayList<GmObject> getAsArrayList(); // ...... ArrayList
    public int getSize(); // Gets the total number of objects stored within the list
    public int getId(GmObject obj); // Gets the numerical id of the object.
    public GmObject getObjectById(int id); // Gets an object by id
}

//=================================================================================================================
//=================================================================================================================
//=================================================================================================================


class GmObjectListFullyDynamic implements GmObjectList { // A fully dynamic object list, using linked list.
    /*
        Because this uses linked lists, this is fastest when the number of objects keeps changing a lot.
        ---> Unlimited number of objects.
    */
    LinkedList<GmObject> objects = new LinkedList<GmObject>();
    LinkedList<GmObject> removal = new LinkedList<GmObject>();
    LinkedList<GmObject> addition = new LinkedList<GmObject>();
    
    
    @Override public void updateStep(SpriteBatch sb) {
        updateAddsAndRemoves();
        for (GmObject obj : objects) {
            obj.stepEvent(sb);
        }
        updateAddsAndRemoves();
    }
    @Override public void updateDraw(SpriteBatch sb) {
        updateAddsAndRemoves();
        for (GmObject obj : objects) {
            obj.drawEvent(sb);
        }
        updateAddsAndRemoves();
    }
    private void updateAddsAndRemoves() {
        if (!removal.isEmpty()) {
            objects.removeAll(removal);
            removal.clear();
        }
        if (!addition.isEmpty()) {
            objects.addAll(addition);
            addition.clear();
        }
    }
    @Override public int add(GmObject obj) {
        objects.add(obj); return 0;
    }
    @Override public void remove(GmObject obj) {
        objects.remove(obj);
    }
    @Override public void remove(int id) {
        objects.remove(id);
    }
    @Override public GmObject[] getAsArray() {
        return (GmObject[])objects.toArray();
    }
    @Override public LinkedList<GmObject> getAsLinkedList() {
        return objects;
    }
    @Override public ArrayList<GmObject> getAsArrayList() {
        return new ArrayList<GmObject>(objects);
    }
    @Override public int getSize() {
        return objects.size();
    }
    @Override public int getId(GmObject obj) {
        return objects.indexOf(obj);
    }
    @Override public GmObject getObjectById(int id) {
        return objects.get(id);
    }

    @Override public void setToRemove(GmObject obj) {
        removal.add(obj);
    }

    @Override public void setToAdd(GmObject obj) {
        addition.add(obj);
    }
}

//=================================================================================================================

class GmObjectListSemiDynamic implements GmObjectList { // A semi dynamic object list, using ArrayList.
    /*
        Because this uses ArrayLists, this is fastest when the total number of objects stays mostly the same.
        ---> Unlimited number of objects.
    */
    ArrayList<GmObject> objects = new ArrayList<GmObject>();
    LinkedList<GmObject> removal = new LinkedList<GmObject>();
    LinkedList<GmObject> addition = new LinkedList<GmObject>();
    
    @Override public void updateStep(SpriteBatch sb) {
        updateAddsAndRemoves();
        for (GmObject obj : objects) {
            obj.stepEvent(sb);
        }
        updateAddsAndRemoves();
    }
    @Override public void updateDraw(SpriteBatch sb) {
        //System.out.println("LIST processing of DRAW");
        updateAddsAndRemoves();
        for (GmObject obj : objects) {
            obj.drawEvent(sb);
        }
        updateAddsAndRemoves();
    }
    private void updateAddsAndRemoves() {
        if (!removal.isEmpty()) {
            objects.removeAll(removal);
            removal.clear();
        }
        if (!addition.isEmpty()) {
            objects.addAll(addition);
            addition.clear();
        }
    }
    @Override public int add(GmObject obj) {
        objects.add(obj); return 0;
    }
    @Override public void remove(GmObject obj) {
        objects.remove(obj);
    }
    @Override public void remove(int id) {
        objects.remove(id);
    }
    @Override public GmObject[] getAsArray() {
        return (GmObject[])objects.toArray();
    }
    @Override public LinkedList<GmObject> getAsLinkedList() {
        return new LinkedList<GmObject>(objects);
    }
    @Override public ArrayList<GmObject> getAsArrayList() {
        return objects;
    }
    @Override public int getSize() {
        return objects.size();
    }
    @Override public int getId(GmObject obj) {
        return objects.indexOf(obj);
    }
    @Override public GmObject getObjectById(int id) {
        return objects.get(id);
    }

    @Override public void setToRemove(GmObject obj) {
        removal.add(obj);
    }
    
    @Override public void setToAdd(GmObject obj) {
        addition.add(obj);
    }
}

//=================================================================================================================

// FOLLOWING CLASSES UNSUPPORTED, GIVEN UP ON, AND TERRIBLE

class GmObjectListNonDynamic implements GmObjectList { // A static object list, using arrays.
    /*
        Because this uses standard arrays, it is extremely fast but the number of objects cannot go over a limit and is pre-defined.
        ---> Limited, pre-defined maximum number of objects
        ---> Removing object by object rather than by id IS VERY SLOW AS HAS TO SEARCH ARRAY
        ---> Getting the id of an object by object IS VERY SLOW AS HAS TO SEARCH ARRAY
    */
    GmObject[] objects; // Stores a list of objects
    int[] ids; // Stores a list of free id's for objects to be put into
    int nextCreateId = 0; // The next id from the id's list to be read and used
    int nextRemoveId = 0; // The next id from the id's list to be re-used
    int size;
    GmObjectListNonDynamic(int size) {
        this.size = size;
        objects = new GmObject[size];
        ids = new int[size];
        int i = 0; while (i < size) {
            ids[i] = i;
        }
    }

    @Override public void updateStep(SpriteBatch sb) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override public void updateDraw(SpriteBatch sb) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override public int add(GmObject obj) {
        int id = ids[nextCreateId]; // Gets the object-list id from the next create id in the id list
        if (ids[id] == -1) {
            throw new UnsupportedOperationException("Too many GmObjects - run out of space.");
        } else {
            objects[id] = obj; // Puts the object in the appropriate place in the objects array
            ids[nextCreateId] = -1; // Sets that position to -1 to signify it is used
            nextCreateId = (nextCreateId+1)%size; // Increments the next create id, wrapping it around to make sure it does not exceed the maximum number of objects
            return id; // Return the id
        }
    }

    @Override public void remove(GmObject obj) { // VERY SLOW BY DEFAULT - HAS TO SEARCH AN ARRAY. IF THIS IS NEEDED USE ONE OF THE SUBCLASSES GmObjectListNonDynamicStep
        int id = Arrays.asList(objects).indexOf(obj);
        if (id != -1) {
            remove(id);
        }
    }

    @Override public void remove(int id) {
        if (ids[id] != -1) { // Checks if there is an instance to remove, otherwise throws an exception
            throw new UnsupportedOperationException("No object to remove."); //To change body of generated methods, choose Tools | Templates.
        } else { // If there is an instance to remove,
            objects[id] = null; // Nullify it in the 
            ids[nextRemoveId] = id;
            nextCreateId = (nextCreateId+1)%size;
        }
    }

    @Override public GmObject[] getAsArray() {
        return objects;
    }

    @Override public LinkedList<GmObject> getAsLinkedList() {
        return new LinkedList(Arrays.asList(objects));
    }

    @Override public ArrayList<GmObject> getAsArrayList() {
        return new ArrayList(Arrays.asList(objects));
    }

    @Override public int getSize() {
        return size;
    }

    @Override public int getId(GmObject obj) {// VERY SLOW BY DEFAULT - HAS TO SEARCH AN ARRAY. IF THIS IS NEEDED USE ONE OF THE SUBCLASSES GmObjectListNonDynamicStep
        return Arrays.asList(objects).indexOf(obj);
    }
    
    @Override public GmObject getObjectById(int id) {
        return objects[id];
    }

    @Override
    public void setToRemove(GmObject obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setToAdd(GmObject obj) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
//-------------------------------------------------------------------------------------------------------------
/*
THE FOLLOWING 2 CLASSES DO EXACTLY THE SAME AS GmObjectListNonDynamicStep, but are for more specific circumstances.

They massively speed up removing and getting object id's by object, by allowing the id's to be stored and kept within the objects.
However, 2 seperate classes are needed because each object will have multiple id's ---> a step id and a draw id.
*/
class GmObjectListNonDynamicStep extends GmObjectListNonDynamic {
    public GmObjectListNonDynamicStep(int size) {
        super(size);
    }
    @Override public int add(GmObject obj) {
        obj.stepId = super.add(obj);
        return obj.stepId;
    }
    @Override public void remove(GmObject obj) {
        remove(obj.stepId);
    }
    @Override public int getId(GmObject obj) {
        return obj.stepId;
    }
}
//..............................................................................................................
class GmObjectListNonDynamicDraw extends GmObjectListNonDynamic {
    public GmObjectListNonDynamicDraw(int size) {
        super(size);
    }
    @Override public int add(GmObject obj) {
        obj.drawId = super.add(obj);
        return obj.stepId;
    }
    @Override public void remove(GmObject obj) {
        remove(obj.drawId);
    }
    @Override public int getId(GmObject obj) {
        return obj.drawId;
    }
}

//=================================================================================================================
//=================================================================================================================
//=================================================================================================================
