/*
 *  Tatris
 *  Copyright (C) April 2016, Daniel Wilson
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
package com.wilsonco.tatris;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import java.util.Arrays;
import java.util.LinkedList;

/**
 *
 * @author WILSON
 */
public class BlockRenderer extends GmObject{
    public final int width; // The width of the peice
    public final int height; // The height of the peice
    Block[][] blocks; // The array holding the blocks
    boolean[][] image; // The boolean representation of where the blocks are
    GmListContainer container; // A reference to the object list continer
    LinkedList<Block> blockList = new LinkedList<Block>(); // The list of blocks (unordered) for iterating when drawn
    //int blocksize = 16; // The width/height of the individual blocks
    TatrisWorldGrid grid;
    
    double idealAngle = 0; // Needed here so this variable (even though unused in static version) can be accsessed from both
    
    boolean updatedBlockPositions = false;
    Block defaultBlockType;
    
    BrickConfig brickConf;
    
    boolean spawnNewAfterFall = false;
    
    boolean trackY = false;
    
    double actualAngle = 0;
    
    private int count;
    boolean printGrid = false;
    
    // DEFAULT CREATION CODE =====================================================
    BlockRenderer(GmListContainer listForWorld,TatrisWorldGrid grid, BrickConfig bConf, GmScaleObject sc) {
        
        this.grid = grid;
        
        brickConf = bConf;
        
        updatePositionBeforeStep = true; // Make the blockRenderer object update it's position at the end of the step event
        
        width = bConf.image[0].length(); // Get the width
        height = bConf.image.length; // Get the height
        
        blocks = new Block[width][height]; // Create the block array
        
        this.image = Common.convertStringImage(bConf.image); // Convert the string image to boolean for convenience
        container = listForWorld; // 
        
        scaleObject = sc;
        
        int i = 0; int ii = 0; Block b; while (ii < height) { // Loop through each block
            while (i < width) {
                if (this.image[i][ii]) {
                    b = bConf.defaultBlockType.getDuplicate(i, ii, this, sc); // Create the block
                    //container.draw.setToAdd(b); //DON'T ADD BLOCK DRAW EVENT
                    blockList.add(b); // Add it to the block list for this object
                    blocks[i][ii] = b; // Add it to the block array
                }
                i++;
            } ii++; i=0;
        }
    }
    
    // CREATION CODE FOR WHEN SUPPLIED WITH BLOCK ARRAY =====================================================
    BlockRenderer(GmListContainer listForWorld,TatrisWorldGrid grid, Block[][] blockArray, GmScaleObject sc) {
        this.grid = grid;
        updatePositionBeforeStep = true; // Make the blockRenderer object update it's position at the end of the step event
        width = blockArray.length; height = blockArray[0].length;
        blocks = blockArray; // Create the block array
        this.image = Common.convertBlockImage(blockArray); // Convert the string image to boolean for convenience
        container = listForWorld; // 
        scaleObject = sc;
        int ix = 0; int iy = 0; Block b; while (iy < height) { // Loop through each block
            while (ix < width) {
                if (image[ix][iy]) {
                    b = blocks[ix][iy]; blockList.add(b); b.drawX = ix; b.drawY = iy; b.gridX = ix; b.gridY = iy;
                    b.parent = this;
                } ix++;
            } iy++; ix=0;
        }
    }
    
    @Override void step(SpriteBatch sb) {
        grid.addToGrid(this);
        updateBlockPositions();
        scaleObject = null;
    }
    
    @Override void endStep(SpriteBatch sb) {
        if (printGrid) {
            if (count % 60 == 0) {
                printImageGrid();
            }
            count++;
        }
    }
    
    void updateBlockPositions() {
        if (!updatedBlockPositions) {
            for (Block b : blockList) {
                b.x = x + b.drawX*grid.blockSize;
                b.y = y + b.drawY*grid.blockSize;
                updatedBlockPositions = true;
            }
        }
    }
    
    final void setScaleObjectForAllBlocks(GmScaleObject sc) {
        scaleObject = sc;
        for (Block b : blockList) {
            b.scaleObject = sc;
        }
    }
    
    @Override void draw(SpriteBatch sb) { // This has to be given a draw event - grid collision checking is done after all step events.
        int blockX = (int)x/grid.blockSize;
        int blockY = (int)y/grid.blockSize;
        int xx = Math.max(blockX,grid.viewControl.renderX);
        int yy = Math.max(blockY,grid.viewControl.renderY);
        int wdth = Math.min(blockX+width,grid.viewControl.renderX2) - xx;
        int hght = Math.min(blockY+height,grid.viewControl.renderY2) - yy;
        //System.out.println("HEIGHT: "+hght);
        if (wdth >= 0 && hght >= 0) {
            int ix=0, iy=0; while (iy < hght) {
                while (ix < wdth) {
                    //System.out.println("X: "+(xx+ix)+", Y: "+(yy+iy));
                    if (/*image[xx+ix][yy+iy] /*&&*/ blocks[xx+ix][yy+iy] != null) {
                       
                        blocks[xx+ix][yy+iy].draw(sb);
                    }
                    ix++;
                }
                iy++; ix=0;
            }
        }
        
        /*for (Block b : blockList) {
            b.draw(sb); // Run block draw events manually, so blocks are drawn at the same time this is.
        }*/
    }
    
    Block[][] getBlockArr() {
        return blocks;
    }
    LinkedList<Block> getBlockList() {
        return blockList;
    }
    boolean[][] getImageArr() {
        return image;
    }
    double getX() {
        return x;
    }
    double getY() {
        return y;
    }
    
    void addBlockRendererTo(BlockRenderer bRend) {
        int bRelX = grid.gridX(bRend.getX()) - grid.gridX(getX()); // Get the gridX...
        int bRelY = grid.gridY(bRend.getY()) - grid.gridY(getY()); // and gridY of the other BlockRenderer (relative to the main one)
        
        System.out.println("RENDERER X: "+bRend.getX());
        System.out.println("RENDERER Y: "+bRend.getY());
        
        /*main.blocks = */Common.addBlockArrays(blocks,bRend.getBlockArr(),bRelX,bRelY); // Add the block arrays
        /*main.image = */Common.addBooleanArrays(image,bRend.getImageArr(),bRelX,bRelY); // Add the boolean arrays
        LinkedList<Block> ll = bRend.getBlockList();
        for (Block b : ll) { // Loop through all of the blocks about to be added
            b.parent = this; // Change the parent to new parent
            b.drawX = b.gridX + bRelX;
            b.drawY = b.gridY + bRelY;
            b.gridX = b.drawX;
            b.gridY = b.drawY;
            if (bRend.idealAngle != bRend.actualAngle) { // Set block angles to ideal to avoid blocks staying diagonal.
                b.angle = bRend.idealAngle; // Only do it if ideal != actual angle as to not reset block rotation unnessacarily
            }
            updatedBlockPositions = false;
        }
        blockList.addAll(ll); // Add the block lists
    }
    
    void removeRegion(int x1,int y1,int x2,int y2,boolean removeDraw) {
        int ix = 0, iy = 0, width = x2-x1, height = y2-y1;
        //System.out.println("x1: "+x1);
        //System.out.println("y1: "+y1);
        //System.out.println("x2: "+x2);
        //System.out.println("y2: "+y2);
        //System.out.println("Width: "+width);
        //System.out.println("Height: "+height);
        while (iy < height) {
            while (ix < width) {
                Block b = blocks[ix+x1][iy+y1];
                if (b != null) {
                    blockList.remove(b);
                    if (removeDraw) {
                        container.draw.setToRemove(b);
                    }
                    blocks[ix+x1][iy+y1] = null;
                } ix++;
            } ix = 0; iy++;
        }
    }
    
    Block[][] getRegion(int x1, int y1, int x2, int y2) {
        int ix = 0, iy = 0, width = x2-x1, height = y2-y1;
        Block[][] out = new Block[width][height];
        while (iy < height) {
            while (ix < width) {
                out[ix][iy] = blocks[ix+x1][iy+y1];
                ix++;
            } ix = 0; iy++;
        }
        return out;
    }
    
    public BlockRendererDynamic transferRegionToNewBlockRenderer(int x1, int y1, int x2, int y2, boolean noRotate) {
        Block[][] bloc = getRegion(x1, y1, x2, y2);
        removeRegion(x1, y1, x2, y2, false);
        if (noRotate) {
            return new BlockRendererDynamicNoRotate(container,grid,bloc,scaleObject);
        } else {
            return new BlockRendererDynamic(container,grid,bloc,scaleObject);
        }
    }
    
    
    public void printImageGrid() {
        System.out.println("================================================");
        int xx = 0; int yy = 0;
        while (yy < height) {
            StringBuilder s = new StringBuilder();
            while (xx < width) {
                if (image[xx][yy]) {
                    s.append("#");
                } else {
                    s.append(".");
                }
                xx++;
            } xx = 0; yy++;
            System.out.println(s.toString());
        }
    }
    public void printBlockGrid() {
        System.out.println("================================================");
        int xx = 0; int yy = 0;
        while (yy < height) {
            StringBuilder s = new StringBuilder();
            while (xx < width) {
                if (blocks[xx][yy] == null) {
                    s.append(".");
                } else {
                    s.append("#");
                }
                xx++;
            } xx = 0; yy++;
            System.out.println(s.toString());
        }
    }
}



//======================================================================================================================
//======================================================================================================================
//======================================================================================================================
//======================================================================================================================
//======================================================================================================================
//======================================================================================================================
//======================================================================================================================
//======================================================================================================================
//======================================================================================================================
//======================================================================================================================
//======================================================================================================================
//======================================================================================================================
//======================================================================================================================
//======================================================================================================================
//======================================================================================================================
//======================================================================================================================
//======================================================================================================================
//======================================================================================================================
//======================================================================================================================
//======================================================================================================================
//======================================================================================================================
//======================================================================================================================
//======================================================================================================================
//======================================================================================================================




class BlockRendererDynamic extends BlockRenderer {
    
    public boolean respondToKeyboard;
    
    double idealX = 0; double idealY = 0;
    double actualX = 0; double actualY = 0;
    
    double idealOffsetX = 0; double idealOffsetY = 0;
    double actualOffsetX = 0; double actualOffsetY = 0;
    
     
    int angleIndex = 0;
    
    double minMovementStep = 2;
    double movementFactor = 0.88;
    
    double minRotationStep = 2;
    double rotationFactor = 0.88;
    
    double idealVspeed = 0;
    double idealIdealVspeed = 0;
    int stoppedCount = 0;
    
    double timeToJoinMultiplier = 1;
    
    //boolean leftPressedPrevious = false;
    //boolean rightPressedPrevious = false;
    
    BrickConfig blockConfig;
    
    boolean forceDraw = false; // SET THIS TO TRUE TO FORCE THE BLOCKRENDERER TO DRAW EVERY BLOCK.
    
    BlockRendererDynamic (GmListContainer listForWorld,TatrisWorldGrid grid,BrickConfig bConf,GmScaleObject sc) {
        super(listForWorld, grid, bConf, sc);
        blockConfig = bConf;
        System.out.println("CREATED BLOCKRENDERDYNAMIC");
    }
    
    BlockRendererDynamic (GmListContainer listForWorld,TatrisWorldGrid grid,Block[][] blockArray,GmScaleObject sc) {
        super(listForWorld, grid, blockArray,sc);
        System.out.println("CREATED BLOCKRENDERDYNAMIC");
    }
    
    @Override double getX() {
        return x + idealOffsetX;
    }
    @Override double getY() {
        return y + idealOffsetY;
    }
    
    private int getXOffsetIncrement(int dir) {
        if (dir == 0 || dir < -1 || dir > 1) return 0;
        int ind = angleIndex+dir;
        if (ind > 3) ind = 0;
        if (ind < 0) ind = 3;
        return blockConfig.offsetX[ind] - blockConfig.offsetX[angleIndex];
    }
    private int getYOffsetIncrement(int dir) {
        if (dir == 0 || dir < -1 || dir > 1) return 0;
        int ind = angleIndex+dir;
        if (ind > 3) ind = 0;
        if (ind < 0) ind = 3;
        return blockConfig.offsetY[ind] - blockConfig.offsetY[angleIndex];
    }
    
    private void moveHorizontally(int amount) {  // Allows the player to move the blocks from side to side
        if (amount == 0 || amount < -1 || amount > 1) { // If not inputted -1 or 1, end
            return;
        }
        final int offset = amount*grid.blockSize; // Calculate the offset to move by
        if (!grid.isCollidingWithGrid(this,offset, 0)) { // If the move will make the block collide with the grid, do nothing.
            if (grid.isCollidingWithGrid(this,offset,-grid.blockSize)) { // If there will be a block directly above after moving:
                x += offset; // Move x
                actualX -= offset; // Calculate actualX for blocks
                actualY -= Math.ceil(y/16)*16-y; // Calculate actualY for when y is moved down to next block
                y = Math.ceil(y/16)*16; // Round y down to next block
                stoppedCount = -(int)((-actualY)/idealVspeed);
                // The game must give the user more time to be able to move...
                // ... after the block has landed when it is slottded into
                // .. place sideways, as it takes time for the block to move.
                System.out.println("Moving around block");
            } else { // If there is no block immediately above after move:
                x += offset; // Add move offset
                actualX -= offset; // Calculate new block actualX
                System.out.println("Moving immediately");
            }
        }
    }
    
    private void rotate(int dir) {
        if (dir == 0 || dir < -1 || dir > 1) { // If not inputted -1 or 1, end
            return;
        }
        Block[][] b = Common.rotate2dBlockArray(blocks,dir);
        int offX = getXOffsetIncrement(-dir);
        int offY = getYOffsetIncrement(-dir);
        System.out.println("offX: "+offX);
        System.out.println("offY: "+offY);
        if (!Common.doBlockArraysCollide(grid.grid,b,grid.gridX(getX())+offX,grid.gridY(getY())+offY)) {
            blocks = b;
            image = Common.rotate2dBooleanArray(image,dir);
            for (Block bloc : blockList) {
                int xx = bloc.gridX;
                int yy = bloc.gridY;
                if (dir == -1) { 
                    bloc.gridX =  yy;
                    bloc.gridY = width-xx-1;
                } else {
                    bloc.gridX = height-yy-1;
                    bloc.gridY = xx;
                }
            }
            idealOffsetX += offX*grid.blockSize; idealOffsetY += offY*grid.blockSize;
            System.out.println("idealOffsetX: "+idealOffsetX);
            System.out.println("idealOffsetY: "+idealOffsetY);
            
            
            //actualX -= offX*grid.blockSize; actualY -= offY*grid.blockSize;
            idealAngle -= 90*dir;
            angleIndex -= dir;
            if (angleIndex > 3) angleIndex = 0;
            if (angleIndex < 0) angleIndex = 3;
            
        }
    }
    
    private void updateRelPositions() {
        if (actualX != idealX || actualY != idealY) {
            //System.out.println("SPAM");
            //double dir = Common.pointDirection(actualX,actualY,idealX,idealY);
            //System.out.println(dir);
            if (actualY != idealY) { // Try to get to correct y position, and....
                actualY = Common.makeCloser(idealY,actualY,movementFactor,minMovementStep);//Common.lengthDirY(minMovementStep,dir));
            } else if (actualX != idealX) { // ... only calculate x if already at y position.
                actualX = Common.makeCloser(idealX,actualX,movementFactor,minMovementStep);//Common.lengthDirX(minMovementStep,dir));
            }
        }
        
        if (actualAngle != idealAngle) {
            //System.out.println("ROTATE");
            actualAngle = Common.makeCloser(idealAngle,actualAngle,rotationFactor,minRotationStep);
        }
        
        if (actualOffsetX != idealOffsetX || actualOffsetY != idealOffsetY) {
            double dir = Common.pointDirection(idealOffsetX,idealOffsetY,actualOffsetX,actualOffsetY);
            
            if (actualOffsetX != idealOffsetX) {
                actualOffsetX = Common.makeCloser(idealOffsetX,actualOffsetX,movementFactor,Common.lengthDirX(minMovementStep,dir));//Common.lengthDirX(minMovementStep,dir));
            }
            if (actualOffsetY != idealOffsetY) {
                actualOffsetY = Common.makeCloser(idealOffsetY,actualOffsetY,movementFactor,Common.lengthDirY(minMovementStep,dir));//Common.lengthDirX(minMovementStep,dir));
            }
        }
    }
    
    @Override void updateBlockPositions() {
        Double centrePoint = ((double)grid.blockSize*width)/2 - 8;
        for (Block b : blockList) {
            if (!b.setLenDir) {
                b.updateLenDir(centrePoint);
            }
            
            b.x = x + actualOffsetX + actualX + centrePoint + Common.lengthDirX(b.len,b.dir+actualAngle);
            b.y = y + actualOffsetY + actualY + centrePoint + Common.lengthDirY(b.len,b.dir+actualAngle);
            
            b.angle = actualAngle;
            //actualAngle++;
            //b.x = x + b.drawX*blocksize + actualX;
            //b.y = y + b.drawY*blocksize + actualY;
        }
    }
    
    @Override void step(SpriteBatch sb) {
        updateRelPositions();
        updateBlockPositions();
        scaleObject = null;
    }
    
    @Override void draw(SpriteBatch sb) {
        // Empty to avoid nullPointerException caused by blockRenderer not having a sprite#
        // Collisions must be calculated on the draw cycle because everything in the draw cycle happens after everything in the step cycle.
        // THis means all collision checking must be done in the cycle after all of the collision grid has been generated.
        // This must also be done in the draw event to disable the draw event - if it is not disabled it throws a NullPointerException because ...
        // ... BlockRenderer has no default sprite.
            //=============== 1: CALCULATE MOVEMENT FROM KEYBOARD =========================
        if (trackY) {
            System.out.println(y);
        }
        if (respondToKeyboard && !grid.isGamePaused) {
            
            if (Gdx.input.isKeyPressed(Tatris.KeyBindings.moveJumpDownKey)) {idealVspeed = grid.blockSize*0.5;} else {idealVspeed = idealIdealVspeed;};
            
            if (actualX == idealX && actualY == idealY) {
                if (Gdx.input.isKeyPressed(Tatris.KeyBindings.moveLeftKey)) moveHorizontally(-1);
                if (Gdx.input.isKeyPressed(Tatris.KeyBindings.moveRightKey)) moveHorizontally(1);
            } else {
                if (/*!leftPressedPrevious &&*/ Gdx.input.isKeyJustPressed(Tatris.KeyBindings.moveLeftKey)) {
                    moveHorizontally(-1);
                }
                if (/*!rightPressedPrevious &&*/ Gdx.input.isKeyJustPressed(Tatris.KeyBindings.moveRightKey)) {
                    moveHorizontally(1);
                }
            }
            
            if (actualAngle == idealAngle) {
                if (Gdx.input.isKeyPressed(Tatris.KeyBindings.moveRotateLeftKey)) rotate(-1);
                if (Gdx.input.isKeyPressed(Tatris.KeyBindings.moveRotateRightKey)) rotate(1);
            } else {
                if (Gdx.input.isKeyJustPressed(Tatris.KeyBindings.moveRotateLeftKey)) {
                    rotate(-1);
                }
                if (Gdx.input.isKeyJustPressed(Tatris.KeyBindings.moveRotateRightKey)) {
                    rotate(1);
                }
            }
                
            //leftPressedPrevious = Gdx.input.isKeyPressed(grid.moveLeftKey);
            //rightPressedPrevious = Gdx.input.isKeyPressed(grid.moveRightKey);
        }
        
        
        //=============== 2: CHECK FOR END GAME, DO APPROPRIATE ACTIONS THEN TERMINATE CODE. =================
        
        BlockRenderer b = grid.getBlockRendererCollidingWidth(this,0,0);
        if (b != null) {
            idealVspeed = 0;
            vspeed = 0;
            respondToKeyboard = false;
            spawnNewAfterFall = false;
            grid.youLose = true;
            return;
        }
        
        //=============== 3: CHECK IF GAME IS PAUSED =========================================================
        
        if (grid.isGamePaused) {
            vspeed = 0;
        }
        
        //=============== 4: CALCULATE COLLISIONS =========================================================
        
        b = grid.getBlockRendererCollidingWidth(this,0,idealVspeed);
        if (b != null) { // If it will be colliding with a static block renderer next step
            vspeed = 0; // Stop moving
            stoppedCount++; // Increment the stopped count
            if (y % grid.blockSize != 0) { // If the block isn't snapped to the grid, (ypos) and it has stopped
                y = Math.ceil(y/((double)(grid.blockSize)))*((double)grid.blockSize); // Snap it to the grid to avoid pixel jumps when landed.
            }
            
            if (stoppedCount > (int)(grid.blockSize/idealVspeed)*timeToJoinMultiplier) { // Wait the same amount of time as it takes for the blockRenderer to fall one block
                grid.addBlockRenderers(b,this); // Add block renderer to static one
                grid.addToGrid(this); // Add shape to grid, so it is available in the grid at the end of the step cycle for the lines algorithm to process.
                x = 0; // TERMINATE BLOCK RENDERER (SOMEHOW)
                y = 0; // placeholder code - add termination here
                idealVspeed = 0;
                stoppedCount = 0;
                if (respondToKeyboard) {
                    grid.scoreShapesLanded++;
                    //grid.updateLevels();
                    grid.updateScoreGui();
                }
            }
        } else {
            stoppedCount = 0; // Make sure stopped count is zero
            if (/*vspeed == 0 &&*/ actualX == idealX && !grid.isGamePaused) { // If vspeed is zero, and blocks are in the correct place, and game is not paused:
                vspeed = idealVspeed; // Start moving.
            }
        }
        
        //======================== 4: RUN BLOCK DRAW EVENTS ======================================================
        
        if (forceDraw) {
            for (Block bloc : blockList) {
                bloc.draw(sb); // Run block draw events manually, so blocks are drawn at the same time this is.
                // Call the drawIfInRange function so each block checks if it is in range
            }
        } else {
            for (Block bloc : blockList) {
                bloc.drawIfInRange(sb); // Run block draw events manually, so blocks are drawn at the same time this is.
                // Call the drawIfInRange function so each block checks if it is in range
            }
        }
    }
}



class BlockRendererDynamicNoRotate extends BlockRendererDynamic {
    BlockRendererDynamicNoRotate (GmListContainer listForWorld,TatrisWorldGrid grid,Block[][] blockArray,GmScaleObject sc) {
        super(listForWorld, grid, blockArray, sc);
    }
    BlockRendererDynamicNoRotate (GmListContainer listForWorld,TatrisWorldGrid grid,BrickConfig bConf, GmScaleObject sc) {
        super(listForWorld, grid, bConf, sc);
    }
    
    
    @Override void updateBlockPositions() {
        for (Block b : blockList) {
            b.x = x + b.drawX*grid.blockSize;
            b.y = y + b.drawY*grid.blockSize;
            updatedBlockPositions = true;
        }
    }
}
