/*
 *  Tatris
 *  Copyright (C) April 2016, Daniel Wilson
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
package com.wilsonco.tatris;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import java.math.RoundingMode;
import java.text.DecimalFormat;

/**
 *
 * @author WILSON
 */
public class GuiRectangleObject extends GmObject implements InputProcessor {
    double lineWidth = 1;
    Color colour = new Color(1,1,1,1);
    BitmapFont font;
    double fontScale = 1;
    boolean drawAsShape = false;
    GuiRectangleObject(ShapeRenderer s) {
         super();
         shapeRenderer = s;
    }
    GuiRectangleObject(ShapeRenderer s, GmScaleObject scaleObject, double x, double y, double width, double height, double lineWidth, Color colour) {
        super();
        this.x = x; this.y = y;
        spriteWidth = width; spriteHeight = height;
        this.lineWidth = lineWidth;
        this.colour = colour;
        shapeRenderer = s;
        if (scaleObject != null) {
            this.scaleObject = scaleObject;
        }
    }
    @Override void draw(SpriteBatch sb) {
        rect(x,y,spriteWidth,spriteHeight,(float)lineWidth,shapeRenderer,sb,colour,Tatris.greenTex);
    }
    
    void rect(double relX, double relY, double wdth, double hght, float lineWidth, ShapeRenderer shp, SpriteBatch sb, Color col, Texture tex) {
        shp.setColor(col);
        float x1 = (float)(relX*scaleObject.scaleX + scaleObject.offsetX) + 1;
        float y1 = (float)(relY*scaleObject.scaleY + scaleObject.offsetY) + 1;
        float wdt = (float)(scaleObject.scaleX*wdth) - 2;
        float hgt = (float)(scaleObject.scaleY*hght) - 2;
        float w = (float)((lineWidth/2)*scaleObject.scaleX);
        float h = (float)((lineWidth/2)*scaleObject.scaleY);
        if (drawAsShape) {
            shp.rect(x1-w, y1-h, 2*w, 2*w + hgt); //0
            shp.rect(x1-w, y1-h, 2*w + wdt, 2*h); //1
            shp.rect(x1 + wdt - w, y1-h, 2*w, 2*h + hgt); //2
            shp.rect(x1-w, y1 + hgt - h, wdt + 2*w, 2*h); //3
        } else {
            sb.draw(tex,x1-w, y1-h, 2*w, 2*w + hgt); //0
            sb.draw(tex,x1-w, y1-h, 2*w + wdt, 2*h); //1
            sb.draw(tex,x1 + wdt - w, y1-h, 2*w, 2*h + hgt); //2
            sb.draw(tex,x1-w, y1 + hgt - h, wdt + 2*w, 2*h); //3
        }
    }
    void rectLine(double relX1, double relY1, double relX2, double relY2, float lineWidth, ShapeRenderer shp, Color col) {
        shp.setColor(col);
        float x1 = (float)(relX1*scaleObject.scaleX + scaleObject.offsetX);
        float y1 = (float)(relY1*scaleObject.scaleY + scaleObject.offsetY);
        float x2 = (float)(relX2*scaleObject.scaleX + scaleObject.offsetX);
        float y2 = (float)(relY2*scaleObject.scaleY + scaleObject.offsetY);
        shp.rectLine(x1,y1,x2,y2,(float)(lineWidth*scaleObject.scaleX));
        /*float w = (float)((lineWidth/2)*scaleObject.scaleX);
        float h = (float)((lineWidth/2)*scaleObject.scaleY);
        shapeRenderer.rect(x1-w, y1-h, 2*w, 2*w + hgt); //0
        shapeRenderer.rect(x1-w, y1-h, 2*w + wdt, 2*h); //1
        shapeRenderer.rect(x1 + wdt - w, y1-h, 2*w, 2*h + hgt); //2
        shapeRenderer.rect(x1-w, y1 + hgt - h, wdt + 2*w, 2*h); //3*/
    }
    
    void drawFont(double relX, double relY, double scaleX, double scaleY, SpriteBatch sb, String text) {
        float x1 = (float)(relX*scaleObject.scaleX + scaleObject.offsetX);
        float y1 = (float)(relY*scaleObject.scaleY + scaleObject.offsetY);
        setFontScale(scaleX, scaleY);
        font.setColor(1f,0.5f,1f,1f);
        font.draw(sb,text,x1+5, y1);
        font.setColor(1f,1f,1f,1f);
        font.draw(sb,text,x1, y1);
    }
    void drawFontInverted(double relX, double relY, double scaleX, double scaleY, SpriteBatch sb, String text) {
        float x1 = (float)(relX*scaleObject.scaleX + scaleObject.offsetX);
        float y1 = (float)(relY*scaleObject.scaleY + scaleObject.offsetY);
        setFontScale(scaleX, scaleY);
        font.setColor(1f,0.5f,1f,1f);
        font.draw(sb,text,x1+5, y1);
        font.setColor(0f,0f,0f,1f);
        font.draw(sb,text,x1, y1);
    }
    void setFontScale(double scaleX,double scaleY) {
        fontScale = spriteWidth/550;
        font.getData().setScale((float)(scaleObject.scaleX*fontScale*scaleX),-(float)(scaleObject.scaleY*fontScale*scaleY));
    }
    
    void hDivider(double n, SpriteBatch sb) {
        if (drawAsShape) {
            rectLine(0d,n*spriteHeight,(double)spriteWidth,n*spriteHeight,4,shapeRenderer,colour);
        } else {
            float x1 = (float)scaleObject.offsetX;
            float y1 = (float)(scaleObject.offsetY+((-lineWidth/2 + n*spriteHeight)*scaleObject.scaleY));
            float w = (float)(spriteWidth*scaleObject.scaleX);
            float h = (float)lineWidth-1;
            sb.draw(Tatris.greenTex,x1,y1,w,h);
        }
    }
    
    void vDivider(double n, SpriteBatch sb) {
        if (drawAsShape) {
            rectLine(n*spriteWidth,0,n*spriteWidth,spriteHeight,(float)lineWidth,shapeRenderer,colour);
        } else {
            float y1 = (float)scaleObject.offsetY;
            float x1 = (float)(scaleObject.offsetX+((-lineWidth/2 + n*spriteWidth)*scaleObject.scaleX));
            float h = (float)(spriteHeight*scaleObject.scaleY);
            float w = (float)lineWidth;
            sb.draw(Tatris.greenTex,x1,y1,w,h);
        }
    }

    @Override public boolean keyDown(int i) { return true; }
    @Override public boolean keyUp(int i) { return true; }
    @Override public boolean keyTyped(char c) { return true; }
    @Override public boolean touchDown(int i, int i1, int i2, int i3) { return true; }
    @Override public boolean touchUp(int i, int i1, int i2, int i3) { return true; }
    @Override public boolean touchDragged(int i, int i1, int i2) { return true; }
    @Override public boolean mouseMoved(int i, int i1) { return true; }
    @Override public boolean scrolled(int i) { return true; }
}


class GuiRectangleObjectWithBlackBorder extends GuiRectangleObject {
    int blackBorderWidth;
    Color blackColour = new Color(0,0,0,1);
    private double borderX = 0; private double borderY = 0;
    private double borderWidth = 0; private double borderHeight = 0;
    GuiRectangleObjectWithBlackBorder(ShapeRenderer s, int blackBorderWidth) {
         super(s);
         this.blackBorderWidth = blackBorderWidth;
    }
    GuiRectangleObjectWithBlackBorder(ShapeRenderer s, GmScaleObject scaleObject, double x, double y, double width, double height, double lineWidth, Color colour, int blackBorderWidth) {
        super(s,scaleObject,x,y,width,height,lineWidth,colour);
        this.blackBorderWidth = blackBorderWidth;
    }
    
    public void updateBlackBorderPositions() {
        borderX = x-0.5*(blackBorderWidth+lineWidth);
        borderY = y-0.5*(blackBorderWidth+lineWidth);
        borderWidth = spriteWidth+blackBorderWidth+lineWidth;
        borderHeight = spriteHeight+blackBorderWidth+lineWidth;
    }
    
    @Override void draw(SpriteBatch sb) {
        rect(x,y,spriteWidth,spriteHeight,(float)lineWidth,shapeRenderer,sb,colour,Tatris.greenTex);
        rect(borderX,
                borderY,
                borderWidth,
                borderHeight,
                (float)blackBorderWidth,
                shapeRenderer,
                sb,
                blackColour,
                Tatris.blackTex);
    }
}





class MainGuiDrawObject extends GuiRectangleObject {
    private GmObject guiBack;

    private String scoreText;/* = "[SCORE][+000]\n"+
                "[----------0]\n"+
                "[LEVEL[000]]\n"+
                "[--0%]\n"+
                "[LINES][0000]\n"+
                "[-0x]";*/
    TatrisWorldGrid grid;
    
    private int baseLineHeight = 80;
    
    MainGuiDrawObject(ShapeRenderer s, TatrisWorldGrid grid, BitmapFont f) {
         super(s);
         create(f,grid);
    }
    MainGuiDrawObject(ShapeRenderer s, TatrisWorldGrid grid, GmScaleObject scaleObject, BitmapFont f, double x, double y, double width, double height, double lineWidth, Color colour) {
        super(s,scaleObject,x,y,width,height,lineWidth,colour);
        create(f,grid);
    }
    
    
    
    private void drawFontYRatio(double relX, double relRatioY, double yOff, double scaleX, double scaleY, SpriteBatch sb, String text) {
        drawFont(relX,relRatioY*spriteHeight+yOff,scaleX,scaleY,sb,text);
    }
    
    private float getLineHeight(double scaleY) {
        return (float)(scaleObject.scaleY*fontScale*scaleY)*baseLineHeight;
    }
    
    private float getRelScaleForFontThingyX(double scaleX) {
        return (float)(scaleObject.scaleX*fontScale*scaleX);
    }
    
    private void create(BitmapFont f,TatrisWorldGrid grid) {
        guiBack = new GmObject(Tatris.menuBackground);
        guiBack.x = x; guiBack.y = y;
        guiBack.spriteWidth = spriteWidth;
        guiBack.spriteHeight = spriteHeight;
        guiBack.scaleObject = scaleObject;
        font = f; this.grid = grid;
        setFontScale(1,1);
        updateScoreText();
    }
    
    public void updateScoreText() {
        StringBuilder str = new StringBuilder("[SCORE][+");
        str.append(Common.fillStringLeft(grid.scorePointsLastGained+"","0",3));
        str.append("]\n[");
        str.append(Common.fillStringLeft(grid.scorePoints+"","-",11));
        str.append("]\n[LEVEL][");
        str.append(Common.fillStringLeft(grid.scoreLevel+"","0",4));
        str.append("]\n[");
        str.append(Common.fillStringLeft(grid.scoreLevelPercent+"","-",3));
        str.append("%]\n[LINES][");
        str.append(Common.fillStringLeft(grid.scoreLines+"","0",4));
        str.append("]\n[");
        
        DecimalFormat df = new DecimalFormat("#.00");
        df.setRoundingMode(RoundingMode.FLOOR);
        str.append(Common.fillStringLeft(df.format(grid.scoreSpeedMultiplier),"-",2));
        
        str.append("x]\n[SHAPES][");
        str.append(Common.fillStringLeft(grid.scoreShapesLanded+"","0",3));
        str.append("]");
        scoreText = str.toString();
    }
    
    @Override void draw(SpriteBatch sb) {
        double h = lineWidth/2;
        guiBack.draw(sb);
        rect(-h,-h,spriteWidth+lineWidth,spriteHeight+lineWidth,(float)lineWidth,shapeRenderer,sb,colour,Tatris.greenTex);
        
        drawFont(8,32,0.8,0.8,sb,"[NEXT]:\\\\");
        
        hDivider(0.25,sb);
        
        drawFontYRatio(0,0.25,32,1,1,sb,scoreText);
        
        drawFontYRatio(getRelScaleForFontThingyX(1)*256,0.25,32+getLineHeight(1)*3,0.5,0.5,sb,"TO NEXT");
        drawFontYRatio(getRelScaleForFontThingyX(1)*300,0.25,32+getLineHeight(1)*4.74,0.5,0.5,sb,"SPEED\nMULTIPLIER");
        
        //divider(0.82,sb);
        
        //drawFontYRatio(8,0.82,32,1,1,sb,"[PAUSE]\n[EXIT]");
        
        
        if (grid.youLose) {
            drawFontYRatio(-500,0.3,0,4,4,sb,"YOU\nLOSE");
        }
    }
}
