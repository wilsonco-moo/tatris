/*
 *  Tatris
 *  Copyright (C) April 2016, Daniel Wilson
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
package com.wilsonco.tatris;

/**
 *
 * @author wilson
 */

public class Common {
    public static boolean[][] convertStringImage(String[] s) {
        boolean [][] b = new boolean[s[0].length()][s.length];
        int i = 0; int ii = 0;
        for (String ss : s) {
            char[] chr = ss.toCharArray();
            for (char c : chr) {
                if (c != '.') {
                    b[ii][i] = true;
                } else {
                    b[ii][i] = false;
                }
                //System.out.println("Created block");
                ii++;
            }
            ii = 0; i++;
        }
        return b;
    }
    
    public static boolean[][] convertBlockImage(Block[][] b) {
        int width = b.length, height = b[0].length;
        boolean [][] out = new boolean[width][height];
        int ix = 0; int iy = 0;
        while (iy < height) {
            while (ix < width) {
                out[ix][iy] = (b[ix][iy] != null);
                ix++;
            } ix = 0; iy++;
        }
        return out;
    }
    
    public static Block[][] rotate2dBlockArray(Block[][] inp,int dir) {
        if (inp[0].length != inp.length || dir < -1 || dir == 0 || dir > 1) {
            return null;
        }
        Block[][] out = new Block[inp.length][inp[0].length];
        int size = inp.length; int ix = 0; int iy = 0;
        while (iy < size) {
            while (ix < size) {
                if (dir == 1) {
                    out[ix][iy] = inp[iy][size-ix-1];
                } else {
                    out[ix][iy] = inp[size-iy-1][ix];
                } ix++;
            } ix = 0; iy++;
        }
        return out;
    }
    
    public static boolean[][] rotate2dBooleanArray(boolean[][] inp,int dir) {
        if (inp[0].length != inp.length || dir < -1 || dir == 0 || dir > 1) {
            return new boolean[0][0];
        }
        boolean[][] out = new boolean[inp.length][inp[0].length];
        int size = inp.length, ix = 0, iy = 0;
        while (iy < size) {
            while (ix < size) {
                if (dir == 1) {
                    out[ix][iy] = inp[iy][size-ix-1];
                } else {
                    out[ix][iy] = inp[size-iy-1][ix];
                } ix++;
            } ix = 0; iy++;
        }
        return out;
    }
    
    public static void print2dBooleanArray(boolean[][] b) {
        int ix = 0, iy = 0, width = b.length, height = b[0].length;
        StringBuilder out = new StringBuilder();
        while (iy < height) {
            while (ix < width) {
                out.append((b[ix][iy])?'#':'.'); ix++;
            } out.append("\n"); ix = 0; iy++;
        }
        System.out.println(out.toString());
    }
    
    public static Block[][] addBlockArrays(Block[][] main, Block[][] other, int x, int y) {
        final int xStart = Math.max(x,0);
        final int yStart = Math.max(y,0);
        final int xEnd = Math.min(x+other.length,main.length);
        final int yEnd = Math.min(y+other[0].length,main[0].length);
        //System.out.println("Width: "+(xEnd-xStart)+" Height: "+(yEnd-yStart));
        int xx = xStart; int yy = yStart;
        
        while (yy < yEnd) {
            while (xx < xEnd) {
                if (other[xx-x][yy-y] != null) {      //[xx-xStart][yy-yStart]
                    main[xx][yy] = other[xx-x][yy-y]; //[xx-xStart][yy-yStart]
                }
                xx++;
            } xx = xStart; yy++;
        }
        return main;
    }
    
    public static boolean[][] addBooleanArrays(boolean[][] main, boolean[][] other, int x, int y) {
        final int xStart = Math.max(x,0);
        final int yStart = Math.max(y,0);
        final int xEnd = Math.min(x+other.length,main.length);
        final int yEnd = Math.min(y+other[0].length,main[0].length);
        //System.out.println("Width: "+(xEnd-xStart)+" Height: "+(yEnd-yStart));
        int xx = xStart; int yy = yStart;
        
        while (yy < yEnd) {
            while (xx < xEnd) {
                if (other[xx-x][yy-y]) {      //[xx-xStart][yy-yStart]
                    main[xx][yy] = other[xx-x][yy-y]; //[xx-xStart][yy-yStart]
                }
                xx++;
            } xx = xStart; yy++;
        }
        return main;
    }
    
    public static boolean doBlockArraysCollide(Block[][] main, Block[][] other, int x, int y) {
        final int xStart = Math.max(x,0); // x = -1 --> xStart = 0
        final int yStart = Math.max(y,0);
        final int xEnd = Math.min(x+other.length,main.length);
        final int yEnd = Math.min(y+other[0].length,main[0].length);
        int xx = xStart; int yy = yStart;
        
        while (yy < yEnd) {
            while (xx < xEnd) {
                //System.out.println("Block collision loop iteration running");
                if (main[xx][yy] != null && other[xx-x][yy-y] != null) { // other[xx-xStart][yy-yStart]
                    return true;
                }
                xx++;
            } xx = xStart; yy++;
        }
        return false;
    }
    
    public static Block getMainCollisionExample(Block[][] main, Block[][] other, int x, int y) {
        final int xStart = Math.max(x,0);
        final int yStart = Math.max(y,0);
        final int xEnd = Math.min(x+other.length,main.length);
        final int yEnd = Math.min(y+other[0].length,main[0].length);
        //Block b = null;
        int xx = xStart; int yy = yStart;
        //StringBuilder sMain = new StringBuilder("==========MAIN================\n");
        //StringBuilder sOther = new StringBuilder("---------OTHER----------------\n");
        while (yy < yEnd) {
            while (xx < xEnd) {
                //System.out.println("Block collision loop iteration running");
                if (main[xx][yy] != null && other[xx-x][yy-y] != null) { // other[xx-xStart][yy-yStart]
                    return main[xx][yy];
                }
                //if (main[xx][yy] == null) sMain.append('.'); else sMain.append('#');
                //if (other[xx-x][yy-y] == null) sOther.append('.'); else sOther.append('#');
                xx++;
            } xx = xStart; yy++; //sOther.append('\n'); sMain.append('\n');
        }
        //System.out.println(sMain.toString());
        //System.out.println(sOther.toString());
        return null;//b;
    }
    
    public static double pointDistance(double x1, double y1, double x2, double y2) {
        return Math.sqrt(Math.pow(x2-x1,2)+Math.pow(y2-y1,2));
    }
    
    public static double pointDirection(double x1, double y1, double x2, double y2) {
        return (Math.toDegrees(Math.atan2((x2-x1),(-y1+y2)))-90);
    }
    
    public static double lengthDirX(double len, double dir) {
        return len*Math.sin(Math.toRadians(dir+90));
    }
    
    public static double lengthDirY(double len, double dir) {
        return len*Math.sin(Math.toRadians(-dir));
    }
    
    
    public static double makeCloser(double ideal, double actual, double factor, double min) {
        Double n = ideal - (ideal-actual)*factor; // Makes 2 points closer by a factor, down to min.
        if (Math.abs(ideal-n) < min) {
            return ideal;
        } else {
            return n;
        }
    }
    
    
    
    
    public static String fillStringLeft(String str, String filler, int length) {
        int len = str.length();
        if (len >= length) {
            return str;
        }
        return new String(new char[length-len]).replace("\0",filler)+str;
    }
    public static String fillStringRight(String str, String filler, int length) {
        int len = str.length();
        if (len >= length) {
            return str;
        }
        return str+new String(new char[length-len]).replace("\0",filler);
    }
}
