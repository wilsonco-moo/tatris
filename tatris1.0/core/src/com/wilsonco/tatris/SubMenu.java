/*
 *  Tatris
 *  Copyright (C) April 2016, Daniel Wilson
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
package com.wilsonco.tatris;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

/**
 *
 * @author WILSON
 */
public class SubMenu extends GuiRectangleObject {
    String[] buttons;
    static Texture noSelectionTexStatic = new Texture(Gdx.files.internal("textures/subMenuNoSelection.png"));
    static Texture selectionTexStatic = new Texture(Gdx.files.internal("textures/subMenuSelection.png"));
    
    Texture noSelectionTex;
    Texture selectionTex;
    
    boolean respond = false;
    boolean draw = false;
    
    boolean respondToUp = true; // Whether the user pressing the up button should do something
    boolean respondToDown = true; // WHether the user pressing the down button should do something
    boolean respondToClose = true; // Whether the user pressing the close button should do something
    boolean respondToEnter = true; // Whether the user pressing the enter button should do something
    
    volatile boolean tempLock = true;
    volatile boolean tempLockLock = true; // Needed because menus are in seperate thread
    
    volatile int selected = 0;
    int len;
    
    double menuScale = 0.5;
    
    boolean drawNoSelectBack = true;
    
    int baseWidth = 512;
    int buttonHeight = 80;
    double buttonTextScale = 1;
    
    public SubMenu(ShapeRenderer s, String[] buttons, BitmapFont fnt, GmScaleObject scaleObject, double x, double y, double lineWidth, Color colour) {
        super(s, scaleObject, x, y, 512, 1, lineWidth, colour);
        this.buttons = buttons; font = fnt;
        create();
    }
    public SubMenu(ShapeRenderer s, String[] buttons, BitmapFont fnt) {
        super(s);
        this.buttons = buttons; font = fnt;
        create();
    }
    
    private void create() {
        noSelectionTex = SubMenu.noSelectionTexStatic;
        selectionTex = SubMenu.selectionTexStatic;
        len = buttons.length;
        drawAsShape = false;
        drawNoSelectBack = true;
        spriteWidth = baseWidth;
        spriteHeight = buttonHeight*buttons.length;
    }
    
    @Override final void draw(SpriteBatch sb) {
        if (draw) {
            rect(x-lineWidth/2-1,y-lineWidth/2-1,spriteWidth*scaleX+lineWidth+2,spriteHeight*scaleY+lineWidth+2,4,shapeRenderer,sb,colour,Tatris.greenTex);
            int i = 0;
            while (i < len) {
                if (selected == i || (selected != i && drawNoSelectBack)) {
                    sb.draw((selected == i)?selectionTex:noSelectionTex,
                        (float)(x*scaleObject.scaleX + scaleObject.offsetX),
                        (float)((y+80*i*scaleY)*scaleObject.scaleY + scaleObject.offsetY),
                        (float)(spriteWidth*scaleObject.scaleX*scaleX),
                        (float)(80d*scaleObject.scaleY*scaleY));
                }
                if (selected == i) {
                    drawFontInverted(x,y+(i*80+40)*scaleY,scaleX*buttonTextScale,scaleY*buttonTextScale,sb,buttons[i]);
                } else {
                    drawFont(x,y+(i*80+40)*scaleY,scaleX*buttonTextScale,scaleY*buttonTextScale,sb,buttons[i]);
                }
                i++;
            }
        }
    }
    
    @Override final void step(SpriteBatch sb) {
        onStep();
        if (tempLockLock) {
            tempLock = true; tempLockLock = false; 
        }
        if (!tempLock) {
            tempLockLock = true;
        }
    }
    
    @Override public boolean keyDown(int keyPressed) {
        if (respond && tempLock) {
                    System.out.println("KEY UP UP");
            if (respondToUp && keyPressed == Tatris.KeyBindings.menuUpKey) {
                selected--;
                System.out.println("UP");
                
            } else if (respondToDown && keyPressed == Tatris.KeyBindings.menuDownKey) {
                selected++;
                System.out.println("DOWN");
                
            } else if (respondToEnter && keyPressed == Tatris.KeyBindings.menuEnterKey) {
                enter();
                
            } else if (respondToClose && keyPressed == Tatris.KeyBindings.menuCloseKey) {
                toCloseMenu();
            }
            
            if (selected < 0) {
                selected = len-1;
            }
            if (selected >= len) {
                selected = 0;
            }
        }
        return true;
    }
    
    final void toCloseMenu() {
        respond = false;
        draw = false;
        close();
        selected = 0;
    }
    
    
    void enter() { // Run when the user clicks on/presses enter on a part of the menu
    }
    void close() { // Run when the user closes the menu
    }
    void onSelect() { // Run when a particular button is selected
    }
    void onStep() {
    }
}




class BottomRightCornerSubMenu extends SubMenu {
    GameRoom room;
    MainGuiDrawObject gui;
    public BottomRightCornerSubMenu(ShapeRenderer s, BitmapFont fnt, GmScaleObject scaleObject, Color colour, GameRoom G) {
        super(s, new String[]{"PAUSE","EXIT"}, fnt, scaleObject, 0, 0, 4, colour);
        respondToClose = false;
        respond = true;
        draw = true;
        room = G;
        gui = room.guiObject;
        x = 1; scaleX = 0.6; scaleY = 0.6; buttonTextScale = 0.8;
        baseWidth = (int)gui.spriteWidth;
        spriteWidth = gui.spriteWidth/scaleX - 3;
        y = gui.y + gui.spriteHeight - spriteHeight*scaleY;
    }
    @Override void enter() {
        switch (selected) {
        case 0:
            room.inGameMenu.draw = true;
            room.inGameMenu.respond = true;
            room.inGameMenu.tempLock = false;
            room.tatrisGrid.isGamePaused = true;
            respond = false;
            break;
        case 1:
            Gdx.app.exit();
            break;
        }
    }
    @Override void onStep() {
        if (respond && tempLock) {
            if (Gdx.input.isKeyJustPressed(Tatris.KeyBindings.menuCloseKey)) {
                selected = 0;
                enter();
            }
        }
    }
}

class InGameMenu extends SubMenu {
    GameRoom room;
    MainGuiDrawObject gui;
    public InGameMenu(ShapeRenderer s, BitmapFont fnt, GmScaleObject scaleObject, Color colour, GameRoom G) {
        super(s, new String[]{"CONTINUE","MAIN MENU","EXIT"}, fnt, scaleObject, 0, 0, 4, colour);
        room = G;
        gui = room.guiObject;
        x = Tatris.gameWidth/2 - spriteWidth/2;
        y = Tatris.gameHeight/2 - spriteHeight/2;
    }
    @Override void enter() {
        switch (selected) {
        case 0:
            draw = false;
            respond = false;
            room.bottomRightCornerSubMenu.respond = true;
            room.tatrisGrid.isGamePaused = false;
            room.bottomRightCornerSubMenu.tempLock = false;
            System.out.println("EEEEEEENNNNNTTTTTEEEEERRRRRRR");
            break;
        case 1:
            Tatris.game.currentRoom.destroy();
            Tatris.game.currentRoom = new MainMenuRoom();
            Tatris.game.currentRoom.create();
            break;
        case 2:
            Gdx.app.exit();
            break;
        }
    }
    @Override void close() {
        draw = false;
        respond = false;
        room.bottomRightCornerSubMenu.respond = true;
        room.tatrisGrid.isGamePaused = false;
        room.bottomRightCornerSubMenu.tempLock = false;
    }
}
