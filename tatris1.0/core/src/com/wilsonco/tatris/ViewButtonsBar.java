/*
 *  Tatris
 *  Copyright (C) April 2016, Daniel Wilson
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
package com.wilsonco.tatris;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

/**
 *
 * @author wilson
 */
public class ViewButtonsBar extends GuiRectangleObject {
    GmObject guiBack;
    TatrisWorldGrid grid;
    ViewButtonsBar(ShapeRenderer s, TatrisWorldGrid grid, BitmapFont f) {
         super(s);
         create(f,grid);
    }
    ViewButtonsBar(ShapeRenderer s, TatrisWorldGrid grid, GmScaleObject scaleObject, BitmapFont f, double x, double y, double width, double height, double lineWidth, Color colour) {
        super(s,scaleObject,x,y,width,height,lineWidth,colour);
        create(f,grid);
    }
    
    private void create(BitmapFont f,TatrisWorldGrid grid) {
        guiBack = new GmObject(Tatris.menuBackgroundTiny);
        guiBack.x = x; guiBack.y = y;
        guiBack.spriteWidth = spriteWidth;
        guiBack.spriteHeight = spriteHeight;
        guiBack.scaleObject = scaleObject;
        font = f; this.grid = grid;
        setFontScale(1,1);
    }
    
    @Override void draw(SpriteBatch sb) {
        guiBack.draw(sb);
        drawFont(0,16,0.6,0.6,sb,(double)(Math.round(grid.viewControl.view.scaleX*1000))/10d+"%");
        rect(x,y,spriteWidth,spriteHeight,(float)lineWidth,shapeRenderer,sb,colour,Tatris.greenTex);
        vDivider(0.5d,sb);
    }
}
