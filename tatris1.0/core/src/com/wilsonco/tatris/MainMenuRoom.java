/*
 *  Tatris
 *  Copyright (C) April 2016, Daniel Wilson
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
package com.wilsonco.tatris;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;

/**
 *
 * @author WILSON
 */
public class MainMenuRoom extends GmRoom {
    GmListContainer menuList;
    ShapeRenderer uiShapes;
    
    
    GuiRectangleObject menuOutsideRectangle;
    GmObject menuBack;
    GmObject logoObject;
    MainMenuSubMenu mainMenu;
    final int lineWidth = 4;
    final int logoWidth = 627;
    final int logoHeight = 328;
    
    @Override public void create() {
        world = new GmObjectWorld(Tatris.inputAdapter);
        menuList = world.add(new GmObjectListSemiDynamic(),new GmObjectListSemiDynamic());
        
        uiShapes = new ShapeRenderer();
        
        menuOutsideRectangle = new GuiRectangleObject(uiShapes,
                null,
                128 - lineWidth/2 - 1,
                1,
                Tatris.gameWidth - 256 + lineWidth + 1,
                Tatris.gameHeight - 2,
                lineWidth,
                Tatris.uiColour);
        menuList.draw.add(menuOutsideRectangle);
        
        
        menuBack = new GmObject(Tatris.menuBackground);
        menuBack.x = 128;
        menuBack.y = lineWidth;
        menuBack.spriteWidth = Tatris.gameWidth - 256;
        menuBack.spriteHeight = Tatris.gameHeight - lineWidth*2;
        menuList.draw.add(menuBack);
        
        logoObject = new GmObject(Tatris.logo);
        logoObject.spriteWidth = logoWidth;
        logoObject.spriteHeight = logoHeight;
        logoObject.x = (Tatris.gameWidth - logoWidth)/2;
        logoObject.y = Tatris.gameHeight*0.02;
        menuList.draw.add(logoObject);
        
        mainMenu = new MainMenuSubMenu(uiShapes,
                Tatris.mainFont,
                null,
                Tatris.uiColour,
                this);
        mainMenu.lineWidth = lineWidth;
        mainMenu.draw = true; mainMenu.respond = true; mainMenu.drawNoSelectBack = false;
        menuList.step.add(mainMenu); menuList.draw.add(mainMenu); world.inputObjects.add(mainMenu);
    }
    @Override public void updateStep(SpriteBatch sb) {
        menuList.updateStep(sb);
    }
    @Override public void updateDraw(SpriteBatch sb) {
        menuList.updateDraw(sb);
    }
    @Override public void destroy() {
        world.destroy();
        world = null;
    }
    @Override public void beforeEverything() {
        uiShapes.setProjectionMatrix(Tatris.game.camera.combined); // Set shapeRenderer camera
        uiShapes.begin(ShapeRenderer.ShapeType.Filled); // ShapeRenderer begin
    }
    @Override public void afterEverything() {
        uiShapes.end();
    }
}










class MainMenuSubMenu extends SubMenu {
    MainMenuRoom room;
    public MainMenuSubMenu(ShapeRenderer s, BitmapFont fnt, GmScaleObject scaleObject, Color colour, MainMenuRoom G) {
        super(s, new String[]{"START","OPTIONS","STATS","EXIT"}, fnt, scaleObject, 0, 0, 4, colour);
        room = G;
        x = Tatris.gameWidth/2 - spriteWidth/2;
        y = Tatris.gameHeight/1.5 - spriteHeight/2;
        respondToClose = false;
    }
    @Override void enter() {
        switch (selected) {
        case 0:
            room.destroy();
            Tatris.game.currentRoom = new GameRoom();
            Tatris.game.currentRoom.create();
            break;
        case 1:

            break;
        case 2:

            break;
        case 3:
            Gdx.app.exit();
            break;
        }
    }
}
