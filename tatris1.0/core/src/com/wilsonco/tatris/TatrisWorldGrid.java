/*
 *  Tatris
 *  Copyright (C) April 2016, Daniel Wilson
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
package com.wilsonco.tatris;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import java.util.Arrays;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.Random;

/**
 *
 * @author wilson
 */


public class TatrisWorldGrid extends GmObject { // Tatris.gameRoom.tatrisGrid
    public final int width;
    public final int height;
    public Block[][] grid;
    int count = 0;
    GmScaleObject scale;
    
    
    public final int blockSize;
    
    GameRoom room;
    
    LinkedList<BrickConfig> brickTypes = new LinkedList<BrickConfig>();
    Hashtable<String,Block> blockTypes = new Hashtable<String,Block>();
    
    boolean shouldCreateNextBrick = true;
    boolean shouldCheckForLines = false;
    
    public boolean isGamePaused = false;
    
    Random random = new Random();
    
    BlockRenderer staticBack;
    
    ShapeRenderer uiShapes;
    
    
    //==================SCORE VARIABLES=======================
    
    public int scoreLines = 0;
    public int scorePoints = 0;
    public int scoreLevel = 0;
    public int scoreLevelPercent = 0;
    
    public int scorePointsLastGained = 0;
    public double scoreSpeedMultiplier = 1;
    public int scoreShapesLanded = 0;
    
    public double scoreLastLevelIncrease = 0;
    public double scoreNextLevelIncrease = 40;
    
    public MainGuiDrawObject gui = null;
    
    private final int[] scoreLinesValues = new int[]{0,10,25,50,100};
    
    
    public final double fallSpeedBase = 16d/24d; // The base fall speed at the start of the game
    
    
    public double fallSpeedIncreaseLevel = 0; // The increase in fall speed (increment) per level increase
    public double fallSpeedIncreaseMultiplierLevel = 1; // The increase in fall speed (multiplier) per level increase
    
    public double fallSpeedIncreaseLine = Double.parseDouble((Gdx.files.internal("speed.txt").readString()));
// The increase in fall speed (increment) per line cycle
    public double fallSpeedIncreaseMultiplierLine = 1; // The increase in fall speed (multiplier) per line cycle
    
    BlockRendererDynamic nextBrickBlockRenderer;
    boolean notSetNextBlock = true;
    
    boolean youLose = false;
    
    public final CameraScalingControlObject viewControl;
    
    public final int gameActualOffsetX;
    public final int gameActualOffsetY;
    
    TatrisWorldGrid(int width, int height, int blockSize, GameRoom room) {
        gameActualOffsetX = room.gameOffsetX - (int)(blockSize*room.gameScale);
        gameActualOffsetY = room.guiOffsetY;
        scale = new GmScaleObject(room.gameScale,room.gameScale,gameActualOffsetX,gameActualOffsetY);
        this.width = width;
        this.height = height;
        grid = new Block[width][height];
        this.room = room;
        this.blockSize = blockSize;
        //================================== BLOCKS =============================================
        
        
        blockTypes.put("dummy",new DummyBlock(0,0,null,this,null)); // Dummy Block
        
        blockTypes.put("pink",new Block(0,0,new Texture(Gdx.files.internal("blocks/pink.png")),this,null));
        blockTypes.put("red",new Block(0,0,new Texture(Gdx.files.internal("blocks/red.png")),this,null));
        blockTypes.put("green",new Block(0,0,new Texture(Gdx.files.internal("blocks/green.png")),this,null));
        blockTypes.put("blue",new Block(0,0,new Texture(Gdx.files.internal("blocks/blue.png")),this,null));
        blockTypes.put("lightBlue",new Block(0,0,new Texture(Gdx.files.internal("blocks/lightBlue.png")),this,null));
        blockTypes.put("yellow",new Block(0,0,new Texture(Gdx.files.internal("blocks/yellow.png")),this,null));
        blockTypes.put("orange",new Block(0,0,new Texture(Gdx.files.internal("blocks/orange.png")),this,null));
        
        
        brickTypes.add(new BrickConfig(this,"zigzag1"));
        brickTypes.add(new BrickConfig(this,"zigzag2"));
        
        brickTypes.add(new BrickConfig(this,"L-block1"));
        brickTypes.add(new BrickConfig(this,"L-block2"));
        
        brickTypes.add(new BrickConfig(this,"line"));
        brickTypes.add(new BrickConfig(this,"tBlock"));
        brickTypes.add(new BrickConfig(this,"square"));
        
        //brickTypes.add(new BrickConfig(this,new String[]{"...",".#.","###"},"green"));
        
        
        /*
        
        When a blockRenderer is created, it needs 2 things:
        
        BrickConfig chooses the shape, and which blocks are in the shape.
        Blocks choose the attributes for each block.
        
        
        */
        
        uiShapes = new ShapeRenderer();
        
        viewControl = new CameraScalingControlObject(this,scale,
                room.gameOffsetX,
                room.gameOffsetY,
                room.gameWidth,
                room.gameHeight
        );
    }

    @Override void endStep(SpriteBatch sb) {
        if (count % 60 == 0) {
            //printGrid();
        }
        count++;
        
    }
    
    private void generateNextBlock() {
        int i = random.nextInt(7); // Pick a random number for the brick
        System.out.println("Spawning "+i);
        
        // Create it, by dragging a brick type from the lists
        nextBrickBlockRenderer = new BlockRendererDynamic(room.gameList,this,brickTypes.get( i ),room.nextBlockScaleObject);
        room.menuList.step.add(nextBrickBlockRenderer); // Add it to step
        room.menuList.draw.add(nextBrickBlockRenderer); // Add it to draw
        nextBrickBlockRenderer.forceDraw = true; // Force it to draw all of it's blocks even if they are outisde the render range.
        nextBrickBlockRenderer.x = 32; // Set it's x
        nextBrickBlockRenderer.y = 28; // Set it's y
        nextBrickBlockRenderer.updateBlockPositions(); // Update block positions as soon as the brick is created as to remove random block spawns at position 0,0
        
    }
    private void transferNextBlockAndGenerateNewOneForUseWhenANewBlockNeedsToBeSpawned() {
        System.out.println("Transferring");
        room.menuList.step.remove(nextBrickBlockRenderer);
        room.menuList.draw.remove(nextBrickBlockRenderer);
        room.gameList.step.setToAdd(nextBrickBlockRenderer);
        room.gameList.draw.setToAdd(nextBrickBlockRenderer);
        nextBrickBlockRenderer.x = 64; // Set it's x
        nextBrickBlockRenderer.y = -32; // Set it's y
        nextBrickBlockRenderer.idealVspeed = scoreSpeedMultiplier*fallSpeedBase; // Set it's speed
        nextBrickBlockRenderer.idealIdealVspeed = nextBrickBlockRenderer.idealVspeed; // Set it's speed
        nextBrickBlockRenderer.respondToKeyboard = true; // Tell it that it must respond to the keyboard
        nextBrickBlockRenderer.spawnNewAfterFall = true; // Tell it that it must spawn a new brick after it lands
        nextBrickBlockRenderer.forceDraw = false; // Stop it from force-drawing it's blocks - as this does not need to happen if the blockRenderer is in the world.
        nextBrickBlockRenderer.setScaleObjectForAllBlocks(scale);
        nextBrickBlockRenderer.updateBlockPositions(); // Update block positions as soon as the brick is created as to remove random block spawns at position 0,0
        generateNextBlock();
    }
    
    @Override void beginStep(SpriteBatch sb) { // BLOCKS GENERATED IN BEGIN STEP, SO BRICKS ARE ALWAYS GENERATED
        // THE STEP AFTER A BRICK HAS BEEN INTEGRATED INTO THE GRID. THIS ALLOWS THE LINES TO UPDATE BEFORE THE
        // BRICK IS CREATED. THIS WAY SPEED MULTIPLIERS FROM LINES/LEVELS TAKE AFFECT IMMEDIATELY.
        if (shouldCreateNextBrick) { // TO GENERATE NEXT BRICK
            if (notSetNextBlock) {
                System.out.println("GENERATING FIRST BLOCK");
                generateNextBlock();
                notSetNextBlock = false;
            }
            transferNextBlockAndGenerateNewOneForUseWhenANewBlockNeedsToBeSpawned();
            shouldCreateNextBrick = false; // Set this to false so a new brick is not spawned next step
            
        }
    }
    
    @Override void step(SpriteBatch sb) {
        viewControl.update();
    }
    
    @Override void draw(SpriteBatch sb) {} // Disable draw (temp)
    
    public void resetGrid() {
        if (shouldCheckForLines) { // If lines should be checked
            checkForLines(); // Run the line-checking function
            shouldCheckForLines = false; // Set this to false so line checking is only done once
        }// This must be done on resetgrid as it is the last event - everything must be drawn in the grid.
        for (Block[] b : grid) { // Reset grid
            Arrays.fill(b,null); // Fill grid with null refererences.
        }
    }
    
    public void printGrid() {
        System.out.println("================================================");
        int xx = 0; int yy = 0;
        while (yy < height) {
            StringBuilder s = new StringBuilder();
            while (xx < width) {
                if (grid[xx][yy] != null) {
                    s.append("#");
                } else {
                    s.append(".");
                }
                xx++;
            } xx = 0; yy++;
            System.out.println(s.toString());
        }
    }
    
    public void addBlockRenderers(BlockRenderer main, BlockRenderer other) { // Adds a blockrenderer to a main static block renderer at position 0,0
        if (other.spawnNewAfterFall) {
            shouldCreateNextBrick = true;
            shouldCheckForLines = true;
        }
        main.addBlockRendererTo(other);
        room.gameList.step.setToRemove(other);
        room.gameList.draw.setToRemove(other);
    }
    
    public boolean isCollidingWithGrid(BlockRenderer b,double offsetX,double offsetY) {
        int bX = gridX(b.getX()+offsetX);
        int bY = gridY(b.getY()+offsetY);
        return Common.doBlockArraysCollide(grid,b.blocks,bX,bY);
    }
    
    public BlockRenderer getBlockRendererCollidingWidth(BlockRenderer b, double offsetX, double offsetY) {
        int bX = gridX(b.getX()+offsetX);
        int bY = gridY(b.getY()+offsetY);
        Block bloc = Common.getMainCollisionExample(grid,b.blocks,bX,bY);
        
        if (bloc != null) {
            return bloc.parent;
        }
        return null;
    }
    
    public void addToGrid(BlockRenderer b) {
        int bX = gridX(b.getX());
        int bY = gridY(b.getY());
        /*grid = */Common.addBlockArrays(grid,b.blocks,bX,bY);
    }
    
    public int gridX(double xx) {
        return (int)Math.floor(xx/16);
    }
    public int gridY(double yy) {
        return (int)Math.ceil(yy/16);
    }
    
    //================================== REMOVING LINES ========================================================
    
    void checkForLines() {
        int ix = 1, iy = 0; boolean hasGotFullLine; // Set up a few variables - ix=1 as to not check first column
        LinkedList<DistPointThing> rem = new LinkedList<DistPointThing>(); // Create a DistPointThing list to store where lines are
        int lastFullLine = 0; // Stores where the last row after the last full line was
        int linesRemoved = 0;
        while (iy < height-1) { // Loop through all columns except the last one - the bottom
            hasGotFullLine = true; // Set this to true
            while (ix < width-1) { // Loop through the row excluding first and last edge blocks
                if (grid[ix][iy] == null) { // If empty,
                    hasGotFullLine = false; break; // Full line set to false, break loop.
                } ix++;
            }
            if (hasGotFullLine) { // If full line,
                linesRemoved++; // Increment lines removed
                System.out.println("Line "+iy+" full"); // output debug info
            }
            if (hasGotFullLine) { // If full line,
                grid[0][iy].parent.removeRegion(1,iy,width-1,iy+1,true); // Remove the line area
                if (iy-lastFullLine > 0) { // If the distance between the line and last full line is greater than 1 block@
                    rem.add(new DistPointThing(lastFullLine,iy,grid[0][iy].parent)); // Add a distPointTHing, store the last full row and the coordinates to the new one.
                    System.out.println("Creating: "+lastFullLine+" to "+iy); // Output debug
                }
                lastFullLine = iy+1; // Set the lastFullLine to iy + 1  --> the last row after the last full line was
            } ix = 0; iy++; // Increment iy, reset ix.
        } 
        // This way, distPointThings store the coordinates of all the places where seperate blockRenderers
        // need to be created, while grouping them into groups of rows. This means that if 4 rows fall, all 4
        // rows are put into the same blockRenderer to fall. It also means that if 3 sets of 4 rows need to fall,
        // they are all grouped into 3 seperate 4-row blockRenderers to fall, thus allowing the default collision
        // system to run.
        
        for (DistPointThing dst : rem) { // Loop through distPointThings.
            BlockRendererDynamic b = dst.bRend.transferRegionToNewBlockRenderer(1,dst.y1,width-1,dst.y2,true);
            // Transfer the region specified by the DistPointThing to a new BlockRenderer - remove it from one and
            // create another with the same transferred blocks. THis way the block objects are kept, but simply
            // moved from one to another.
            room.gameList.step.add(b); // Add to step list
            room.gameList.draw.add(b); // Add to draw list - notice setToAdd is not used because this function runs in the reset stage - outside of any event. This way no CocurrentModificationExceptions can happen, so delayed adding is not needed.
            //b.trackY = true; // Track Y position for debug purposes - no longer needed.
            b.x = blockSize; // Set x to blocksSize, because the first column is ignored
            b.y = dst.y1*blockSize; // Set y to the area's y coordinate * blockSize as to keep it's y position.
            b.idealVspeed = 1; // Set it's ideal Vspeed - so the blockRenderer moves down to join with the rest of the grid.
            b.timeToJoinMultiplier = 0; // Set it's timeToJoinMultiplier, so it joins onto the grid as soon as it touches.
            System.out.println("Creating blockRenderer at x:"+b.x+" y:"+b.y+" at coords: "+1+","+dst.y1+" "+(width-1)+","+dst.y2); // DEBUG
        }
        
        // SCORE CALCULATION SYSTEM. WORKS BY LINESREMOVED.
        
        if (linesRemoved > 0) {
            incrementScoreByLinesRemoved(linesRemoved);
            increaseFallSpeedLine();
            updateLevels();
            updateScoreGui();
        }
    }
    
    
    //=================================== SCORE FUNCTIONS =======================================
    
    public void increaseFallSpeedLevel() { // Run per level increase
        scoreSpeedMultiplier *= fallSpeedIncreaseMultiplierLevel;
        scoreSpeedMultiplier += fallSpeedIncreaseLevel;
    }
    public void increaseFallSpeedLine() { // Run per line increase
        scoreSpeedMultiplier *= fallSpeedIncreaseMultiplierLine;
        scoreSpeedMultiplier += fallSpeedIncreaseLine;
    }
    
    public void incrementScoreByLinesRemoved(int linesRemoved) {
        scoreLines += linesRemoved;
        scorePointsLastGained = (int)(((float)(width-2))/12f * ((float)scoreLinesValues[linesRemoved]));
        scorePoints += scorePointsLastGained;
    }
    
    public void updateLevels() {
        while (scorePoints >= scoreNextLevelIncrease) {
            scoreLevel++;
            scoreLastLevelIncrease = scoreNextLevelIncrease;
            scoreNextLevelIncrease *= 1.2;
            increaseFallSpeedLevel();
        }
        scoreLevelPercent = (int)((
                ( (double)scorePoints - scoreLastLevelIncrease )
                /
                ( (double)scoreNextLevelIncrease - scoreLastLevelIncrease)
                )*100d);
        
        System.out.println("Next level score: "+scoreNextLevelIncrease);
        System.out.println("Last level score: "+scoreLastLevelIncrease);
    }
    
    public void updateScoreGui() {
        if (gui != null) {
            gui.updateScoreText();
        }
    }
    
    //================================= BLOCK AND BRICK LISTING SYSTEM ===========================================
    
}




class DistPointThing {
    int y1;
    int y2;
    BlockRenderer bRend;
    DistPointThing(int y1, int y2, BlockRenderer bRend) {
        this.y1 = y1; this.y2 = y2; this.bRend = bRend;
    }
}






class Block extends GmObject {
    public int drawX; // The x position relative to the block renderer
    public int drawY; // The y position relative to the block renderer
    public int gridX;
    public int gridY;
    
    double len = 0;
    double dir = 0;
    boolean setLenDir = false;
    TatrisWorldGrid grid;
    
    /*
    A not about the difference between relx, rely, newRotX, and newRotY.
    
    When a tertis blockRenderer is rotated, all of the block arrays
    are rotated. However, the blocks are not drawn at the new positions,
    their draw positions are worked out with an angle and their old positions.
    The old positions are relx and rely - do not change.
    However, when a blockRenderer is joined onto another block renderer,
    new positions are needed. These are newRotX and newRotY. These DO change
    when the blockRenderer is rotated, and these are updated to be the new
    positions.
    
    */
    public BlockRenderer parent;
    Block(int x, int y, BlockRenderer parent, Texture tex, TatrisWorldGrid grid, GmScaleObject s) {
        this.grid = grid;
        this.parent = parent;
        drawX = x; drawY = y;
        gridX = x; gridY = y;
        setSprite(tex);
        spriteWidth = grid.blockSize; spriteHeight = grid.blockSize;
        transformOriginX = grid.blockSize/2; transformOriginY = grid.blockSize/2;
        scaleObject = parent.grid.scale;
        if (s != null) scaleObject = s;
    }
    
    Block(int x, int y, Texture tex, TatrisWorldGrid grid, GmScaleObject s) {
        this.grid = grid;
        drawX = x; drawY = y;
        gridX = x; gridY = y;
        setSprite(tex);
        spriteWidth = grid.blockSize; spriteHeight = grid.blockSize;
        transformOriginX = grid.blockSize/2; transformOriginY = grid.blockSize/2;
        if (s != null) scaleObject = s;
    }
    
    public void updateLenDir(Double centrePoint) {
        double xxx = drawX*parent.grid.blockSize;
        double yyy = drawY*parent.grid.blockSize;
        len = Common.pointDistance(centrePoint,centrePoint,xxx,yyy);
        dir = Common.pointDirection(centrePoint,centrePoint,xxx,yyy);
        setLenDir = true;
        //System.out.println("\nX: "+xxx);
        //System.out.println("Y: "+yyy);
    }
    
    
    public Block getDuplicate(int x, int y, BlockRenderer parent, GmScaleObject s) {
        return new Block(x,y,parent,sprite,grid,s);
    }
    
    public void drawIfInRange(SpriteBatch sb) {
        if (grid.viewControl.isRelativePositionInRenderRangeWithRoundingAjustment(x-originX,y-originY,grid.viewControl.extraMargin)) {
            //System.out.println("x: "+(x+1d/65536d)+" IN RANGE");
            draw(sb);
        }/* else {
            System.out.println("x: "+(x+1d/65536d)+" NOT IN RANGE");
        }*/
    }
    
    /*@Override void draw(SpriteBatch sb) {
        super.draw(sb);
        sb.draw(Tatris.greenTex,
                    ((float)x - (float)originX) * (float)scaleObject.scaleX + (float)scaleObject.offsetX, // x
                    ((float)y - (float)originY) * (float)scaleObject.scaleY + (float)scaleObject.offsetY, // y
                    0, // originx
                    0, // originy
                    2, // width
                    2, // height
                    1, // scalex
                    1, // scaley
                    0, // rotation
                    0, // srcX
                    0, // srcY
                    2, // srcWidth
                    2, // srcHeight
                    (flipX), // flipX
                    (!flipY) // flipY
                    );
    }*/
}


class DummyBlock extends Block { // A block that exists but is not drawn - world edge
    DummyBlock(int x, int y, BlockRenderer parent, Texture tex, TatrisWorldGrid grid, GmScaleObject sc) {
        super(x,y,parent,tex, grid, sc);
        sprite = null; // No sprite
    }
    DummyBlock(int x, int y, Texture tex, TatrisWorldGrid grid, GmScaleObject sc) {
        super(x,y,tex, grid, sc);
        sprite = null; // No sprite
    }
    @Override public Block getDuplicate(int x, int y, BlockRenderer parent, GmScaleObject sc) {
        return new DummyBlock(x,y,parent,null,grid,sc);
    }
    @Override void draw(SpriteBatch sb) {} // No draw event
}
