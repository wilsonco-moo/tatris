/*
 *  Tatris
 *  Copyright (C) April 2016, Daniel Wilson
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
package com.wilsonco.tatris;

import com.badlogic.gdx.Gdx;
import java.awt.Insets;

/**
 *
 * @author WILSON
 */
public class CameraScalingControlObject {
    
    final TatrisWorldGrid grid; // FINAL REFERENCES
    final GmScaleObject view;
    
    public final int viewX; // FINAL VIEW COORDINATES
    public final int viewY; // The scale and position of the grid scale object
    public final int viewX2; // are compared with these - the real pixel screen
    public final int viewY2; // bounds for the view area, to work out the render
    public final int viewWidth; // bounds in relative blocks.
    public final int viewHeight;
    
    // RELATIVE VIEW X,Y,WIDTH,HEIGHT ARE STORED IN VIEW OBJECT
    
    
    
    // ------------ ZOOM CONTROL VARIABLES ------------------------
    private int zoomValue; // The zoom value - this uses exponentation to then work out the scale.
    // 2^zoomValue --> zoomScale.
    private float zoomStep = 0.5f; // The multiplier for the zoom value - the smaller this is the more zoom increments there will be
    private double actualZoomValue = 0; // The actual zoom value - directly used to work out scale
    private double idealZoomValue = 0; // The ideal zoom value
    
    // ------------------ ZOOM CENTERING CONTROL VARIABLES --------------------
    private double viewCentreX;
    private double viewCentreY;
    private double relativeViewX;
    private double relativeViewY;
    
    
    // ---------------- MOVE CONTROL VARIABLES
    private final int cameraMoveSpeed = 8;
    
    
    
    
    
    
    public Insets renderMargin = new Insets(
            0, // TOP
            0, // LEFT
            1, // BOTTOM
            1 // RIGHT
    );
    
    public Insets extraMargin = new Insets(
            1, // TOP
            1, // LEFT
            1, // BOTTOM
            0 // RIGHT
    );
    
    public int renderX = 0; // THE RENDER BOUNDS. THESE ARE CALCULATED FROM THE VIEW POSITION
    public int renderY = 0; // AND THE OFFSETS OF THE SCALE OBJECT.
    public int renderWidth = 0;
    public int renderHeight = 0; // RENDER RANGE IS MEASURED IN GRID POSITIONS.
    public int renderX2 = 0;
    public int renderY2 = 0;
    
    CameraScalingControlObject(TatrisWorldGrid grid, GmScaleObject view, int x, int y, int width, int height) {
        
        this.grid = grid; // COLLECT AND STORE VARIABLES
        this.view = view;
        viewX = x; viewY = y; viewWidth = width; viewHeight = height;
        viewX2 = x+width; viewY2 = y+height;
        view.scaleY = view.scaleX;
        
        zoomValue = (int)(Math.round(Math.log(view.scaleX)/Math.log(2))/zoomStep); // SET UP ZOOM CONTROL VARIABLES
        actualZoomValue = zoomValue*zoomStep;
        idealZoomValue = actualZoomValue;
        System.out.println("DEFAULT ZOOM VALUE: "+zoomValue);
        
        viewCentreX = getRelativeXFromGridX(grid.width/2) * view.scaleX;
        viewCentreY = getRelativeXFromGridX(grid.height/2) * view.scaleY;
        
        relativeViewX = getRelativeXFromGridX(grid.width/2);
        relativeViewY = getRelativeXFromGridX(grid.height/2);
    }
    
    private void zoom(int amount) {
        zoomValue += amount;
        idealZoomValue = zoomValue*zoomStep;
    }
    
    public void update() {
        if (idealZoomValue != actualZoomValue) {
            actualZoomValue = Common.makeCloser(idealZoomValue,actualZoomValue,0.9,0.01);
            view.scaleX = Math.pow(2, actualZoomValue);
            view.scaleY = view.scaleX;
        }
        
        view.offsetX = grid.gameActualOffsetX + viewCentreX - relativeViewX*view.scaleX;
        view.offsetY = grid.gameActualOffsetY + viewCentreY - relativeViewY*view.scaleY;
        
        if (Gdx.input.isKeyPressed(Tatris.KeyBindings.cameraLeftKey)) relativeViewX -= cameraMoveSpeed/view.scaleX;
        if (Gdx.input.isKeyPressed(Tatris.KeyBindings.cameraUpKey)) relativeViewY -= cameraMoveSpeed/view.scaleY;
        if (Gdx.input.isKeyPressed(Tatris.KeyBindings.cameraDownKey)) relativeViewY += cameraMoveSpeed/view.scaleY;
        if (Gdx.input.isKeyPressed(Tatris.KeyBindings.cameraRightKey)) relativeViewX += cameraMoveSpeed/view.scaleX;
        if (Gdx.input.isKeyJustPressed(Tatris.KeyBindings.cameraZoomInKey)) zoom(1);
        if (Gdx.input.isKeyJustPressed(Tatris.KeyBindings.cameraZoomOutKey)) zoom(-1);

        renderX = (int) Math.floor(getGridXFromScreenX(viewX)) - renderMargin.left;
        renderY = (int) Math.floor(getGridYFromScreenY(viewY)) - renderMargin.top;
        renderX2 = (int) Math.floor(getGridXFromScreenX(viewX2)) + renderMargin.right;
        //System.out.println("viewY2: "+viewY2);
        //System.out.println("ViewY2ScreenPos: "+renderY2);
        renderY2 = (int) Math.floor(getGridYFromScreenY(viewY2)) + renderMargin.bottom;
        renderWidth = renderX2-renderX;
        renderHeight = renderY2-renderY;
    }
    
    // NOTE: ACTUALLY LESS EFFICIENT TO USE THE INTEGER VERSIONS OF THE METHODS:
    // isRelativePositionInRenderRange(), isScreenPositionInRenderRange()
    // ONLY USE IF NEEDED EXPLICITLY AS AN INTEGER - AS INTEGERS CAUSE A SERIES OF TYPE CASTING TO NEED TO BE DONE
    // IF THE VALUE YOU HAVE IS A DOUBLE, SEND IT INTO THE METHODS AS A DOUBLE, AS LESS TYPE CASTING IS USED.
    // ALSO, DO NOT CONVERT AN INTEGER TO A DOUBLE TO USE IN THESE METHODS, AS THAT IN ITSELF WILL
    // REQUIRE UN-NESSACARY TYPE CASTING - THE FUNCTIONS DO THIS ANYWAY.
    
    public boolean isGridPositionInRenderRange(int x, int y) {
        return x >= renderX && x < renderX2 && y >= renderY && y < renderY2;
    }
    public boolean isGridPositionInRenderRange(double x, double y) {
        return x >= renderX && x < renderX2 && y >= renderY && y < renderY2;
    }
    
    public boolean isGridPositionInRenderRange(int x, int y, Insets margin) {
        return x >= renderX-margin.left && x < renderX2+margin.right && y >= renderY-margin.top && y < renderY2+margin.bottom;
    }
    public boolean isGridPositionInRenderRange(double x, double y, Insets margin) {
        return x >= renderX-margin.left && x < renderX2+margin.right && y >= renderY-margin.top && y < renderY2+margin.bottom;
    }
    
    
    
    public boolean isRelativePositionInRenderRange(int x, int y) {
        return isGridPositionInRenderRange(getGridXFromRelativeX(x),getGridYFromRelativeY(y));
    }
    public boolean isRelativePositionInRenderRange(double x, double y) {
        return isGridPositionInRenderRange(getGridXFromRelativeX(x),getGridYFromRelativeY(y));
    }
    final double roundingAjustment = 1d/256d;
    public boolean isRelativePositionInRenderRangeWithRoundingAjustment(double x, double y) {
        return isGridPositionInRenderRange(getGridXFromRelativeX(x+roundingAjustment),getGridYFromRelativeY(y+roundingAjustment));
    }
    
    public boolean isRelativePositionInRenderRange(int x, int y, Insets margin) { return isGridPositionInRenderRange(getGridXFromRelativeX(x),getGridYFromRelativeY(y),margin); }
    public boolean isRelativePositionInRenderRange(double x, double y, Insets margin) { return isGridPositionInRenderRange(getGridXFromRelativeX(x),getGridYFromRelativeY(y),margin); }
    public boolean isRelativePositionInRenderRangeWithRoundingAjustment(double x, double y,Insets margin) { return isGridPositionInRenderRange(getGridXFromRelativeX(x+roundingAjustment),getGridYFromRelativeY(y+roundingAjustment),margin); }
    
    
    public boolean isScreenPositionInRenderRange(int x, int y) {
        return isGridPositionInRenderRange(getGridXFromScreenX(x),getGridYFromScreenY(y));
    }
    public boolean isScreenPositionInRenderRange(double x, double y) {
        return isGridPositionInRenderRange(getGridXFromScreenX(x),getGridYFromScreenY(y));
    }
    
    //=============================== BIG PILE OF POSITION CONVERSION FUNCTIONS ======================================
    
    // VIEW OFFSETX AND OFFSETY MEASURED IN SCREEN POSITION.
    
    // SCREEN POSITION:   Absolute position (in pixels) on screen --> relative to origin of window.
    // RELATIVE POSITION: Position (in pixels) in the drawn world - relative to origin, takes into account view scale.
    // GRID POSITION:     Position (in blocks) in the drawn world - relative to origin, takes into account view scale.
    
    
    // DOUBLE FUNCTIONS
    public double getRelativeXFromScreenX(double x) {
        return (x - view.offsetX)/view.scaleX;
    }
    public double getRelativeYFromScreenY(double y) {
        return (y - view.offsetY)/view.scaleY;
    }
    
    public double getGridXFromScreenX(double x) {
        return (x - view.offsetX)/(view.scaleX*grid.blockSize);
    }
    public double getGridYFromScreenY(double y) {
        return (y - view.offsetY)/(view.scaleY*grid.blockSize);
    }
    
    
    public double getScreenXFromRelativeX(double x) {
        return x*view.scaleX + view.offsetX;
    }
    public double getScreenYFromRelativeY(double y) {
        return y*view.scaleY + view.offsetY;
    }
    
    public double getGridXFromRelativeX(double x) {
        return x/grid.blockSize;
    }
    public double getGridYFromRelativeY(double y) {
        return y/grid.blockSize;
    }
    
    
    public double getScreenXFromGridX(double x) {
        return x*grid.blockSize*view.scaleX + view.offsetX;
    }
    public double getScreenYFromGridY(double y) {
        return y*grid.blockSize*view.scaleY + view.offsetY;
    }
    
    public double getRelativeXFromGridX(double x) {
        return x*grid.blockSize;
    }
    public double getRelativeYFromGridY(double y) {
        return y*grid.blockSize;
    }
    
    
    
    // INTEGER FUNCTIONS
    public int getRelativeXFromScreenX(int x) {
        return (int)(((double)(x) - view.offsetX)/view.scaleX);
    }
    public int getRelativeYFromScreenY(int y) {
        return (int)(((double)(y) - view.offsetY)/view.scaleY);
    }
    
    public int getGridXFromScreenX(int x) {
        return (int)(((double)(x) - view.offsetX)/view.scaleX) /grid.blockSize;
    }
    public int getGridYFromScreenY(int y) {
        return (int)(((double)(y) - view.offsetY)/view.scaleY) / grid.blockSize;
    }
    
    
    public int getScreenXFromRelativeX(int x) {
        return (int)((double)(x)*view.scaleX + view.offsetX);
    }
    public int getScreenYFromRelativeY(int y) {
        return (int)((double)(y)*view.scaleY + view.offsetY);
    }
    
    public int getGridXFromRelativeX(int x) {
        return x/grid.blockSize;
    }
    public int getGridYFromRelativeY(int y) {
        return y/grid.blockSize;
    }
    
    
    public int getScreenXFromGridX(int x) {
        return (int)((double)(x*grid.blockSize)*view.scaleX + view.offsetX);
    }
    public int getScreenYFromGridY(int y) {
        return (int)((double)(y*grid.blockSize)*view.scaleY + view.offsetY);
    }
    
    public int getRelativeXFromGridX(int x) {
        return x*grid.blockSize;
    }
    public int getRelativeYFromGridY(int y) {
        return y*grid.blockSize;
    }
}
