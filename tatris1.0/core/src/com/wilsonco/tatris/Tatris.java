/*
 *  Tatris
 *  Copyright (C) April 2016, Daniel Wilson
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
package com.wilsonco.tatris;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.FPSLogger;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.GL30;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.badlogic.gdx.utils.viewport.FitViewport;

public class Tatris extends ApplicationAdapter {
        static Tatris game;
        

        
	SpriteBatch batch;
        OrthographicCamera camera;
        FPSLogger fps;
	FitViewport viewPort;
        
        OrthographicCamera backCamera;
	ExtendViewport backViewPort;
        
        GmRoom currentRoom;
	
        
        //============== STATIC ASSETS AND SETTINGS ====================================================
        
        public static GmInputAdapter inputAdapter;
        
        public static Texture greenTex;
        public static Texture blackTex;
        public static Texture menuBackground;
        public static Texture menuBackgroundTiny;
        public static Texture logo;
        
        public final static int gameWidth = 1067;
        public final static int gameHeight = 832;
        public final static Color uiColour = new Color(11f/256f,211f/256f,51f/256f,0);
        public static BitmapFont mainFont;
        
        public static class KeyBindings {
            public static int moveLeftKey = Input.Keys.A;
            public static int moveRightKey = Input.Keys.D;
            public static int moveRotateLeftKey = Input.Keys.Q;
            public static int moveRotateRightKey = Input.Keys.E;
            public static int moveJumpUpKey = Input.Keys.W;
            public static int moveJumpDownKey = Input.Keys.S;
            
            public static int menuUpKey = Input.Keys.UP;
            public static int menuDownKey = Input.Keys.DOWN;
            public static int menuEnterKey = Input.Keys.ENTER;
            public static int menuCloseKey = Input.Keys.ESCAPE;
            
            public static int cameraUpKey = Input.Keys.NUMPAD_8;
            public static int cameraDownKey = Input.Keys.NUMPAD_2;
            public static int cameraLeftKey = Input.Keys.NUMPAD_4;
            public static int cameraRightKey = Input.Keys.NUMPAD_6;
            public static int cameraZoomInKey = Input.Keys.PLUS;
            public static int cameraZoomOutKey = Input.Keys.MINUS;
        }
        
	@Override
	public void create () {
            Tatris.game = this;
            //========================== LOAD IN STATIC ASSETS =======================================
            System.out.println("Loading static texture assets...");
            Tatris.greenTex = new Texture(Gdx.files.internal("textures/green.png"));
            Tatris.blackTex = new Texture(Gdx.files.internal("textures/black.png"));
            Tatris.menuBackground = new Texture(Gdx.files.internal("ui/uiBackDownScaled.png"));
            Tatris.menuBackgroundTiny = new Texture(Gdx.files.internal("ui/uiBackTiny.png"));
            Tatris.logo = new Texture(Gdx.files.internal("ui/loogo.png"));
            Tatris.mainFont = new BitmapFont(Gdx.files.internal("fonts/tatrisFont3.fnt"));
            System.out.println("Done.");
            // ============================ SET UP INPUT ADAPTER =======================================
            
            inputAdapter = new GmInputAdapter();
            
            // ============================ CREATE FIRST ROOM, SET AS CURRENT ROOM ========================
            
            currentRoom = new MainMenuRoom();
            currentRoom.create();
            
            // ============================ SET UP FPSLOGGER, CORRECT WAY UP CAMERA AND SPRITEBATCH ===========
            
            fps = new FPSLogger();
            fps.log();
            
            camera= new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
            camera.setToOrtho(true, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
            
            viewPort = new FitViewport(gameWidth,gameHeight,camera);
            
            
            batch = new SpriteBatch();
            
            
            
	}

	@Override
	public void render() {
            //fps.log();
            camera.update(); // Update camera
            batch.setProjectionMatrix(camera.combined); // Set batch camera
            
            currentRoom.beforeEverything();
            
            Gdx.gl.glClearColor(0, 0, 0, 1);
            Gdx.gl.glClear(GL30.GL_COLOR_BUFFER_BIT);
            
            batch.begin(); // Batch begin
            
            
            currentRoom.updateStep(batch);
            currentRoom.updateDraw(batch); // RUN STEP AND DRAW ETC
            
            //GmObject.renderAllGmObjects(batch);          OBSELETE
            batch.end(); // Batch end
            
            currentRoom.afterEverything();
            
	}
        
        @Override
        public void resize(int width, int height) {
            viewPort.update(width,height);
        }
}


class GameRoom extends GmRoom { // Tatris.gameroom (static) ---> e.g: Tatris.gameroom.gameList.add(fhiei)
    
    String[] backImage = new String[]{"#............#","#............#","#............#","#............#","#............#","#............#","#............#","#............#","#............#","#............#","#............#","#............#","#............#","#............#","#............#","#............#","#............#","#............#","#............#","#............#","#............#","#............#","#............#","#............#","##############"};
    
    GmListContainer menuList;
    GmListContainer guiList;
    GmListContainer gameList;
    
    TatrisWorldGrid tatrisGrid;
    
    GmObject testSquare;
    
    
    GmScaleObject guiScale;
    GmScaleObject viewButtonsScale;
    GmScaleObject nextBlockScaleObject;
    
    GuiRectangleObjectWithBlackBorder gameBorder;
    GuiRectangleObject interiorGameBorder;
    MainGuiDrawObject guiObject;
    GmObject guiBack;
    GuiFontObject guiText;
    
    ViewButtonsBar viewButtonsBar;
    
    InGameMenu inGameMenu;
    BottomRightCornerSubMenu bottomRightCornerSubMenu;
    
    public final int gridWidth = 12;
    public final int gridHeight = 24;
    public final int gameOffsetX = 128;
    public final int gameOffsetY = 16;
    public final double gameScale = 2;
    public final int guiOffsetX = 24;
    public final int guiOffsetY = 16;
    public final int guiBorderRight = 128;
    public final int blockSize = 16;
    public final int borderLineWidth = 4;
    public final int viewButtonHeight = 32;
    
    public final int guiPositionX = (int)(blockSize*gameScale*gridWidth + gameOffsetX + guiOffsetX);
    public final int guiHeight = (int)(blockSize*gameScale*gridHeight) + viewButtonHeight;
    public final int guiWidth = Tatris.gameWidth - guiBorderRight - guiPositionX;
    public final int gameWidth = (int)(blockSize*gameScale*gridWidth);
    public final int gameHeight = (int)(blockSize*gameScale*gridHeight);
    
    
    @Override public void create() {
        // --------------------------------------------- INITIALISE OBJECT LISTS
        world = new GmObjectWorld(Tatris.inputAdapter);
        gameList = world.add(new GmObjectListFullyDynamic(), new GmObjectListFullyDynamic());
        guiList = world.add(new GmObjectListSemiDynamic(), new GmObjectListSemiDynamic());
        menuList = world.add(new GmObjectListSemiDynamic(), new GmObjectListSemiDynamic());
        
        // --------------------------------------------- CREATE GUI SCALE OBJECT
        
        guiScale = new GmScaleObject(1,1,guiPositionX,guiOffsetY);
        nextBlockScaleObject = new GmScaleObject(2,2,guiPositionX,guiOffsetY);
        viewButtonsScale = new GmScaleObject(1,1,gameOffsetX-borderLineWidth/2,gameOffsetY + gameHeight);
        
        // --------------------------------------------- OLD CODE
        
        /*guiBack = new GmObject("ui/uiBack.png");
        guiBack.scaleObject = guiScale;
        guiList.draw.add(guiBack);
        guiBack.spriteWidth = guiWidth;
        guiBack.spriteHeight = guiHeight;*/
        
        /*guiText = new GuiFontObject(mainFont, "SPAM\nCORNED BEEF");
        guiText.scaleObject = guiScale;
        guiText.scaleX = 0.8;
        guiText.scaleY = 0.8;
        guiText.updateScale();
        guiList.draw.add(guiText);
        guiText.y = 128;*/
        
        // --------------------------------------------- CREATE TATRIS WORLD GRID
        
        tatrisGrid = new TatrisWorldGrid(gridWidth+2,gridHeight+1,16,this);
        gameList.draw.add(tatrisGrid);
        gameList.step.add(tatrisGrid);
        
        
        // --------------------------------------------- CREATE INTERIOR GAME BORDER -DRAWN BEHIND GAME BORDER
        
        interiorGameBorder = new GuiRectangleObject(tatrisGrid.uiShapes,
                tatrisGrid.scale,
                tatrisGrid.blockSize - borderLineWidth/2,
                -borderLineWidth/2,
                (tatrisGrid.width-2)*tatrisGrid.blockSize+borderLineWidth,
                (tatrisGrid.height-1)*tatrisGrid.blockSize+borderLineWidth,
                borderLineWidth,
                Tatris.uiColour);
        guiList.draw.add(interiorGameBorder);
        
        
        // --------------------------------------------- CREATE GAME BORDER
        
        gameBorder = new GuiRectangleObjectWithBlackBorder(tatrisGrid.uiShapes,
                null,
                gameOffsetX-borderLineWidth/2,
                gameOffsetY-borderLineWidth/2,
                gameWidth+borderLineWidth,
                gameHeight+borderLineWidth,
                borderLineWidth,
                Tatris.uiColour,
                1024);
        gameBorder.updateBlackBorderPositions();
        guiList.draw.add(gameBorder);
        
        //----------------------------------------------- CREATE VIEW BUTTONS BAR
        
        viewButtonsBar = new ViewButtonsBar(tatrisGrid.uiShapes,
                tatrisGrid,
                viewButtonsScale,
                Tatris.mainFont,
                0,
                0,
                gameWidth+borderLineWidth,
                viewButtonHeight+borderLineWidth/2,
                borderLineWidth,
                Tatris.uiColour);
        guiList.draw.add(viewButtonsBar);
        
        // --------------------------------------------- CREATE GUI OBJECT FOR RIGHT SIDE OF SCREEN
        
        guiObject = new MainGuiDrawObject(tatrisGrid.uiShapes,
                tatrisGrid,
                guiScale,
                Tatris.mainFont,
                0,//guiPositionX,
                0,//gameOffsetY,
                guiWidth/guiScale.scaleX,
                guiHeight/guiScale.scaleY,
                borderLineWidth,
                Tatris.uiColour);
        guiList.draw.add(guiObject);
        tatrisGrid.gui = guiObject;
        
        // --------------------------------------------- CREATE BOTTOM-RIGHT-CORNER-SUB-MENU
        
        bottomRightCornerSubMenu = new BottomRightCornerSubMenu(tatrisGrid.uiShapes,
                Tatris.mainFont,guiScale,Tatris.uiColour,this);
        menuList.draw.add(bottomRightCornerSubMenu);
        menuList.step.add(bottomRightCornerSubMenu);
        world.inputObjects.add(bottomRightCornerSubMenu);
        
        // --------------------------------------------- CREATE IN-GAME-MENU
        
        inGameMenu = new InGameMenu(tatrisGrid.uiShapes,
                Tatris.mainFont,null,Tatris.uiColour,this);
        menuList.draw.add(inGameMenu);
        menuList.step.add(inGameMenu);
        world.inputObjects.add(inGameMenu);
        
        // --------------------------------------------- CREATE BLOCK RENDERER FOR STATIC BACK
        
        BrickConfig bConf = new BrickConfig(tatrisGrid,backImage,"dummy");
        BlockRenderer c = new BlockRenderer(gameList,tatrisGrid,bConf,tatrisGrid.scale);
        //c.printGrid = true;
        gameList.step.add(c); gameList.draw.add(c);
        System.out.println("Created");
    }
    @Override public void updateStep(SpriteBatch sb) {
        world.updateStep(sb);
    }
    @Override public void updateDraw(SpriteBatch sb) {
        world.updateDraw(sb);
    }
    @Override public void destroy() {
        world.destroy();
        world = null; // Obselete (ish)
    }
    
    @Override public void beforeEverything() {
        tatrisGrid.uiShapes.setProjectionMatrix(Tatris.game.camera.combined); // Set shapeRenderer camera
        tatrisGrid.uiShapes.begin(ShapeType.Filled); // ShapeRenderer begin
    }
    @Override public void afterEverything() {
        tatrisGrid.resetGrid();
        tatrisGrid.uiShapes.end(); // ShapeRenderer end
    }
}

//=========================================== MISC DRAW OBJECTS ================================================

class GuiFontObject extends GmFontObject {
    GuiFontObject(BitmapFont font, String text) {
         super(font,text);
    }
    @Override void draw(SpriteBatch sb) {
        if (autoSetScale) updateScale();
        font.setColor(1f,0.5f,1f,1f);
        font.draw(sb,text,(int)(scaleObject.scaleX*x + scaleObject.offsetX)+5, (int)(scaleObject.scaleY*y + scaleObject.offsetY));
        font.setColor(1f,1f,1f,1f);
        font.draw(sb,text,(int)(scaleObject.scaleX*x + scaleObject.offsetX), (int)(scaleObject.scaleY*y + scaleObject.offsetY));
    }
}

